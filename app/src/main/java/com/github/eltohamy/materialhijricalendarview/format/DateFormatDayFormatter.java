package com.github.eltohamy.materialhijricalendarview.format;


import androidx.annotation.NonNull;

import com.github.eltohamy.materialhijricalendarview.CalendarDay;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.volleyutils.MyApplication;

/**
 * Format using a {@linkplain DateFormat} instance.
 */
public class DateFormatDayFormatter implements DayFormatter {

    private final DateFormat dateFormat;

    /**
     * Format using a default format
     */
    public DateFormatDayFormatter() {
        this.dateFormat = new SimpleDateFormat("d", Locale.forLanguageTag(UserPreferenceHelper.getCurrentLanguage(MyApplication.getInstance().getApplicationContext())));
    }

    /**
     * Format using a specific {@linkplain DateFormat}
     *
     * @param format the format to use
     */
    public DateFormatDayFormatter(@NonNull DateFormat format) {
        this.dateFormat = format;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    public String format(@NonNull CalendarDay day) {
        dateFormat.setCalendar(day.getCalendar());
        return dateFormat.format(day.getDate());
    }
}
