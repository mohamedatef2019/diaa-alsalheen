package com.github.eltohamy.materialhijricalendarview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.Gravity;
import android.widget.TextView;

import com.github.eltohamy.materialhijricalendarview.format.WeekDayFormatter;

import java.util.Calendar;

import grand.diaaalsalheen.R;

/**
 * Display a day of the week
 */
@SuppressLint("ViewConstructor")
class WeekDayView extends androidx.appcompat.widget.AppCompatTextView {

    private WeekDayFormatter formatter = WeekDayFormatter.DEFAULT;
    private int dayOfWeek;

    public WeekDayView(Context context, int dayOfWeek) {
        super(context);
        setGravity(Gravity.CENTER);

        setTextAlignment(TEXT_ALIGNMENT_CENTER);

        setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        setTextColor(Color.WHITE);
        setDayOfWeek(dayOfWeek);
    }

    public void setWeekDayFormatter(WeekDayFormatter formatter) {
        this.formatter = formatter == null ? WeekDayFormatter.DEFAULT : formatter;
        setDayOfWeek(dayOfWeek);
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
        setText(formatter.format(dayOfWeek));
    }

    public void setDayOfWeek(Calendar calendar) {
        setDayOfWeek(CalendarUtils.getDayOfWeek(calendar));
    }
}
