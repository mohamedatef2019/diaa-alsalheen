package grand.diaaalsalheen.zyarat.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentZyaratBinding;
import grand.diaaalsalheen.zyarat.viewmodel.ZyaratViewModel;


public class ZyaratFragment extends BaseFragment {
    private View rootView;
    private ZyaratViewModel zyaratViewModel;
    private FragmentZyaratBinding fragmentZyaratBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentZyaratBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_zyarat, container, false);
        assert getArguments() != null;

        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();

    }

    private void binding() {
        rootView = fragmentZyaratBinding.getRoot();
        assert getArguments() != null;
        zyaratViewModel = new ZyaratViewModel(getArguments().getInt(Params.BUNDLE_ZYARA_TYPE,0));
        fragmentZyaratBinding.setZyaratViewModel(zyaratViewModel);
    }


    private void liveDataListeners() {
        zyaratViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });

        zyaratViewModel.getZyaratAdapter().getItemsOperationsLiveListener().observe(this, zyaraItem -> {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Params.BUNDLE_ZYARA_ITEM, zyaraItem);
            MovementManager.startActivity(getActivity(), Codes.ZYARAT_DETAILS, bundle);
        });
    }


}