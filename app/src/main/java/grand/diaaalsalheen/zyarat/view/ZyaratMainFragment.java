package grand.diaaalsalheen.zyarat.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentZyaratMainBinding;
import grand.diaaalsalheen.zyarat.viewmodel.MainZyaratViewModel;


public class ZyaratMainFragment extends BaseFragment {
    private View rootView;
    private FragmentZyaratMainBinding fragmentZyaratMainBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentZyaratMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_zyarat_main, container, false);
        init();
        return rootView;
    }

    private void init() {
        binding();

        Bundle zyarat1 = new Bundle();
        zyarat1.putInt(Params.BUNDLE_ZYARA_TYPE,1);
        Bundle zyarat2 = new Bundle();
        zyarat2.putInt(Params.BUNDLE_ZYARA_TYPE,0);

        Bundle zyarat3 = new Bundle();
        zyarat3.putInt(Params.BUNDLE_ZYARA_TYPE,2);


        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getChildFragmentManager(), FragmentPagerItems.with(getActivity())
                .add(R.string.label_house_visit, ZyaratFragment.class,zyarat1)
                .add(R.string.label_week_visit, ZyaratFragment.class,zyarat2)
                .add(R.string.label_ask_visit, ZyaratFragment.class,zyarat3)
                .create());

        fragmentZyaratMainBinding.viewpager.setAdapter(adapter);
        fragmentZyaratMainBinding.viewpagertab.setViewPager(fragmentZyaratMainBinding.viewpager);

    }

    private void binding() {
        MainZyaratViewModel mainZyaratViewModel = new MainZyaratViewModel();
        rootView = fragmentZyaratMainBinding.getRoot();
        fragmentZyaratMainBinding.setMainZyaratViewModel(mainZyaratViewModel);

    }


}