
package grand.diaaalsalheen.zyarat.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.databinding.ItemZyaraBinding;
import grand.diaaalsalheen.zyarat.response.ZyaraItem;
import grand.diaaalsalheen.zyarat.viewmodel.ZyaraItemViewModel;


public class ZyaratAdapter extends RecyclerView.Adapter<ZyaratAdapter.CompaniesViewHolder> {

    private List<ZyaraItem> zyaraItems;
    private MutableLiveData<ZyaraItem> itemsOperationsLiveListener;


    public ZyaratAdapter() {
        this.zyaraItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_zyara,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {
        ZyaraItem zyaraItem = zyaraItems.get(position);
        ZyaraItemViewModel companyItemViewModel = new ZyaraItemViewModel(zyaraItem,position);
        companyItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
            itemsOperationsLiveListener.setValue(zyaraItem);
        });
        holder.setViewModel(companyItemViewModel);
    }



    public MutableLiveData<ZyaraItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.zyaraItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<ZyaraItem> data) {

            this.zyaraItems.clear();

        assert data != null;
        this.zyaraItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemZyaraBinding itemZyaraBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemZyaraBinding == null) {
                itemZyaraBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemZyaraBinding != null) {
                itemZyaraBinding.unbind();
            }
        }

        void setViewModel(ZyaraItemViewModel zyaraItemViewModel) {
            if (itemZyaraBinding != null) {
                itemZyaraBinding.setItemZyaraCategoriesViewModel(zyaraItemViewModel);
            }
        }


    }



}
