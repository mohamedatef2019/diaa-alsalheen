package grand.diaaalsalheen.zyarat.view;


import android.content.Intent;
import android.os.Bundle;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import com.huhx0015.hxaudio.audio.HXMusic;
import org.sufficientlysecure.htmltextview.HtmlFormatter;
import org.sufficientlysecure.htmltextview.HtmlFormatterBuilder;
import grand.diaaalsalheen.BuildConfig;
import grand.diaaalsalheen.MediaPlayerUtils;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.SweetAlertDialogss;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentZyaraDetailsBinding;
import grand.diaaalsalheen.zyarat.response.ZyaraItem;
import grand.diaaalsalheen.zyarat.viewmodel.ZyaraDetailsViewModel;


public class ZyaratDetailsFragment extends BaseFragment {
    private View rootView;
    private ZyaraDetailsViewModel zyaraDetailsViewModel;
    private FragmentZyaraDetailsBinding fragmentZyaraDetailsBinding;
    private ZyaraItem zyaraItem;
    int textSize = 0;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentZyaraDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_zyara_details, container, false);
        assert getArguments() != null;

        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();

    }

    private void binding() {
        rootView = fragmentZyaraDetailsBinding.getRoot();
        assert getArguments() != null;
        zyaraItem = (ZyaraItem) getArguments().getSerializable(Params.BUNDLE_ZYARA_ITEM);
        zyaraDetailsViewModel = new ZyaraDetailsViewModel(zyaraItem);
        fragmentZyaraDetailsBinding.setZyaraDetailsViewModel(zyaraDetailsViewModel);
    }

    @Override
    public void onDestroyView() {
        HXMusic.stop();
        super.onDestroyView();
    }

    private void liveDataListeners() {
        zyaraDetailsViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.ZOOM_IN) {
                textSize+=5;
                fragmentZyaraDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            } else if (result == Codes.ZOOM_OUT) {
                textSize-=5;
                fragmentZyaraDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            }
            else if (result == Codes.PLAY_AUDIO) {
                MediaPlayerUtils.playAudio(zyaraDetailsViewModel.getZyaraDetailsItem().getVoice(),
                        zyaraItem.getName(),getContext(),fragmentZyaraDetailsBinding.ivPlay);

            }else if (result == Codes.ADD_TO_FAVOURITE) {
                if(UserPreferenceHelper.isLogined()){
                SweetAlertDialogss.showFavDialog(getActivity());
                }else {
                    SweetAlertDialogss.loginToContinue(getActivity());
                }
            }else if (result == Codes.DATA_RECIVED) {
                Spanned formattedHtml = HtmlFormatter.formatHtml(new HtmlFormatterBuilder().setHtml(zyaraDetailsViewModel.getZyaraDetailsItem().getText()));
                String str=zyaraDetailsViewModel.getZyaraDetailsItem().getText();
                fragmentZyaraDetailsBinding.tvHtmlViewer.loadDataWithBaseURL(null, str, "text/html", "utf-8", null);
                textSize =  270;
                fragmentZyaraDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            }else if(result == Codes.SHARE){
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, WebServices.SHARE_LINK+"6/"+ zyaraDetailsViewModel.getZyaraDetailsItem().getId() + "/ar \n\n" + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);

                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }


        });


    }


}