package grand.diaaalsalheen.zyarat.response.zyaraDetails;

 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;


public class ZyaraDetailsItem implements Serializable {

	@SerializedName("voice")
	private String voice;

	@SerializedName("zyarat_category_id")
	private int zyaratCategoryId;

	@SerializedName("id")
	private int id;

	@SerializedName("text")
	private String text;

	public void setVoice(String voice){
		this.voice = voice;
	}

	public String getVoice(){
		return voice;
	}

	public void setZyaratCategoryId(int zyaratCategoryId){
		this.zyaratCategoryId = zyaratCategoryId;
	}

	public int getZyaratCategoryId(){
		return zyaratCategoryId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"voice = '" + voice + '\'' + 
			",zyarat_category_id = '" + zyaratCategoryId + '\'' + 
			",id = '" + id + '\'' + 
			",text = '" + text + '\'' + 
			"}";
		}
}