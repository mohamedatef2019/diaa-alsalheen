package grand.diaaalsalheen.zyarat.viewmodel;

import android.util.Log;
import android.view.View;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.zyarat.response.ZyaratCategoriesResponse;
import grand.diaaalsalheen.zyarat.view.ZyaratAdapter;


public class ZyaratViewModel extends BaseViewModel {

    private ZyaratAdapter zyaratAdapter;


    public ZyaratViewModel(int type) {
        getZyarat(type);
    }


    @BindingAdapter({"app:adapter"})
    public static void getCategoriesBinding(RecyclerView recyclerView, ZyaratAdapter zyaratAdapter) {
        recyclerView.setAdapter(zyaratAdapter);
    }


    @Bindable
    public ZyaratAdapter getZyaratAdapter() {
        return this.zyaratAdapter == null ? this.zyaratAdapter = new ZyaratAdapter() : this.zyaratAdapter;
    }


    private void getZyarat(int type) {
         accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                Log.e("zyaratCatego",response+"");
                ZyaratCategoriesResponse zyaratCategoriesResponse = (ZyaratCategoriesResponse) response;
                getZyaratAdapter().updateData(zyaratCategoriesResponse.getData());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.ZYARAT_CATEGORIES+type,null,   ZyaratCategoriesResponse.class);

    }
}
