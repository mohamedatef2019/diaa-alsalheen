package grand.diaaalsalheen.zyarat.viewmodel;

import android.view.View;

import com.android.volley.Request;

import grand.diaaalsalheen.FavouriteRequest;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.favourite.viewmodel.FavouriteViewModel;
import grand.diaaalsalheen.zyarat.response.ZyaraItem;
import grand.diaaalsalheen.zyarat.response.zyaraDetails.ZyaraDetailsItem;
import grand.diaaalsalheen.zyarat.response.zyaraDetails.ZyaraDetailsResponse;

public class ZyaraDetailsViewModel extends BaseViewModel {

    private ZyaraDetailsItem zyaraDetailsItem;

    public ZyaraDetailsViewModel(ZyaraItem zyaraItem) {
        getMonagats(zyaraItem);
    }

    private void getMonagats(ZyaraItem zyaraItem) {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                ZyaraDetailsResponse zyaraDetailsResponse = (ZyaraDetailsResponse) response;
                zyaraDetailsItem = zyaraDetailsResponse.getData();
                accessLoadingBar(View.GONE);
                getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.ZYARAT_DETAILS + zyaraItem.getId(), null, ZyaraDetailsResponse.class);
    }

    public ZyaraDetailsItem getZyaraDetailsItem() {
        return zyaraDetailsItem;
    }

    public void onPlayAudioClick(){
        getClicksMutableLiveData().setValue(Codes.PLAY_AUDIO);
    }

    public void onZoomInFontClick() {
        getClicksMutableLiveData().setValue(Codes.ZOOM_IN);
    }

    public void onZoomOutFontClick() {
        getClicksMutableLiveData().setValue(Codes.ZOOM_OUT);
    }

    public void addToFavouriteClick() {
        getClicksMutableLiveData().setValue(Codes.ADD_TO_FAVOURITE);
        FavouriteViewModel.addToFavourite(new FavouriteRequest(getZyaraDetailsItem().getId(), "App\\Models\\Zyarat"));
    }

    public void onShareClick() {
        getClicksMutableLiveData().setValue(Codes.SHARE);
    }
}
