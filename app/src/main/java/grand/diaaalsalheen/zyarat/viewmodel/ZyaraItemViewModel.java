
package grand.diaaalsalheen.zyarat.viewmodel;

import androidx.databinding.Bindable;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.mongat.response.MonagatCategoryItem;
import grand.diaaalsalheen.zyarat.response.ZyaraItem;


public class ZyaraItemViewModel extends BaseItemViewModel {
    private ZyaraItem zyaraItem;
    private int position;

    public ZyaraItemViewModel(ZyaraItem zyaraItem,int position) {
        this.zyaraItem = zyaraItem;
        this.position=position;
    }


    @Bindable
    public ZyaraItem getZyaraItem() {
        return zyaraItem;
    }

    public void onZyaratCategoryItemClicked(){
        getItemsOperationsLiveListener().setValue(Codes.ZYARAT_DETAILS);
    }

    public boolean isLight() {
        return position%2==0;
    }


}
