
package grand.diaaalsalheen.search.viewmodel;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.search.response.SearchModel;


public class SearchItemViewModel extends BaseObservable {
    private SearchModel searchModel;
    private MutableLiveData<Integer> itemsOperationsLiveListener;

    public SearchItemViewModel(SearchModel searchModel) {
        this.searchModel = searchModel;
        this.itemsOperationsLiveListener = new MutableLiveData<>();
    }


    public MutableLiveData<Integer> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener;
    }

    @Bindable
    public SearchModel getSearchModel() {
        return searchModel;
    }


    public void setSearchModel(SearchModel searchModel) {
        this.searchModel = searchModel;
    }





}
