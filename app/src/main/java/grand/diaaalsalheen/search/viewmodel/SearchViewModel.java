package grand.diaaalsalheen.search.viewmodel;


import android.util.Log;
import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.search.SearchRequest;
import grand.diaaalsalheen.search.response.AamalItem;
import grand.diaaalsalheen.search.response.AzkarItem;
import grand.diaaalsalheen.search.response.DoaaItem;
import grand.diaaalsalheen.search.response.MonagatItem;
import grand.diaaalsalheen.search.response.SearchModel;
import grand.diaaalsalheen.search.response.SearchesResonse;
import grand.diaaalsalheen.search.response.TaqebatItem;
import grand.diaaalsalheen.search.response.ZyaratItem;
import grand.diaaalsalheen.search.view.SearchAdapter;

import static grand.diaaalsalheen.quran.view.SewarFragment.getJsonFromAssets;


public class SearchViewModel extends BaseViewModel {

    private SearchAdapter itemsAdapter;
    private SearchRequest searchRequest;

    public SearchViewModel( ) {
        searchRequest = new SearchRequest();

    }


    @BindingAdapter({"app:adapter"})
    public static void getItemsBinding(RecyclerView recyclerView, SearchAdapter itemsAdapter) {
        recyclerView.setAdapter(itemsAdapter);

    }

    public SearchAdapter getItemsAdapter() {
        return this.itemsAdapter == null ? this.itemsAdapter = new SearchAdapter() : this.itemsAdapter;
    }

    public void search(String text) {
        searchRequest.setText(text);

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                SearchesResonse searchesResonse = (SearchesResonse) response;
                List<SearchModel> searchModels = new ArrayList<>();

                Log.e("Errora","here "+searchRequest.getText()+" ");
                List<DoaaItem> doaa = searchesResonse.getData().getDoaa();
                for(int i=0; i<doaa.size(); i++){
                    searchModels.add(new SearchModel(doaa.get(i).getDoaaId(), ResourcesManager.getString(R.string.label_adaya),doaa.get(i).getText(),"doaa",i==0));
                }

                List<TaqebatItem> taqebat = searchesResonse.getData().getTaqebat();

                for(int i=0; i<taqebat.size(); i++){
                    searchModels.add(new SearchModel(taqebat.get(i).getTaqebatId(), ResourcesManager.getString(R.string.takebat),taqebat.get(i).getText(),"taqebat",i==0));
                }

                List<ZyaratItem> zyarat = searchesResonse.getData().getZyarat();

                for(int i=0; i<zyarat.size(); i++){
                    searchModels.add(new SearchModel(zyarat.get(i).getZyaratId(), ResourcesManager.getString(R.string.label_zyarat),zyarat.get(i).getText(),"zyarat",i==0));
                }


                List<MonagatItem> monagat = searchesResonse.getData().getMonagat();

                for(int i=0; i<monagat.size(); i++){
                    searchModels.add(new SearchModel(monagat.get(i).getMonagatId(), ResourcesManager.getString(R.string.label_monagat),monagat.get(i).getText(),"monagat",i==0));
                }

                List<AamalItem> aamal = searchesResonse.getData().getAamal();

                for(int i=0; i<aamal.size(); i++){
                    searchModels.add(new SearchModel(aamal.get(i).getAamalId(), ResourcesManager.getString(R.string.amal),aamal.get(i).getText(),"aamal",i==0));
                }

                List<AzkarItem> azkar = searchesResonse.getData().getAzkar();

                for(int i=0; i<azkar.size(); i++){
                    searchModels.add(new SearchModel(azkar.get(i).getAzkarId(), ResourcesManager.getString(R.string.label_azkar_),azkar.get(i).getText(),"azkar",i==0));
                }



                getItemsAdapter().updateData(searchModels,text);
                getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
                accessLoadingBar(View.GONE);
                notifyChange();

            }

        }).requestJsonObject(Request.Method.POST, WebServices.SEARCH, searchRequest, SearchesResonse.class);
    }


    public SearchRequest getSearchRequest() {
        return searchRequest;
    }
}
