package grand.diaaalsalheen.search.view;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentFavouritesBinding;
import grand.diaaalsalheen.databinding.FragmentSearchBinding;
import grand.diaaalsalheen.search.viewmodel.SearchViewModel;


public class SearchFragment extends BaseFragment {

    private View rootView;
    private SearchViewModel searchViewModel;
    private FragmentSearchBinding fragmentSearchBinding;



    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSearchBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentSearchBinding.getRoot();
        assert getArguments() != null;
        searchViewModel = new SearchViewModel();
        fragmentSearchBinding.setSearchViewModel(searchViewModel);
    }


    private void liveDataListeners() {
        searchViewModel.getClicksMutableLiveData().observe(this, result -> {

         if(result == Codes.DATA_RECIVED){
                setAdapterListener();
            }else if(result ==  View.VISIBLE || result ==  View.GONE){
                accessLoadingBar(result);
            }
        });

        fragmentSearchBinding.etSearchText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchViewModel.search(fragmentSearchBinding.etSearchText.getText()+"");
                return true;
            }
            return false;
        });
    }


    private void setAdapterListener(){
        searchViewModel.getItemsAdapter().getItemsOperationsLiveListener().observeForever(itemsItem -> {


        });
    }






}