
package grand.diaaalsalheen.search.view;

import android.graphics.Color;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.binaryfork.spanny.Spanny;

import org.sufficientlysecure.htmltextview.HtmlFormatter;
import org.sufficientlysecure.htmltextview.HtmlFormatterBuilder;

import java.util.ArrayList;
import java.util.List;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.databinding.ItemSearchBinding;
import grand.diaaalsalheen.search.response.SearchModel;
import grand.diaaalsalheen.search.viewmodel.SearchItemViewModel;



public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ItemsViewHolder> {

    private List<SearchModel> searchModels;
    private MutableLiveData<SearchModel> itemsOperationsLiveListener;
    private String text;

    public SearchAdapter() {
        this.searchModels = new ArrayList<>();
    }

    @NonNull
    @Override
    public ItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search,
                new FrameLayout(parent.getContext()), false);
        return new ItemsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsViewHolder holder, final int position) {
        SearchModel itemsItem = searchModels.get(position);
        SearchItemViewModel categoryItemViewModel = new SearchItemViewModel(itemsItem);

        Spanned formattedHtml = HtmlFormatter.formatHtml(new HtmlFormatterBuilder().setHtml(itemsItem.getDetails()));
        Spanny spanny = new Spanny(formattedHtml);
        spanny.findAndSpan(text, () -> new ForegroundColorSpan(holder.itemView.getResources().getColor(R.color.colorPrimaryDark)));
        holder.itemSearchBinding.tvSearchText.setText(spanny);

        categoryItemViewModel.getItemsOperationsLiveListener().observeForever(integer -> {


        });
        holder.setViewModel(categoryItemViewModel);
    }


    public List<SearchModel> getItemsItems() {
        return searchModels;
    }

    public MutableLiveData<SearchModel> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }

    @Override
    public int getItemCount() {
        return this.searchModels.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ItemsViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ItemsViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<SearchModel> data,String text) {
        this.text=text;
        this.searchModels.clear();
        assert data != null;
        this.searchModels.addAll(data);
        notifyDataSetChanged();
    }

    static class ItemsViewHolder extends RecyclerView.ViewHolder {
        ItemSearchBinding itemSearchBinding;

        ItemsViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemSearchBinding == null) {
                itemSearchBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemSearchBinding != null) {
                itemSearchBinding.unbind();
            }
        }

        void setViewModel(SearchItemViewModel itemItemViewModel) {
            if (itemSearchBinding != null) {
                itemSearchBinding.setItemSearchViewModel(itemItemViewModel);
            }
        }


    }


}
