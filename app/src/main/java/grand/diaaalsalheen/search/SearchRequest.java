package grand.diaaalsalheen.search;


import com.google.gson.annotations.SerializedName;


public class SearchRequest {

	@SerializedName("text")
	private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}