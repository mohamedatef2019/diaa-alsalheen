package grand.diaaalsalheen.search.response;


import com.google.gson.annotations.SerializedName;


public class ZyaratItem{

	@SerializedName("zyarat_id")
	private int zyaratId;

	@SerializedName("text")
	private String text;

	@SerializedName("zyarat_trans_id")
	private int zyaratTransId;

	@SerializedName("locale")
	private String locale;

	public void setZyaratId(int zyaratId){
		this.zyaratId = zyaratId;
	}

	public int getZyaratId(){
		return zyaratId;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	public void setZyaratTransId(int zyaratTransId){
		this.zyaratTransId = zyaratTransId;
	}

	public int getZyaratTransId(){
		return zyaratTransId;
	}

	public void setLocale(String locale){
		this.locale = locale;
	}

	public String getLocale(){
		return locale;
	}

	@Override
 	public String toString(){
		return 
			"ZyaratItem{" + 
			"zyarat_id = '" + zyaratId + '\'' + 
			",text = '" + text + '\'' + 
			",zyarat_trans_id = '" + zyaratTransId + '\'' + 
			",locale = '" + locale + '\'' + 
			"}";
		}
}