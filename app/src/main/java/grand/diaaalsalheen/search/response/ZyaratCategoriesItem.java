package grand.diaaalsalheen.search.response;


import com.google.gson.annotations.SerializedName;


public class ZyaratCategoriesItem{

	@SerializedName("zyarat_category_id")
	private int zyaratCategoryId;

	@SerializedName("name")
	private String name;

	@SerializedName("zyarat_category_trans_id")
	private int zyaratCategoryTransId;

	@SerializedName("locale")
	private String locale;

	public void setZyaratCategoryId(int zyaratCategoryId){
		this.zyaratCategoryId = zyaratCategoryId;
	}

	public int getZyaratCategoryId(){
		return zyaratCategoryId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setZyaratCategoryTransId(int zyaratCategoryTransId){
		this.zyaratCategoryTransId = zyaratCategoryTransId;
	}

	public int getZyaratCategoryTransId(){
		return zyaratCategoryTransId;
	}

	public void setLocale(String locale){
		this.locale = locale;
	}

	public String getLocale(){
		return locale;
	}

	@Override
 	public String toString(){
		return 
			"ZyaratCategoriesItem{" + 
			"zyarat_category_id = '" + zyaratCategoryId + '\'' + 
			",name = '" + name + '\'' + 
			",zyarat_category_trans_id = '" + zyaratCategoryTransId + '\'' + 
			",locale = '" + locale + '\'' + 
			"}";
		}
}