package grand.diaaalsalheen.search.response;


import com.google.gson.annotations.SerializedName;


public class DoaaItem{

	@SerializedName("doaa_id")
	private int doaaId;

	@SerializedName("doaa_trans_id")
	private int doaaTransId;

	@SerializedName("text")
	private String text;

	@SerializedName("locale")
	private String locale;

	public void setDoaaId(int doaaId){
		this.doaaId = doaaId;
	}

	public int getDoaaId(){
		return doaaId;
	}

	public void setDoaaTransId(int doaaTransId){
		this.doaaTransId = doaaTransId;
	}

	public int getDoaaTransId(){
		return doaaTransId;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	public void setLocale(String locale){
		this.locale = locale;
	}

	public String getLocale(){
		return locale;
	}

	@Override
 	public String toString(){
		return 
			"DoaaItem{" + 
			"doaa_id = '" + doaaId + '\'' + 
			",doaa_trans_id = '" + doaaTransId + '\'' + 
			",text = '" + text + '\'' + 
			",locale = '" + locale + '\'' + 
			"}";
		}
}