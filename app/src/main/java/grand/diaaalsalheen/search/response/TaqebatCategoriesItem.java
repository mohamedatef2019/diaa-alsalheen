package grand.diaaalsalheen.search.response;


import com.google.gson.annotations.SerializedName;


public class TaqebatCategoriesItem{

	@SerializedName("name")
	private String name;

	@SerializedName("locale")
	private String locale;

	@SerializedName("taqebat_category_trans_id")
	private int taqebatCategoryTransId;

	@SerializedName("taqebat_category_id")
	private int taqebatCategoryId;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setLocale(String locale){
		this.locale = locale;
	}

	public String getLocale(){
		return locale;
	}

	public void setTaqebatCategoryTransId(int taqebatCategoryTransId){
		this.taqebatCategoryTransId = taqebatCategoryTransId;
	}

	public int getTaqebatCategoryTransId(){
		return taqebatCategoryTransId;
	}

	public void setTaqebatCategoryId(int taqebatCategoryId){
		this.taqebatCategoryId = taqebatCategoryId;
	}

	public int getTaqebatCategoryId(){
		return taqebatCategoryId;
	}

	@Override
 	public String toString(){
		return 
			"TaqebatCategoriesItem{" + 
			"name = '" + name + '\'' + 
			",locale = '" + locale + '\'' + 
			",taqebat_category_trans_id = '" + taqebatCategoryTransId + '\'' + 
			",taqebat_category_id = '" + taqebatCategoryId + '\'' + 
			"}";
		}
}