package grand.diaaalsalheen.search.response;


import com.google.gson.annotations.SerializedName;


public class AzkarItem{

	@SerializedName("said_text")
	private String saidText;

	@SerializedName("azkar_trans_id")
	private int azkarTransId;

	@SerializedName("azkar_id")
	private int azkarId;

	@SerializedName("text")
	private String text;

	@SerializedName("locale")
	private String locale;

	public void setSaidText(String saidText){
		this.saidText = saidText;
	}

	public String getSaidText(){
		return saidText;
	}

	public void setAzkarTransId(int azkarTransId){
		this.azkarTransId = azkarTransId;
	}

	public int getAzkarTransId(){
		return azkarTransId;
	}

	public void setAzkarId(int azkarId){
		this.azkarId = azkarId;
	}

	public int getAzkarId(){
		return azkarId;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	public void setLocale(String locale){
		this.locale = locale;
	}

	public String getLocale(){
		return locale;
	}

	@Override
 	public String toString(){
		return 
			"AzkarItem{" + 
			"said_text = '" + saidText + '\'' + 
			",azkar_trans_id = '" + azkarTransId + '\'' + 
			",azkar_id = '" + azkarId + '\'' + 
			",text = '" + text + '\'' + 
			",locale = '" + locale + '\'' + 
			"}";
		}
}