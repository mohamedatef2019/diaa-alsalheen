package grand.diaaalsalheen.search.response;


import com.google.gson.annotations.SerializedName;


public class AamalCategoriesItem{

	@SerializedName("aamal_category_trans_id")
	private int aamalCategoryTransId;

	@SerializedName("aamal_category_id")
	private int aamalCategoryId;

	@SerializedName("name")
	private String name;

	@SerializedName("locale")
	private String locale;

	public void setAamalCategoryTransId(int aamalCategoryTransId){
		this.aamalCategoryTransId = aamalCategoryTransId;
	}

	public int getAamalCategoryTransId(){
		return aamalCategoryTransId;
	}

	public void setAamalCategoryId(int aamalCategoryId){
		this.aamalCategoryId = aamalCategoryId;
	}

	public int getAamalCategoryId(){
		return aamalCategoryId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setLocale(String locale){
		this.locale = locale;
	}

	public String getLocale(){
		return locale;
	}

	@Override
 	public String toString(){
		return 
			"AamalCategoriesItem{" + 
			"aamal_category_trans_id = '" + aamalCategoryTransId + '\'' + 
			",aamal_category_id = '" + aamalCategoryId + '\'' + 
			",name = '" + name + '\'' + 
			",locale = '" + locale + '\'' + 
			"}";
		}
}