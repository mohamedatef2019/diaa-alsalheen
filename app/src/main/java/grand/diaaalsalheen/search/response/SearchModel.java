package grand.diaaalsalheen.search.response;

public class SearchModel {
    private int id;
    private  String title, details ,type;
    private boolean isParent;

    public SearchModel(int id,String title,String details,String type,boolean isParent){
        this.id=id;
        this.title=title;
        this.details=details;
        this.type=type;
        this.isParent=isParent;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public String getDetails() {
        return details;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setParent(boolean parent) {
        isParent = parent;
    }

    public boolean isParent() {
        return isParent;
    }
}
