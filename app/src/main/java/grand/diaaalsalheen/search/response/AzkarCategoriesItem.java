package grand.diaaalsalheen.search.response;


import com.google.gson.annotations.SerializedName;


public class AzkarCategoriesItem{

	@SerializedName("azkar_category_trans_id")
	private int azkarCategoryTransId;

	@SerializedName("name")
	private String name;

	@SerializedName("locale")
	private String locale;

	@SerializedName("azkar_category_id")
	private int azkarCategoryId;

	public void setAzkarCategoryTransId(int azkarCategoryTransId){
		this.azkarCategoryTransId = azkarCategoryTransId;
	}

	public int getAzkarCategoryTransId(){
		return azkarCategoryTransId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setLocale(String locale){
		this.locale = locale;
	}

	public String getLocale(){
		return locale;
	}

	public void setAzkarCategoryId(int azkarCategoryId){
		this.azkarCategoryId = azkarCategoryId;
	}

	public int getAzkarCategoryId(){
		return azkarCategoryId;
	}

	@Override
 	public String toString(){
		return 
			"AzkarCategoriesItem{" + 
			"azkar_category_trans_id = '" + azkarCategoryTransId + '\'' + 
			",name = '" + name + '\'' + 
			",locale = '" + locale + '\'' + 
			",azkar_category_id = '" + azkarCategoryId + '\'' + 
			"}";
		}
}