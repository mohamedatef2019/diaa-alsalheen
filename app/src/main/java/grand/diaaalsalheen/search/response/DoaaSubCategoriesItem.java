package grand.diaaalsalheen.search.response;


import com.google.gson.annotations.SerializedName;


public class DoaaSubCategoriesItem{

	@SerializedName("doaa_sub_category_id")
	private int doaaSubCategoryId;

	@SerializedName("sub_category_trans_id")
	private int subCategoryTransId;

	@SerializedName("name")
	private String name;

	@SerializedName("locale")
	private String locale;

	public void setDoaaSubCategoryId(int doaaSubCategoryId){
		this.doaaSubCategoryId = doaaSubCategoryId;
	}

	public int getDoaaSubCategoryId(){
		return doaaSubCategoryId;
	}

	public void setSubCategoryTransId(int subCategoryTransId){
		this.subCategoryTransId = subCategoryTransId;
	}

	public int getSubCategoryTransId(){
		return subCategoryTransId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setLocale(String locale){
		this.locale = locale;
	}

	public String getLocale(){
		return locale;
	}

	@Override
 	public String toString(){
		return 
			"DoaaSubCategoriesItem{" + 
			"doaa_sub_category_id = '" + doaaSubCategoryId + '\'' + 
			",sub_category_trans_id = '" + subCategoryTransId + '\'' + 
			",name = '" + name + '\'' + 
			",locale = '" + locale + '\'' + 
			"}";
		}
}