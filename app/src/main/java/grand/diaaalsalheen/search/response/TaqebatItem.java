package grand.diaaalsalheen.search.response;


import com.google.gson.annotations.SerializedName;


public class TaqebatItem{

	@SerializedName("taqebat_id")
	private int taqebatId;

	@SerializedName("text")
	private String text;

	@SerializedName("taqebat_trans_id")
	private int taqebatTransId;

	@SerializedName("locale")
	private String locale;

	public void setTaqebatId(int taqebatId){
		this.taqebatId = taqebatId;
	}

	public int getTaqebatId(){
		return taqebatId;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	public void setTaqebatTransId(int taqebatTransId){
		this.taqebatTransId = taqebatTransId;
	}

	public int getTaqebatTransId(){
		return taqebatTransId;
	}

	public void setLocale(String locale){
		this.locale = locale;
	}

	public String getLocale(){
		return locale;
	}

	@Override
 	public String toString(){
		return 
			"TaqebatItem{" + 
			"taqebat_id = '" + taqebatId + '\'' + 
			",text = '" + text + '\'' + 
			",taqebat_trans_id = '" + taqebatTransId + '\'' + 
			",locale = '" + locale + '\'' + 
			"}";
		}
}