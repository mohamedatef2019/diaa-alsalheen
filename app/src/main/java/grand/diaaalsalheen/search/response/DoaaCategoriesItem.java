package grand.diaaalsalheen.search.response;


import com.google.gson.annotations.SerializedName;


public class DoaaCategoriesItem{

	@SerializedName("name")
	private String name;

	@SerializedName("doaa_category_id")
	private int doaaCategoryId;

	@SerializedName("locale")
	private String locale;

	@SerializedName("doaa_category_trans_id")
	private int doaaCategoryTransId;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDoaaCategoryId(int doaaCategoryId){
		this.doaaCategoryId = doaaCategoryId;
	}

	public int getDoaaCategoryId(){
		return doaaCategoryId;
	}

	public void setLocale(String locale){
		this.locale = locale;
	}

	public String getLocale(){
		return locale;
	}

	public void setDoaaCategoryTransId(int doaaCategoryTransId){
		this.doaaCategoryTransId = doaaCategoryTransId;
	}

	public int getDoaaCategoryTransId(){
		return doaaCategoryTransId;
	}

	@Override
 	public String toString(){
		return 
			"DoaaCategoriesItem{" + 
			"name = '" + name + '\'' + 
			",doaa_category_id = '" + doaaCategoryId + '\'' + 
			",locale = '" + locale + '\'' + 
			",doaa_category_trans_id = '" + doaaCategoryTransId + '\'' + 
			"}";
		}
}