package grand.diaaalsalheen.search.response;


import com.google.gson.annotations.SerializedName;


public class AamalItem{

	@SerializedName("aamal_trans_id")
	private int aamalTransId;

	@SerializedName("aamal_id")
	private int aamalId;

	@SerializedName("text")
	private String text;

	@SerializedName("locale")
	private String locale;

	public void setAamalTransId(int aamalTransId){
		this.aamalTransId = aamalTransId;
	}

	public int getAamalTransId(){
		return aamalTransId;
	}

	public void setAamalId(int aamalId){
		this.aamalId = aamalId;
	}

	public int getAamalId(){
		return aamalId;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	public void setLocale(String locale){
		this.locale = locale;
	}

	public String getLocale(){
		return locale;
	}

	@Override
 	public String toString(){
		return 
			"AamalItem{" + 
			"aamal_trans_id = '" + aamalTransId + '\'' + 
			",aamal_id = '" + aamalId + '\'' + 
			",text = '" + text + '\'' + 
			",locale = '" + locale + '\'' + 
			"}";
		}
}