package grand.diaaalsalheen.search.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Data{

	@SerializedName("doaa_sub-categories")
	private List<DoaaSubCategoriesItem> doaaSubCategories;

	@SerializedName("doaa")
	private List<DoaaItem> doaa;

	@SerializedName("taqebat")
	private List<TaqebatItem> taqebat;

	@SerializedName("zyarat")
	private List<ZyaratItem> zyarat;

	@SerializedName("aamal_categories")
	private List<AamalCategoriesItem> aamalCategories;

	@SerializedName("azkar_categories")
	private List<AzkarCategoriesItem> azkarCategories;

	@SerializedName("monagat")
	private List<MonagatItem> monagat;

	@SerializedName("doaa_categories")
	private List<DoaaCategoriesItem> doaaCategories;

	@SerializedName("taqebat_categories")
	private List<TaqebatCategoriesItem> taqebatCategories;

	@SerializedName("aamal")
	private List<AamalItem> aamal;

	@SerializedName("azkar")
	private List<AzkarItem> azkar;

	@SerializedName("zyarat_categories")
	private List<ZyaratCategoriesItem> zyaratCategories;

	public void setDoaaSubCategories(List<DoaaSubCategoriesItem> doaaSubCategories){
		this.doaaSubCategories = doaaSubCategories;
	}

	public List<DoaaSubCategoriesItem> getDoaaSubCategories(){
		return doaaSubCategories;
	}

	public void setDoaa(List<DoaaItem> doaa){
		this.doaa = doaa;
	}

	public List<DoaaItem> getDoaa(){
		return doaa;
	}

	public void setTaqebat(List<TaqebatItem> taqebat){
		this.taqebat = taqebat;
	}

	public List<TaqebatItem> getTaqebat(){
		return taqebat;
	}

	public void setZyarat(List<ZyaratItem> zyarat){
		this.zyarat = zyarat;
	}

	public List<ZyaratItem> getZyarat(){
		return zyarat;
	}

	public void setAamalCategories(List<AamalCategoriesItem> aamalCategories){
		this.aamalCategories = aamalCategories;
	}

	public List<AamalCategoriesItem> getAamalCategories(){
		return aamalCategories;
	}

	public void setAzkarCategories(List<AzkarCategoriesItem> azkarCategories){
		this.azkarCategories = azkarCategories;
	}

	public List<AzkarCategoriesItem> getAzkarCategories(){
		return azkarCategories;
	}

	public void setMonagat(List<MonagatItem> monagat){
		this.monagat = monagat;
	}

	public List<MonagatItem> getMonagat(){
		return monagat;
	}

	public void setDoaaCategories(List<DoaaCategoriesItem> doaaCategories){
		this.doaaCategories = doaaCategories;
	}

	public List<DoaaCategoriesItem> getDoaaCategories(){
		return doaaCategories;
	}

	public void setTaqebatCategories(List<TaqebatCategoriesItem> taqebatCategories){
		this.taqebatCategories = taqebatCategories;
	}

	public List<TaqebatCategoriesItem> getTaqebatCategories(){
		return taqebatCategories;
	}

	public void setAamal(List<AamalItem> aamal){
		this.aamal = aamal;
	}

	public List<AamalItem> getAamal(){
		return aamal;
	}

	public void setAzkar(List<AzkarItem> azkar){
		this.azkar = azkar;
	}

	public List<AzkarItem> getAzkar(){
		return azkar;
	}

	public void setZyaratCategories(List<ZyaratCategoriesItem> zyaratCategories){
		this.zyaratCategories = zyaratCategories;
	}

	public List<ZyaratCategoriesItem> getZyaratCategories(){
		return zyaratCategories;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"doaa_sub-categories = '" + doaaSubCategories + '\'' + 
			",doaa = '" + doaa + '\'' + 
			",taqebat = '" + taqebat + '\'' + 
			",zyarat = '" + zyarat + '\'' + 
			",aamal_categories = '" + aamalCategories + '\'' + 
			",azkar_categories = '" + azkarCategories + '\'' + 
			",monagat = '" + monagat + '\'' + 
			",doaa_categories = '" + doaaCategories + '\'' + 
			",taqebat_categories = '" + taqebatCategories + '\'' + 
			",aamal = '" + aamal + '\'' + 
			",azkar = '" + azkar + '\'' + 
			",zyarat_categories = '" + zyaratCategories + '\'' + 
			"}";
		}
}