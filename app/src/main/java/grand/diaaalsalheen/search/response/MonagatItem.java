package grand.diaaalsalheen.search.response;


import com.google.gson.annotations.SerializedName;


public class MonagatItem{

	@SerializedName("monagat_trans_id")
	private int monagatTransId;

	@SerializedName("monagat_id")
	private int monagatId;

	@SerializedName("text")
	private String text;

	@SerializedName("locale")
	private String locale;

	public void setMonagatTransId(int monagatTransId){
		this.monagatTransId = monagatTransId;
	}

	public int getMonagatTransId(){
		return monagatTransId;
	}

	public void setMonagatId(int monagatId){
		this.monagatId = monagatId;
	}

	public int getMonagatId(){
		return monagatId;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	public void setLocale(String locale){
		this.locale = locale;
	}

	public String getLocale(){
		return locale;
	}

	@Override
 	public String toString(){
		return 
			"MonagatItem{" + 
			"monagat_trans_id = '" + monagatTransId + '\'' + 
			",monagat_id = '" + monagatId + '\'' + 
			",text = '" + text + '\'' + 
			",locale = '" + locale + '\'' + 
			"}";
		}
}