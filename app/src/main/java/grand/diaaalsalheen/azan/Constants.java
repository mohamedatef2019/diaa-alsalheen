package grand.diaaalsalheen.azan;

public interface Constants {


  int ALARM_ID = 1010;
  int PASSIVE_LOCATION_ID = 1011;
  int PRE_SUHOOR_ALARM_ID = 1012;
  int PRE_IFTAR_ALARM_ID = 1013;
  long ONE_MINUTE = 60000;
  long FIVE_MINUTES = ONE_MINUTE * 5;

  String EXTRA_PRAYER_NAME = "prayer_name";
  String EXTRA_PRAYER_TIME = "prayer_time";
  String EXTRA_PRE_ALARM_FLAG = "pre_alarm_flag";


  int NOTIFICATION_ID = 2010;

}
