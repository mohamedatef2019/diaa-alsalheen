package grand.diaaalsalheen.azan.scheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.util.Log;
import androidx.legacy.content.WakefulBroadcastReceiver;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TimeZone;
import grand.diaaalsalheen.RingAlarmActivity;
import grand.diaaalsalheen.azan.Constants;
import grand.diaaalsalheen.azan.util.AppSettings;
import grand.diaaalsalheen.azan.util.PrayTime;


public class SalaatAlarmReceiver extends WakefulBroadcastReceiver implements Constants {


    private AlarmManager alarmMgr;

    private PendingIntent alarmIntent;

    @Override
    public void onReceive(Context context, Intent intent) {


        String prayerName = "Atef";

        boolean timePassed = true;

        AppSettings settings = AppSettings.getInstance(context);





        Intent service = new Intent(context, SalaatSchedulingService.class);
        service.putExtra(EXTRA_PRAYER_NAME, prayerName);


        if (settings.isAlarmSetFor(0)) {

                // Start the service, keeping the device awake while it is launching.
                startWakefulService(context, service);


                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        context.startForegroundService(service);
                    } else {
                        context.startService(service);
                    }
                }catch (Exception e){
                    e.getStackTrace();
                }

                // END_INCLUDE(alarm_onreceive)

                // START THE ALARM ACTIVITY
                Intent newIntent = new Intent(context, RingAlarmActivity.class);
                newIntent.putExtra(EXTRA_PRAYER_NAME, prayerName);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(newIntent);

        }
        //SET THE NEXT ALARM
        setAlarm(context);

    }

    int it = 3;

    public void setAlarm(Context context) {
        alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SalaatAlarmReceiver.class);

        Calendar now = Calendar.getInstance(TimeZone.getDefault());
        now.setTimeInMillis(System.currentTimeMillis());
        // Set the alarm's trigger time to 8:30 a.m.


        int alarmIndex = 0;

        Calendar then = Calendar.getInstance(TimeZone.getDefault());
        then.setTimeInMillis(System.currentTimeMillis());

        AppSettings settings = AppSettings.getInstance(context);

        double lat = settings.getLatFor(alarmIndex);
        double lng = settings.getLngFor(alarmIndex);


        LinkedHashMap<String, String> prayerTimes = PrayTime.getPrayerTimes(context, alarmIndex, lat, lng, PrayTime.TIME_24);
        List<String> prayerNames = new ArrayList<>(prayerTimes.keySet());

        boolean nextAlarmFound = false;

        for (String prayer : prayerNames) {

            then = getCalendarFromPrayerTime(then, prayerTimes.get(prayer), it);
            it += 2;

            if (then.after(now)) {
                // this is the alarm to set
                nextAlarmFound = true;
                break;
            }


        }


        if (!nextAlarmFound) {
            for (String prayer : prayerNames) {


                then = getCalendarFromPrayerTime(then, prayerTimes.get(prayer), it);
                it++;

                if (then.before(now)) {
                    // this is the next day.

                    nextAlarmFound = true;
                    then.add(Calendar.DAY_OF_YEAR, 1);
                    break;
                }
            }
        }

        if (!nextAlarmFound) {
            return; //something went wrong, abort!
        }


        intent.putExtra(EXTRA_PRAYER_TIME, then.getTimeInMillis());
        alarmIntent = PendingIntent.getBroadcast(context, ALARM_ID, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            //lollipop_mr1 is 22, this is only 23 and above
            alarmMgr.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, then.getTimeInMillis(), alarmIntent);
        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
            //JB_MR2 is 18, this is only 19 and above.
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, then.getTimeInMillis(), alarmIntent);
        } else {
            //available since api1
            alarmMgr.set(AlarmManager.RTC_WAKEUP, then.getTimeInMillis(), alarmIntent);
        }

        // SET PASSIVE LOCATION RECEIVER
        Intent passiveIntent = new Intent(context, PassiveLocationChangedReceiver.class);
        PendingIntent locationListenerPassivePendingIntent = PendingIntent.getActivity(context, PASSIVE_LOCATION_ID, passiveIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        requestPassiveLocationUpdates(context, locationListenerPassivePendingIntent);

        // Enable {@code SampleBootReceiver} to automatically restart the alarm when the
        // device is rebooted.
        ComponentName receiver = new ComponentName(context, SalaatBootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

        Log.e("pass", "mzbot end");
    }
    // END_INCLUDE(set_alarm)

    /**
     * Cancels the alarm.
     *
     * @param context
     */
    // BEGIN_INCLUDE(cancel_alarm)
    public void cancelAlarm(Context context) {
        // If the alarm has been set, cancel it.
        if (alarmMgr == null) {
            alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        }
        if (alarmMgr != null) {
            if (alarmIntent == null) {
                Intent intent = new Intent(context, SalaatAlarmReceiver.class);
                alarmIntent = PendingIntent.getBroadcast(context, ALARM_ID, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            }
            alarmMgr.cancel(alarmIntent);

            //REMOVE PASSIVE LOCATION RECEIVER
            Intent passiveIntent = new Intent(context, PassiveLocationChangedReceiver.class);
            PendingIntent locationListenerPassivePendingIntent = PendingIntent.getActivity(context, PASSIVE_LOCATION_ID, passiveIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            removePassiveLocationUpdates(context, locationListenerPassivePendingIntent);
        }

        // Disable {@code SampleBootReceiver} so that it doesn't automatically restart the
        // alarm when the device is rebooted.
        ComponentName receiver = new ComponentName(context, SalaatBootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
    // END_INCLUDE(cancel_alarm)


    public void requestPassiveLocationUpdates(Context context, PendingIntent pendingIntent) {
        long oneHourInMillis = 1000 * 60 * 60;
        long fiftyKinMeters = 50000;

        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER,
                    oneHourInMillis, fiftyKinMeters, pendingIntent);
        } catch (SecurityException se) {
            Log.w("SetAlarmReceiver", se.getMessage(), se);
            //do nothing. We should always have permision in order to reach this screen.
        }
    }

    public void removePassiveLocationUpdates(Context context, PendingIntent pendingIntent) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.removeUpdates(pendingIntent);
        } catch (SecurityException se) {
            //do nothing. We should always have permision in order to reach this screen.
        }
    }

    private int getPrayerIndexFromName(String prayerName) {
        String name = prayerName.toLowerCase();
        char index = name.charAt(0);
        switch (index) {
            case 'f':
                return 0;
            case 'd':
                return 1;
            case 'a':
                return 2;
            case 'm':
                return 3;
            case 'i':
                return 4;
        }
        return -1;
    }

    private String getPrayerKeyFromIndex(AppSettings settings, int prayerIndex, int index) {
        String key = null;
        switch (prayerIndex) {
            case 0:
                key = settings.getKeyFor(AppSettings.Key.IS_FAJR_ALARM_SET, index);
                break;
            case 1:
                key = settings.getKeyFor(AppSettings.Key.IS_DHUHR_ALARM_SET, index);
                break;
            case 2:
                key = settings.getKeyFor(AppSettings.Key.IS_ASR_ALARM_SET, index);
                break;
            case 3:
                key = settings.getKeyFor(AppSettings.Key.IS_MAGHRIB_ALARM_SET, index);
                break;
            case 4:
                key = settings.getKeyFor(AppSettings.Key.IS_ISHA_ALARM_SET, index);
                break;
        }
        return key;
    }

    private String getPrayerNameFromIndex(Context context, int prayerIndex) {
        String prayerName = null;
        switch (prayerIndex) {
            case 0:
                prayerName = "Fajr";
                break;
            case 1:
                prayerName = "dhuhr";
                break;
            case 2:
                prayerName = "asr";
                break;
            case 3:
                prayerName = "maghrib";
            case 4:
                prayerName = "isha";
                break;
        }
        return prayerName;
    }

    private boolean isAlarmEnabledForPrayer(AppSettings settings, String prayer, int alarmIndex) {
        if (prayer.equalsIgnoreCase("sunrise") || prayer.equalsIgnoreCase("sunset")) {
            return false;
        }

        int prayerIndex = getPrayerIndexFromName(prayer);
        String key = getPrayerKeyFromIndex(settings, prayerIndex, alarmIndex);
        return settings.getBoolean(key);
    }

    private Calendar getCalendarFromPrayerTime(Calendar cal, String prayerTime, int i) {
        String[] time = prayerTime.split(":");
        cal.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));//
        cal.set(Calendar.MINUTE, Integer.valueOf(time[1]));//
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal;
    }
}
