
package grand.diaaalsalheen.prayertimes.view;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.diaaalsalheen.PrayerTimes2;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.databinding.ItemPrayerTimesBinding;
import grand.diaaalsalheen.prayertimes.response.PrayerTimesItem;
import grand.diaaalsalheen.prayertimes.viewmodel.PrayerTimesItemViewModel;


public class PrayerTimesAdapter extends RecyclerView.Adapter<PrayerTimesAdapter.ViewHolder> {

    private List<PrayerTimesItem> prayerTimesItems;
    ArrayList<String> prayerTimes;
    private MutableLiveData<PrayerTimesItem> itemsOperationsLiveListener;
    private int nextPrayerIndex;

    public PrayerTimesAdapter(ArrayList<String> prayerTimes,int nextPrayerIndex) {
        this.prayerTimes=prayerTimes;
        prayerTimes.add(ResourcesManager.getString(R.string.label_middle));
        prayerTimesItems = new ArrayList<>();
        this.nextPrayerIndex = nextPrayerIndex;
        getItems();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_prayer_times,
                new FrameLayout(parent.getContext()), false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        PrayerTimesItem prayerTimesItem = prayerTimesItems.get(position);

        PrayerTimesItemViewModel prayerTimesItemViewModel = new PrayerTimesItemViewModel(prayerTimesItem,nextPrayerIndex==position,position!=1&&position!=6);
        prayerTimesItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {

            Log.e("PraerID",prayerTimesItem.getId()+"");

            prayerTimesItem.setActive(!prayerTimesItem.isActive());
            UserPreferenceHelper.setPrayerAvailable(prayerTimesItem.getId(),prayerTimesItem.isActive());
            notifyItemChanged(position);
        });
        holder.setViewModel(prayerTimesItemViewModel);
    }



    public MutableLiveData<PrayerTimesItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.prayerTimesItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }




    public void getItems() {

        PrayerTimes2 prayTime = new PrayerTimes2();
        prayTime.setCalcMethod(PrayerTimes2.Calculation.Jafari);


        int counter=0;
        for(int i=0; i<prayerTimes.size(); i++) {
            if(i!=4) {
                counter++;
                prayerTimesItems.add(new PrayerTimesItem(i,prayerTimes.get(i), prayTime.getTimeNames().get(i), UserPreferenceHelper.getPrayerAvailable(i)));
            }
        }


    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ItemPrayerTimesBinding itemPrayerTimesBinding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemPrayerTimesBinding == null) {
                itemPrayerTimesBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemPrayerTimesBinding != null) {
                itemPrayerTimesBinding.unbind();
            }
        }

        void setViewModel(PrayerTimesItemViewModel prayerTimesItemViewModel) {
            if (itemPrayerTimesBinding != null) {
                itemPrayerTimesBinding.setItemPrayerTimesViewModel(prayerTimesItemViewModel);
            }
        }


    }



}
