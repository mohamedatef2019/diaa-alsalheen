package grand.diaaalsalheen.prayertimes.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.UserLocation;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentPrayerTimesBinding;
import grand.diaaalsalheen.prayertimes.viewmodel.PrayerTimesViewModel;


public class PrayerTimesFragment extends BaseFragment {

    View rootView;
    private PrayerTimesViewModel prayerTimesViewModel;
    private  FragmentPrayerTimesBinding fragmentPrayerTimesBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentPrayerTimesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_prayer_times, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
    }

    private void binding() {
        rootView = fragmentPrayerTimesBinding.getRoot();
        UserLocation userLocation = UserPreferenceHelper.getUserLocation();
        if(userLocation!=null) {
            prayerTimesViewModel = new PrayerTimesViewModel(userLocation.getLat(), userLocation.getLng());
        }
        fragmentPrayerTimesBinding.setPrayerTimesViewModel(prayerTimesViewModel);

    }





}