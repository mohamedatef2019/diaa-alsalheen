package grand.diaaalsalheen.prayertimes.response;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.utils.ResourcesManager;

public class PrayerTimesItem {

    private String timeString,time, name;
    private boolean isActive;
    private int id;



    public PrayerTimesItem(int id,String time, String name, boolean isActive) {
        this.time = time;
        this.name = name;
        this.isActive = isActive;
        this.id=id;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setActive(boolean active) {
        isActive = active;
    }



    public String getName() {
        return name;
    }

    public String getTime() {
        return time;
    }

    public String getTimeString() {

        try{
            String []splitedTime=time.split(":");
            int hour=Integer.parseInt(splitedTime[0]);
            String amOrPm = ResourcesManager.getString(R.string.label_am);

            if(hour>12){
                hour-=12;
                amOrPm = ResourcesManager.getString(R.string.label_pm);
            }

            if(hour==0){
                amOrPm = ResourcesManager.getString(R.string.label_after_noon);
            }

            timeString = String.format("%02d", hour)+":"+splitedTime[1]+" "+amOrPm;

        }catch (Exception e){
            timeString = "12:00"+" "+ResourcesManager.getString(R.string.label_am);
            e.getStackTrace();
        }

        return timeString;
    }

    public void setTimeString(String timeString) {

        this.timeString = timeString;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
