package grand.diaaalsalheen.prayertimes.viewmodel;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import grand.diaaalsalheen.home.viewmodel.HomeViewModel;
import grand.diaaalsalheen.prayertimes.view.PrayerTimesAdapter;

public class PrayerTimesViewModel extends HomeViewModel {

    private PrayerTimesAdapter prayerTimesAdapter;

    public PrayerTimesViewModel(double lat, double lng) {
        super(lat, lng);
        prayerTimesAdapter= new PrayerTimesAdapter(getPrayerTimes(),getNextPrayerIndex());
    }


    @BindingAdapter({"app:adapter"})
    public static void getPrayerTimesBinding(RecyclerView recyclerView, PrayerTimesAdapter prayerTimesAdapter) {
        if (recyclerView.getAdapter() == null) {

            recyclerView.setAdapter(prayerTimesAdapter);
        }
    }


    public PrayerTimesAdapter getPrayerTimesAdapter() {
        return prayerTimesAdapter;
    }

}
