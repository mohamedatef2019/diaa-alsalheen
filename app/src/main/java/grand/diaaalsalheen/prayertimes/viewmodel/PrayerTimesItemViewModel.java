
package grand.diaaalsalheen.prayertimes.viewmodel;

import androidx.databinding.Bindable;

import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.prayertimes.response.PrayerTimesItem;


public class PrayerTimesItemViewModel extends BaseItemViewModel {
    private PrayerTimesItem prayerTimesItem;
    private boolean isNextPrayer,isSoundActive;

    public PrayerTimesItemViewModel(PrayerTimesItem prayerTimesItem,boolean isNextPrayer,boolean isSoundActive) {
        this.prayerTimesItem = prayerTimesItem;
        this.isNextPrayer=isNextPrayer;
        this.isSoundActive=isSoundActive;
    }


    public boolean isNextPrayer() {
        return isNextPrayer;
    }

    public void setNextPrayer(boolean nextPrayer) {
        isNextPrayer = nextPrayer;
    }

    public void setSoundActive(boolean soundActive) {
        isSoundActive = soundActive;
    }

    public boolean isSoundActive() {
        return isSoundActive;
    }

    @Bindable
    public PrayerTimesItem getPrayerTimesItem() {
        return prayerTimesItem;
    }



    public void onItemClick(){




        getItemsOperationsLiveListener().setValue(null);
    }
}
