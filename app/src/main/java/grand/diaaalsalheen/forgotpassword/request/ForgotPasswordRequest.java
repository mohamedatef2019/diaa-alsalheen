package grand.diaaalsalheen.forgotpassword.request;


import com.google.gson.annotations.SerializedName;

public class ForgotPasswordRequest {

    @SerializedName("phone")
	private String phone;


    @SerializedName("email")
    private String email;



	private String error;
	private String type = "user";

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}


}