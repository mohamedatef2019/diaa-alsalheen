package grand.diaaalsalheen.forgotpassword.viewmodel;

import android.view.View;

import com.android.volley.Request;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.forgotpassword.request.ForgotPasswordRequest;
import grand.diaaalsalheen.forgotpassword.response.ForgotPasswordResponse;



public class ForgotPasswordViewModel extends BaseViewModel {
    private ForgotPasswordRequest sendCodeRequest;


    public ForgotPasswordViewModel( ) {

        sendCodeRequest = new ForgotPasswordRequest();
    }


    public void sendCodeClick() {
            sendCodeForgotPassword();
    }


    public ForgotPasswordRequest getSendCodeRequest() {
        return sendCodeRequest;
    }



    private void sendCodeForgotPassword() {

        accessLoadingBar(View.VISIBLE);
        notifyChange();
        sendCodeRequest.setEmail(sendCodeRequest.getPhone());
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                ForgotPasswordResponse sendCodeResponse = (ForgotPasswordResponse) response;
                if (sendCodeResponse.getStatus().equals(WebServices.SUCCESS)) {
                    getClicksMutableLiveData().setValue(Codes.SEND_CODE_SCREEN);
                }else {
                    setReturnedMessage(sendCodeResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.FORGOT_PASSWORD, sendCodeRequest, ForgotPasswordResponse.class);
    }


}
