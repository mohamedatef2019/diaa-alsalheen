package grand.diaaalsalheen.forgotpassword.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentPhoneBinding;
import grand.diaaalsalheen.forgotpassword.viewmodel.ForgotPasswordViewModel;


public class ForgotPasswordFragment extends BaseFragment {

    private View rootView;
    private ForgotPasswordViewModel forgotPasswordViewModel;
    private FragmentPhoneBinding fragmentPhoneBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentPhoneBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_phone, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentPhoneBinding.getRoot();
        forgotPasswordViewModel = new ForgotPasswordViewModel();
        fragmentPhoneBinding.setForgotPasswordViewModel(forgotPasswordViewModel);
    }


    private void liveDataListeners() {
        forgotPasswordViewModel.getClicksMutableLiveData().observe(this, result -> {

            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.SEND_CODE_SCREEN) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(Params.IS_FORGOT_PASSWORD, true);
                bundle.putString(Params.BUNDLE_PHONE, forgotPasswordViewModel.getSendCodeRequest().getPhone());
                MovementManager.startActivity(getActivity(), result, bundle);

            } else {

                showMessage(forgotPasswordViewModel.getReturnedMessage());
            }

        });
    }


}