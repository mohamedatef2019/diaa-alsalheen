package grand.diaaalsalheen;

import com.google.gson.annotations.SerializedName;

public class QuranPage {


   private int from, to;
    private String ayat;

    public QuranPage(String ayat,int from, int to) {
        this.from=from;
        this.to=to;
        this.ayat=ayat;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public String getAyat() {
        return ayat;
    }
}