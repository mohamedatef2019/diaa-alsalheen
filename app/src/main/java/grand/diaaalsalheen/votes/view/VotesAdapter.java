
package grand.diaaalsalheen.votes.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.databinding.ItemVoteBinding;
import grand.diaaalsalheen.muslimcalender.response.MonsbatItem;
import grand.diaaalsalheen.votes.VotesItem;
import grand.diaaalsalheen.votes.viewmodel.VotesItemViewModel;


public class VotesAdapter extends RecyclerView.Adapter<VotesAdapter.ViewHolder> {

    private List<VotesItem> votesItems;
    private int currentPosition = 0;


    private MutableLiveData<VotesItem> itemsOperationsLiveListener;


    public VotesAdapter() {
        String[] strings = ResourcesManager.getStringArr(R.array.rate_items);

        votesItems = new ArrayList<>();
    }


    public void updateData(@Nullable List<VotesItem> votesItems) {
        this.votesItems.clear();
        this.votesItems.addAll(votesItems);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vote,
                new FrameLayout(parent.getContext()), false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        VotesItem readerItem = votesItems.get(position);

        VotesItemViewModel votesItemViewModel = new VotesItemViewModel(readerItem,position);
        votesItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
            votesItems.get(currentPosition).setSelected(false);
            notifyItemChanged(currentPosition);
            votesItems.get(position).setSelected(true);
            currentPosition = position;
            notifyItemChanged(position);

        });

      
        holder.setViewModel(votesItemViewModel);
    }



    public MutableLiveData<VotesItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.votesItems.size();
    }

    public  VotesItem  getSelectedItem() {
        return votesItems.get(currentPosition);
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ItemVoteBinding itemVoteBinding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemVoteBinding == null) {
                itemVoteBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemVoteBinding != null) {
                itemVoteBinding.unbind();
            }
        }

        void setViewModel(VotesItemViewModel ayahItemViewModel) {
            if (itemVoteBinding != null) {
                itemVoteBinding.setItemVoteViewModel(ayahItemViewModel);
            }
        }


    }



}
