package grand.diaaalsalheen.votes.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import cn.pedant.SweetAlert.SweetAlertDialog;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.SweetAlertDialogss;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentVotesBinding;
import grand.diaaalsalheen.votes.viewmodel.VotesViewModel;

public class VotesFragment extends BaseFragment {

    private View rootView;
    private FragmentVotesBinding fragmentVotesBinding;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentVotesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_votes, container, false);
        init();
        return rootView;
    }

    private void init() {
        binding();
    }

    private void binding() {
        rootView = fragmentVotesBinding.getRoot();
        VotesViewModel votesViewModel = new VotesViewModel();
        fragmentVotesBinding.setReadersViewModel(votesViewModel);

        votesViewModel.getClicksMutableLiveData().observe(getActivity(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(integer==View.GONE||integer==View.VISIBLE){
                    accessLoadingBar(integer);
                }else if(integer == Codes.SHOW_MESSAGE) {

                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(ResourcesManager.getString(R.string.label_success))
                            .setContentText( votesViewModel.getReturnedMessage())
                            .setConfirmButton(ResourcesManager.getString(R.string.action_submit), sweetAlertDialog -> {
                                sweetAlertDialog.cancel();
                                sweetAlertDialog.dismiss();
                            })
                            .show();
                }
            }
        });

    }


}