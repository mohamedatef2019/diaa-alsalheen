package grand.diaaalsalheen.votes.response;


import com.google.gson.annotations.SerializedName;


public class AnswersItem{

	@SerializedName("id")
	private int id;

	@SerializedName("text")
	private String text;

	@SerializedName("type")
	private String type;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	@Override
 	public String toString(){
		return 
			"AnswersItem{" + 
			"id = '" + id + '\'' + 
			",text = '" + text + '\'' + 
			",type = '" + type + '\'' + 
			"}";
		}
}