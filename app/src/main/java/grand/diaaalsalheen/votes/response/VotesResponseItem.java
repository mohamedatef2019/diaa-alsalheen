package grand.diaaalsalheen.votes.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class VotesResponseItem {

	@SerializedName("question")
	private Question question;

	@SerializedName("answers")
	private List<AnswersItem> answers;

	public void setQuestion(Question question){
		this.question = question;
	}

	public Question getQuestion(){
		return question;
	}

	public void setAnswers(List<AnswersItem> answers){
		this.answers = answers;
	}

	public List<AnswersItem> getAnswers(){
		return answers;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"question = '" + question + '\'' + 
			",answers = '" + answers + '\'' + 
			"}";
		}
}