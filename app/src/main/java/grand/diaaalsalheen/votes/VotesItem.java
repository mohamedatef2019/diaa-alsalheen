package grand.diaaalsalheen.votes;

import com.google.gson.annotations.Expose;

public class VotesItem {
    private int id;
    private String name;


    @Expose
    private boolean isSelected;

    public VotesItem(int id, String name) {
        this.id = id;
        this.name = name;

    }

    public void setId(int id) {
        this.id = id;
    }

    public void setReaderName(String readerName) {
        this.name = readerName;
    }

    public int getId() {
        return id;
    }

    public String getReaderName() {
        return name;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }


}
