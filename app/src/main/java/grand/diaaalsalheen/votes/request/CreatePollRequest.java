package grand.diaaalsalheen.votes.request;


import com.google.gson.annotations.SerializedName;


public class CreatePollRequest {

	@SerializedName("poll_id")
	private int pollId;

    public void setPollId(int pollId) {
        this.pollId = pollId;
    }

    public int getPollId() {
        return pollId;
    }


}