
package grand.diaaalsalheen.votes.viewmodel;

import androidx.databinding.Bindable;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.votes.VotesItem;


public class VotesItemViewModel extends BaseItemViewModel {
    private VotesItem votesItem;
    private int position;

    public VotesItemViewModel(VotesItem votesItem, int position) {
        this.votesItem = votesItem;
        this.position=position;
    }


    @Bindable
    public VotesItem getVotesItem() {
        return votesItem;
    }

    public void onItemItemClicked(){
        getItemsOperationsLiveListener().setValue(null);
    }

    public boolean isLight() {
        return position%2==0;
    }
}
