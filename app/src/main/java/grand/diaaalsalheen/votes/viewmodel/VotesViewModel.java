package grand.diaaalsalheen.votes.viewmodel;

import android.view.View;
import androidx.databinding.Bindable;
import com.android.volley.Request;
import java.util.ArrayList;
import java.util.List;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.votes.VotesItem;
import grand.diaaalsalheen.votes.request.CreatePollRequest;
import grand.diaaalsalheen.votes.response.AnswersItem;
import grand.diaaalsalheen.votes.response.VotesResponse;
import grand.diaaalsalheen.votes.view.VotesAdapter;


public class VotesViewModel extends BaseViewModel {

    private VotesAdapter votesAdapter;

    public VotesViewModel() {
        votesAdapter = new VotesAdapter();
        notifyChange();
        getVotes();
    }


    @Bindable
    public VotesAdapter getVotesAdapter() {
        return votesAdapter;
    }


    public void onVoteClick(){
        goVote();
    }


    private void getVotes() {
        accessLoadingBar(View.VISIBLE);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                VotesResponse votesResponse = (VotesResponse)response;

                List<AnswersItem> answers = votesResponse.getData().getAnswers();
                List<VotesItem> votesItems = new ArrayList<>();

                for(int i=0; i<answers.size(); i++)votesItems.add(new VotesItem(answers.get(i).getId(),answers.get(i).getText()));
                getVotesAdapter().updateData(votesItems);
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject( Request.Method.GET,WebServices.POLLS, null, VotesResponse.class);

    }

    private void goVote() {

        CreatePollRequest createPollRequest = new CreatePollRequest();
        createPollRequest.setPollId(getVotesAdapter().getSelectedItem().getId());

        accessLoadingBar(View.VISIBLE);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
                getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
            }
        }).requestJsonObject( Request.Method.POST,WebServices.CREATE_POLLS, createPollRequest, VotesResponse.class);

    }

}
