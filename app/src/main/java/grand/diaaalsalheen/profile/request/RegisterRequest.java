package grand.diaaalsalheen.profile.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterRequest {

	@SerializedName("password")
	private String password;

	@SerializedName("city")
	private String city;

	@SerializedName("city_id")
	private String cityId;


	@SerializedName("role")
	private String role="worker";

	@SerializedName("address")
	private String address="address not available yet";

	@SerializedName("lat")
	private String lat;

	@SerializedName("lng")
	private String lng;

	@SerializedName("phone")
	private String phone;

	@SerializedName("name")
	private String name;

	@SerializedName("email")
	private String email;

	@SerializedName("user_id")
	private String workerId;

	@Expose
	@SerializedName("user_image")
	private String userImage;


	public String getUserImage() {
		return userImage;
	}

	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setRole(String role){
		this.role = role;
	}

	public String getRole(){
		return role;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setLng(String lng){
		this.lng = lng;
	}

	public String getLng(){
		return lng;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setLat(String lat){
		this.lat = lat;
	}

	public String getLat(){
		return lat;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	public String getWorkerId() {
		return workerId;
	}

	public void setWorkerId(String workerId) {
		this.workerId = workerId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCityId() {
		return cityId;
	}

	@Override
 	public String toString(){
		return 
			"RegisterResponse{" + 
			"password = '" + password + '\'' + 
			",role = '" + role + '\'' + 
			",address = '" + address + '\'' + 
			",lng = '" + lng + '\'' + 
			",phone = '" + phone + '\'' + 
			",name = '" + name + '\'' + 
			",email = '" + email + '\'' + 
			",lat = '" + lat + '\'' + 
			"}";
		}
}