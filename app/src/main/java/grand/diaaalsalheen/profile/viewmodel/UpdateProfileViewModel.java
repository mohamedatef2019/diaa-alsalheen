package grand.diaaalsalheen.profile.viewmodel;

import android.view.View;

import java.util.ArrayList;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.filesutils.VolleyFileObject;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.base.volleyutils.MyApplication;
import grand.diaaalsalheen.login.response.UserItem;
import grand.diaaalsalheen.login.response.UserResponse;
import grand.diaaalsalheen.register.request.RegisterRequest;

public class UpdateProfileViewModel extends BaseViewModel {
    private RegisterRequest registerRequest;
     private ArrayList<VolleyFileObject> volleyFileObjects;
    private UserItem userItem;

    public UpdateProfileViewModel( UserItem userItem) {
        this.userItem = userItem;
        registerRequest = new RegisterRequest();
        volleyFileObjects = new ArrayList<>();

        setProfileData();

    }

    private void setProfileData(){
        registerRequest.setName(userItem.getUsersName());
        registerRequest.setPhone(userItem.getPhone());
        registerRequest.setEmail(userItem.getEmail());
        registerRequest.setLat(userItem.getLat());
        registerRequest.setLng(userItem.getLng());

        notifyChange();
    }

    public void uploadPhotoClick() {
        getClicksMutableLiveData().setValue(Codes.SELECT_PROFILE_IMAGE);
    }

    public RegisterRequest getRegisterRequest() {
        return registerRequest;
    }

    @Override
    public String getReturnedMessage() {
        return super.getReturnedMessage();
    }

    public void setRegisterRequest(RegisterRequest registerRequest) {
        this.registerRequest = registerRequest;
    }

    public void selectLocationClick() {
        getClicksMutableLiveData().setValue(Codes.SELECT_LOCATION);
    }


    public ArrayList<VolleyFileObject> getVolleyFileObjects() {
        return volleyFileObjects;
    }


    public void registerClick() {
        goRegister();
    }



    public void changePasswordClick() {
        getClicksMutableLiveData().setValue(Codes.CHANGE_PASSWORD_SCREEN);
    }

    private void goRegister() {

        accessLoadingBar(View.VISIBLE);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
                UserResponse userResponse = (UserResponse) response;
                if (userResponse.getStatus().equals(WebServices.SUCCESS)) {
                    UserItem userItem = userResponse.getData();
                    userItem.setPassword(UpdateProfileViewModel.this.userItem.getPassword());
                    UserPreferenceHelper.saveUserDetails(userItem);
                    setReturnedMessage(userResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                    getClicksMutableLiveData().setValue(Codes.DATA_UPDATED);
                } else if (userResponse.getStatus().equals(WebServices.FAILED)) {
                    setReturnedMessage(userResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                }

            }
        }).multiPartConnect(WebServices.UPDATE_PROFILE, registerRequest, volleyFileObjects, UserResponse.class);
    }






}
