package grand.diaaalsalheen.profile.view;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import java.util.Objects;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.filesutils.FileOperations;
import grand.diaaalsalheen.base.filesutils.VolleyFileObject;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.activities.BaseActivity;
import grand.diaaalsalheen.base.views.activities.MainActivity;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.databinding.FragmentUpdateProfileBinding;
import grand.diaaalsalheen.login.response.UserItem;
import grand.diaaalsalheen.profile.viewmodel.UpdateProfileViewModel;


public class UpdateProfileFragment extends BaseFragment {

    private View rootView;
    private UpdateProfileViewModel registerViewModel;
    private  FragmentUpdateProfileBinding fragmentUpdateProfileBinding;
    private UserItem userItem;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentUpdateProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_update_profile, container, false);

        requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},1231);

        userItem = UserPreferenceHelper.getUserDetails();
        init();

        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentUpdateProfileBinding.getRoot();
        registerViewModel = new UpdateProfileViewModel(userItem);
        fragmentUpdateProfileBinding.setRegisterViewModel(registerViewModel);
        ConnectionHelper.loadImage(fragmentUpdateProfileBinding.profileImage,userItem.getUsersImg());
    }

    public UserItem getUserItem() {
        return userItem;
    }

    private void liveDataListeners() {
        registerViewModel.getClicksMutableLiveData().observe(this, result -> {

            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.SELECT_PROFILE_IMAGE) {
                FileOperations.pickImage(getActivity());
            } else if (result == Codes.SELECT_COMMERICAL_IMAGE) {
                FileOperations.pickFile(getActivity());
            } else if (result == Codes.CHANGE_PASSWORD_SCREEN) {
                MovementManager.startActivity(getActivity(), result);
            } else if (result == Codes.SHOW_MESSAGE) {
                showMessage(registerViewModel.getReturnedMessage());
            } else if (result == Codes.SELECT_LOCATION) {
                MovementManager.startActivityForResult(getActivity(), BaseActivity.class, Codes.SELECT_LOCATION, Codes.SELECT_LOCATION);
            }  else if (result == Codes.DATA_UPDATED) {

                try{
                    ((MainActivity) Objects.requireNonNull(getActivity())).getNavigationDrawerView().setUserDetails();
                }catch (Exception e){
                    e.getStackTrace();
                }

            }else {
                MovementManager.startActivity(getActivity(), result);
            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        super.onActivityResult(requestCode, resultCode, data);

            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, WebServices.IMAGE, Codes.FILE_TYPE_IMAGE);
            fragmentUpdateProfileBinding.profileImage.setImageBitmap(Objects.requireNonNull(volleyFileObject).getCompressObject().getImage());
            registerViewModel.getVolleyFileObjects().add(volleyFileObject);

    }





}