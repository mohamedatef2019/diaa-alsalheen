package grand.diaaalsalheen.beads.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.beads.viewmodel.TasbehViewModel;
import grand.diaaalsalheen.databinding.FragmentTasbehBinding;


public class TasbehFragment extends BaseFragment {
    private View rootView;

    private FragmentTasbehBinding fragmentTasbehBinding;
    private TasbehViewModel tasbehViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentTasbehBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_tasbeh, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();


    }

    private void binding() {
        rootView = fragmentTasbehBinding.getRoot();
        tasbehViewModel = new TasbehViewModel();
        fragmentTasbehBinding.setTasbehViewModel(tasbehViewModel);
    }


}