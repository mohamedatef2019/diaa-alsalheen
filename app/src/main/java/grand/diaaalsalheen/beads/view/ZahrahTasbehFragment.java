package grand.diaaalsalheen.beads.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.aboutus.viewmodel.AboutUsViewModel;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.beads.viewmodel.ZahrahTasbehViewModel;
import grand.diaaalsalheen.databinding.FragmentBeadsMainBinding;
import grand.diaaalsalheen.databinding.FragmentZahrahTasbehBinding;


public class ZahrahTasbehFragment extends BaseFragment {
    private View rootView;

    private FragmentZahrahTasbehBinding fragmentZahrahTasbehBinding;
    private   ZahrahTasbehViewModel zahrahTasbehViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentZahrahTasbehBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_zahrah_tasbeh, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();

        zahrahTasbehViewModel = new ZahrahTasbehViewModel();
        fragmentZahrahTasbehBinding.setTasbehViewModel(zahrahTasbehViewModel);

    }

    private void binding() {
        rootView = fragmentZahrahTasbehBinding.getRoot();
    }





}