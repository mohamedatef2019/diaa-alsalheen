package grand.diaaalsalheen.beads.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentBeadsMainBinding;


public class MainBeadsFragment extends BaseFragment {
    private View rootView;

    private FragmentBeadsMainBinding fragmentBeadsMainBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentBeadsMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_beads_main, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getChildFragmentManager(), FragmentPagerItems.with(getActivity())
                .add(R.string.label_zahrah_beads, ZahrahTasbehFragment.class)
                .add(R.string.label_beads, TasbehFragment.class)
                .create());

        fragmentBeadsMainBinding.viewpager.setAdapter(adapter);
        fragmentBeadsMainBinding.viewpagertab.setViewPager(fragmentBeadsMainBinding.viewpager);

    }

    private void binding() {
        rootView = fragmentBeadsMainBinding.getRoot();
    }


}