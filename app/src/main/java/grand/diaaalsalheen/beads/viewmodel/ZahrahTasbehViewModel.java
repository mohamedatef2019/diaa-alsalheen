
package grand.diaaalsalheen.beads.viewmodel;

import androidx.databinding.Bindable;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.doaa.response.CategoriesItem;


public class ZahrahTasbehViewModel extends BaseItemViewModel {
    protected int count=0;
    private int currentZekr=0;
    private String []azkar = {ResourcesManager.getString(R.string.label_allah_akber),ResourcesManager.getString(R.string.label_alhmd_lel_allah),ResourcesManager.getString(R.string.label_sobhan_allah)};
    private int []azkarTimes = {34,33,33};

    public ZahrahTasbehViewModel() {

    }


    public String getCount() {
        return count+"";
    }

    public String getCurrentZekr() {
        return azkar[currentZekr%3];
    }

    public void onTasbehClick(){
        count++;
        if(count==azkarTimes[currentZekr%3]){
            count=0;
            currentZekr++;
        }
        notifyChange();
    }

    public void onReloadClick(){
        count=0;
        notifyChange();
    }

    public String getCurrentZekrCount(){
        return azkarTimes[currentZekr%3]+"";
    }
}