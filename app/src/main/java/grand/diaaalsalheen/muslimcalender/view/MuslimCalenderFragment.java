package grand.diaaalsalheen.muslimcalender.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.github.eltohamy.materialhijricalendarview.CalendarDay;
import com.github.eltohamy.materialhijricalendarview.MaterialHijriCalendarView;
import com.github.eltohamy.materialhijricalendarview.OnDateSelectedListener;
import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.google.android.gms.maps.model.LatLng;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import grand.diaaalsalheen.MediaPlayerUtils;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentBeadsMainBinding;
import grand.diaaalsalheen.databinding.FragmentMuslimCalenderBinding;
import grand.diaaalsalheen.muslimcalender.viewmodel.MuslimCalenderViewModel;


public class MuslimCalenderFragment extends BaseFragment {
    private View rootView;
    MuslimCalenderViewModel muslimCalenderViewModel;
    private FragmentMuslimCalenderBinding fragmentMuslimCalenderBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMuslimCalenderBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_muslim__calender, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
    }

    private void binding() {
        rootView = fragmentMuslimCalenderBinding.getRoot();

        UmmalquraCalendar cal = new UmmalquraCalendar();


        try {
            cal.add(Calendar.DAY_OF_MONTH, Integer.parseInt(UserPreferenceHelper.getCurrentTime(context)));
        }catch (Exception e){
            e.getStackTrace();
        }

        fragmentMuslimCalenderBinding.calendarView.setSelectedDate(cal);


        muslimCalenderViewModel = new MuslimCalenderViewModel();
        fragmentMuslimCalenderBinding.setMuslimCalenderViewModel(muslimCalenderViewModel);

        fragmentMuslimCalenderBinding.calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialHijriCalendarView widget, @NonNull CalendarDay calendarDay, boolean selected) {
                Calendar cal = calendarDay.getCalendar();
                Date dates = cal.getTime();
                Calendar now = Calendar.getInstance();
                now.setTime(dates);

                muslimCalenderViewModel.getMonsbats( (now.get(Calendar.YEAR)) + "-" + ((now.get(Calendar.MONTH) + 1) < 10 ? "0" : "") + (now.get(Calendar.MONTH) + 1) + "-" + (now.get(Calendar.DAY_OF_MONTH) < 10 ? "0" : "") +  now.get(Calendar.DAY_OF_MONTH));            }
        });


        liveDataListeners();
    }


    private void liveDataListeners() {
        muslimCalenderViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });


    }


}