
package grand.diaaalsalheen.muslimcalender.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.databinding.ItemDoaaCategoryBinding;
import grand.diaaalsalheen.databinding.ItemMonsbaBinding;

import grand.diaaalsalheen.muslimcalender.response.MonsbatItem;
import grand.diaaalsalheen.muslimcalender.viewmodel.MuslimCalenderItemViewModel;


public class MonsbatAdapter extends RecyclerView.Adapter<MonsbatAdapter.CompaniesViewHolder> {

    private List<MonsbatItem> monsbatItems;
    private MutableLiveData<MonsbatItem> itemsOperationsLiveListener;
    private int currentPosition = 0;

    public MonsbatAdapter() {
        this.monsbatItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_monsba,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {
        MonsbatItem adiaCategoryItem = monsbatItems.get(position);
        MuslimCalenderItemViewModel companyItemViewModel = new MuslimCalenderItemViewModel(adiaCategoryItem);
        companyItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
            notifyItemChanged(position);

            itemsOperationsLiveListener.setValue(adiaCategoryItem);
        });
        holder.setViewModel(companyItemViewModel);
    }


    public MutableLiveData<MonsbatItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.monsbatItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<MonsbatItem> data) {
        this.monsbatItems.clear();
        this.monsbatItems.addAll(data);
        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemMonsbaBinding itemMonsbaBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMonsbaBinding == null) {
                itemMonsbaBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMonsbaBinding != null) {
                itemMonsbaBinding.unbind();
            }
        }

        void setViewModel(MuslimCalenderItemViewModel muslimCalenderItemViewModel) {
            if (itemMonsbaBinding != null) {
                itemMonsbaBinding.setItemMonsbaViewModel(muslimCalenderItemViewModel);
            }
        }


    }


}
