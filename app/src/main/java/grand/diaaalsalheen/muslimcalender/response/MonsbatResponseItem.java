package grand.diaaalsalheen.muslimcalender.response;

import com.google.gson.annotations.SerializedName;

public class MonsbatResponseItem {

	@SerializedName("second_one")
	private MonsbatItem secondOne;

	@SerializedName("first_one")
	private MonsbatItem firstOne;

	public void setSecondOne(MonsbatItem secondOne){
		this.secondOne = secondOne;
	}

	public MonsbatItem getSecondOne(){
		return secondOne;
	}

	public void setFirstOne(MonsbatItem firstOne){
		this.firstOne = firstOne;
	}

	public MonsbatItem getFirstOne(){
		return firstOne;
	}
}