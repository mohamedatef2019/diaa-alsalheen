package grand.diaaalsalheen.muslimcalender.response;

 import com.google.gson.annotations.Expose;
 import com.google.gson.annotations.SerializedName;


public class MonsbatItem {

	@SerializedName("date")
	private String date;

	@SerializedName("name")
	private String name;

    @Expose
    private String title;



    @SerializedName("desc")
    private String desc;

	@SerializedName("id")
	private int id;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }


    @Override
 	public String toString(){
		return 
			"DataItem{" + 
			"date = '" + date + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}