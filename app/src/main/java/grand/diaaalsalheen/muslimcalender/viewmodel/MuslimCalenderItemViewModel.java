
package grand.diaaalsalheen.muslimcalender.viewmodel;

import androidx.databinding.Bindable;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.doaa.response.CategoriesItem;
import grand.diaaalsalheen.muslimcalender.response.MonsbatItem;


public class MuslimCalenderItemViewModel extends BaseItemViewModel {
    private MonsbatItem monsbatItem;


    public MuslimCalenderItemViewModel(MonsbatItem monsbatItem) {
        this.monsbatItem = monsbatItem;
    }


    @Bindable
    public MonsbatItem getMonsbatItem() {
        return monsbatItem;
    }



}
