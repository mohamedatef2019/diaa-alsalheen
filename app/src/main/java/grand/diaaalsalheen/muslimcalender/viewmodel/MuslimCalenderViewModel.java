package grand.diaaalsalheen.muslimcalender.viewmodel;

import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import java.util.ArrayList;
import java.util.List;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.home.viewmodel.ImagesViewModel;
import grand.diaaalsalheen.muslimcalender.response.MonsbatItem;
import grand.diaaalsalheen.muslimcalender.response.MonsbatResponse;
import grand.diaaalsalheen.muslimcalender.view.MonsbatAdapter;
import grand.diaaalsalheen.tasks.viewmodel.TasksViewModel;


public class MuslimCalenderViewModel extends ImagesViewModel {

    private MonsbatAdapter monsbatAdapter;


    public MuslimCalenderViewModel() {
        getMonsbats(TasksViewModel.getCurrentDateString2());
    }


    @BindingAdapter({"app:adapter"})
    public static void getMonsbatBinding(RecyclerView recyclerView, MonsbatAdapter monsbatAdapter) {
        recyclerView.setAdapter(monsbatAdapter);
    }


    public MonsbatAdapter getMonsbatAdapter() {
        return monsbatAdapter = (monsbatAdapter==null?new MonsbatAdapter():monsbatAdapter);
    }

    public void getMonsbats(String date) {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                MonsbatResponse monsbatResponse = (MonsbatResponse) response;

                List<MonsbatItem> data = new ArrayList<>();

                if(monsbatResponse.getData().getFirstOne()!=null) {
                    data.add(monsbatResponse.getData().getFirstOne());
                    data.get(0).setTitle(ResourcesManager.getString(R.string.label_current));
                }

                if(monsbatResponse.getData().getSecondOne()!=null) {
                    monsbatResponse.getData().getSecondOne().setTitle(ResourcesManager.getString(R.string.label_next));
                    data.add(monsbatResponse.getData().getSecondOne());

                }

                getMonsbatAdapter().updateData(data);
                accessLoadingBar(View.GONE);
                notifyChange();


            }
        }).requestJsonObject(Request.Method.GET, WebServices.MONSBAT+date, null, MonsbatResponse.class);
    }






}
