package grand.diaaalsalheen.aboutus.response;


import com.google.gson.annotations.SerializedName;

public class AboutUsResponse {

    @SerializedName("msg")
    private String msg;

    @SerializedName("data")
    private AboutUsItem data;

    @SerializedName("status")
    private String status;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setData(AboutUsItem data) {
        this.data = data;
    }

    public AboutUsItem getData() {
        return data;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return
                "AboutUsResponse{" +
                        "msg = '" + msg + '\'' +
                        ",data = '" + data + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}