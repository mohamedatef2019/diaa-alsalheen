package grand.diaaalsalheen.aboutus.response;


import com.google.gson.annotations.SerializedName;


public class AboutUsItem {

	@SerializedName("name")
	private String name;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"name = '" + name + '\'' + 
			"}";
		}
}