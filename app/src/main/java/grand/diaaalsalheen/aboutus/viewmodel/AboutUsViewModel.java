package grand.diaaalsalheen.aboutus.viewmodel;


import android.view.View;
import androidx.databinding.Bindable;
import com.android.volley.Request;
import grand.diaaalsalheen.aboutus.response.AboutUsItem;
import grand.diaaalsalheen.aboutus.response.AboutUsResponse;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;


public class AboutUsViewModel extends BaseViewModel {

    private AboutUsItem aboutUsItem;

    public AboutUsViewModel() {
        getAboutUs();
    }


    private void getAboutUs() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                aboutUsItem = ((AboutUsResponse)response).getData();
                notifyChange();
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.ABOUT_US, new Object(), AboutUsResponse.class);
    }

    @Bindable
    public AboutUsItem getAboutUsItem() {
        return aboutUsItem;
    }
}
