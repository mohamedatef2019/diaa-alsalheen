package grand.diaaalsalheen.takeeb.response;

 import com.google.gson.annotations.SerializedName;

public class TakeebDetailsITem {

	@SerializedName("voice")
	private String voice;

	@SerializedName("id")
	private int id;

	@SerializedName("text")
	private String text;

	@SerializedName("taqebat_category_id")
	private int taqebatCategoryId;

	public void setVoice(String voice){
		this.voice = voice;
	}

	public String getVoice(){
		return voice;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	public void setTaqebatCategoryId(int taqebatCategoryId){
		this.taqebatCategoryId = taqebatCategoryId;
	}

	public int getTaqebatCategoryId(){
		return taqebatCategoryId;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"voice = '" + voice + '\'' + 
			",id = '" + id + '\'' + 
			",text = '" + text + '\'' + 
			",taqebat_category_id = '" + taqebatCategoryId + '\'' + 
			"}";
		}
}