package grand.diaaalsalheen.takeeb.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentTakeebBinding;
import grand.diaaalsalheen.takeeb.viewmodel.TakeebViewModel;


public class TakeebCategoriesFragment extends BaseFragment {
    private View rootView;
    private TakeebViewModel takeebViewModel;
    private FragmentTakeebBinding fragmentTakeebBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentTakeebBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_takeeb, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();

    }

    private void binding() {
        rootView = fragmentTakeebBinding.getRoot();
        takeebViewModel = new TakeebViewModel();
        fragmentTakeebBinding.setTakeebViewModel(takeebViewModel);
    }


    private void liveDataListeners() {
        takeebViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }
        });

        takeebViewModel.getTakeebCategoriesAdapter().getItemsOperationsLiveListener().observe(this, takeebCategoryItem -> {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Params.BUNDLE_TAKEEB_ITEM, takeebCategoryItem);
            MovementManager.startActivity(getActivity(), Codes.TAQEEB_DETAILS, bundle);

        });
    }


}