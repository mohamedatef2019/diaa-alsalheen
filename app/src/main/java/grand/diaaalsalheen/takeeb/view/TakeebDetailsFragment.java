package grand.diaaalsalheen.takeeb.view;


import android.os.Bundle;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import org.sufficientlysecure.htmltextview.HtmlFormatter;
import org.sufficientlysecure.htmltextview.HtmlFormatterBuilder;

import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import grand.diaaalsalheen.MediaPlayerUtils;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.SweetAlertDialogss;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;

import grand.diaaalsalheen.databinding.FragmentTakeebDetailsBinding;
import grand.diaaalsalheen.mongat.response.MonagatCategoryItem;
 import grand.diaaalsalheen.takeeb.response.TakeebCategoryItem;
import grand.diaaalsalheen.takeeb.viewmodel.TaqeebDetailsViewModel;


public class TakeebDetailsFragment extends BaseFragment {
    private View rootView;
    private TaqeebDetailsViewModel taqeebDetailsViewModel;
    private FragmentTakeebDetailsBinding fragmentTakeebDetailsBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentTakeebDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_takeeb_details, container, false);
        assert getArguments() != null;

        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();

    }
    int textSize = 0;
    private void binding() {
        rootView = fragmentTakeebDetailsBinding.getRoot();
        assert getArguments() != null;
        taqeebDetailsViewModel = new TaqeebDetailsViewModel((TakeebCategoryItem) getArguments().getSerializable(Params.BUNDLE_TAKEEB_ITEM));
        fragmentTakeebDetailsBinding.setTakeebDetailsViewModel(taqeebDetailsViewModel);
    }


    private void liveDataListeners() {
        taqeebDetailsViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }else if (result == Codes.PLAY_AUDIO) {

              //  MediaPlayerUtils.playAudio(taqeebDetailsViewModel.getTakeebDetailsITem().getVoice(), taqeebDetailsViewModel.getTakeebDetailsITem().getText()
                    //    , getActivity(),fragmentTakeebDetailsBinding.ivPlay);

            }
            else if(result == Codes.ADD_TO_FAVOURITE){
                if(UserPreferenceHelper.isLogined()){
                SweetAlertDialogss.showFavDialog(getActivity());
                }else {
                    SweetAlertDialogss.loginToContinue(getActivity());
                }
            }
            else if (result == Codes.ZOOM_IN) {
                textSize+=5;
                fragmentTakeebDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            } else if (result == Codes.ZOOM_OUT) {
                textSize-=5;
                fragmentTakeebDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            }
            else if (result == Codes.DATA_RECIVED) {
                Spanned formattedHtml = HtmlFormatter.formatHtml(new HtmlFormatterBuilder().setHtml(taqeebDetailsViewModel.getTakeebDetailsITem().getText()));
                String str=taqeebDetailsViewModel.getTakeebDetailsITem().getText();
                fragmentTakeebDetailsBinding.tvHtmlViewer.loadDataWithBaseURL(null, str, "text/html", "utf-8", null);
                textSize =  270;
                fragmentTakeebDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            }

        });


    }




}