
package grand.diaaalsalheen.takeeb.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.databinding.ItemTakeebBinding;
import grand.diaaalsalheen.takeeb.response.TakeebCategoryItem;
import grand.diaaalsalheen.takeeb.viewmodel.TakeebCategoryItemViewModel;


public class TakeebCategoriesAdapter extends RecyclerView.Adapter<TakeebCategoriesAdapter.CompaniesViewHolder> {

    private List<TakeebCategoryItem> takeebCategoryItems;
    private MutableLiveData<TakeebCategoryItem> itemsOperationsLiveListener;


    public TakeebCategoriesAdapter() {
        this.takeebCategoryItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_takeeb,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {
        TakeebCategoryItem adiaCategoryItem = takeebCategoryItems.get(position);
        TakeebCategoryItemViewModel companyItemViewModel = new TakeebCategoryItemViewModel(adiaCategoryItem);
        companyItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
            itemsOperationsLiveListener.setValue(adiaCategoryItem);
        });
        holder.setViewModel(companyItemViewModel);
    }



    public MutableLiveData<TakeebCategoryItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.takeebCategoryItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<TakeebCategoryItem> data) {

            this.takeebCategoryItems.clear();

        assert data != null;
        this.takeebCategoryItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemTakeebBinding itemTakeebBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemTakeebBinding == null) {
                itemTakeebBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemTakeebBinding != null) {
                itemTakeebBinding.unbind();
            }
        }

        void setViewModel(TakeebCategoryItemViewModel takeebCategoryItemViewModel) {
            if (itemTakeebBinding != null) {
                itemTakeebBinding.setItemTakeebViewModel(takeebCategoryItemViewModel);
            }
        }


    }



}
