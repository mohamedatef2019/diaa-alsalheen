
package grand.diaaalsalheen.takeeb.viewmodel;

import androidx.databinding.Bindable;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
 import grand.diaaalsalheen.takeeb.response.TakeebCategoryItem;


public class TakeebCategoryItemViewModel extends BaseItemViewModel {
    private TakeebCategoryItem takeebCategoryItem;


    public TakeebCategoryItemViewModel(TakeebCategoryItem takeebCategoryItem) {
        this.takeebCategoryItem = takeebCategoryItem;
    }


    @Bindable
    public TakeebCategoryItem getTakeebCategoryItem() {
        return takeebCategoryItem;
    }


    public void onTakeebCategoryClick(){
        getItemsOperationsLiveListener().setValue(Codes.ADAIA_SUB_CATEGORIES);
    }


}
