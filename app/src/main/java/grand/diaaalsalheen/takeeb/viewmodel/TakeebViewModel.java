package grand.diaaalsalheen.takeeb.viewmodel;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.doaa.view.AdaiaSubCategoriesAdapter;
import grand.diaaalsalheen.home.viewmodel.ImagesViewModel;
import grand.diaaalsalheen.takeeb.response.TakeebCategoriesResponse;
import grand.diaaalsalheen.takeeb.view.TakeebCategoriesAdapter;


public class TakeebViewModel extends ImagesViewModel {

    private TakeebCategoriesAdapter takeebCategoriesAdapter;

   

    public TakeebViewModel() {
        getAdiaCategories();
    }


    @BindingAdapter({"app:adapter"})
    public static void getTakeebBinding(RecyclerView recyclerView, TakeebCategoriesAdapter adaiaAdapter) {
        recyclerView.setAdapter(adaiaAdapter);
    }





    @Bindable
    public TakeebCategoriesAdapter getTakeebCategoriesAdapter() {
        return this.takeebCategoriesAdapter == null ? this.takeebCategoriesAdapter = new TakeebCategoriesAdapter() : this.takeebCategoriesAdapter;
    }

    

    private void getAdiaCategories() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                TakeebCategoriesResponse doaaResponse = (TakeebCategoriesResponse) response;
                getTakeebCategoriesAdapter().updateData(doaaResponse.getData());

                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.TAQEEB_CATEGORIES, null, TakeebCategoriesResponse.class);
    }


   



}
