package grand.diaaalsalheen.takeeb.viewmodel;

import android.view.View;

import com.android.volley.Request;

import grand.diaaalsalheen.FavouriteRequest;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.favourite.viewmodel.FavouriteViewModel;
import grand.diaaalsalheen.mongat.response.MonagatCategoryItem;
import grand.diaaalsalheen.mongat.response.MongatDetailsItem;
import grand.diaaalsalheen.mongat.response.MongatDetailsResponse;
import grand.diaaalsalheen.takeeb.response.TakeebCategoryItem;
import grand.diaaalsalheen.takeeb.response.TakeebDetailsITem;
import grand.diaaalsalheen.takeeb.response.TaqeebDetailsResponse;


public class TaqeebDetailsViewModel extends BaseViewModel {

    private TakeebDetailsITem takeebDetailsITem;

    public TaqeebDetailsViewModel(TakeebCategoryItem takeebCategoryItem) {
        getMonagats(takeebCategoryItem);
    }

    private void getMonagats(TakeebCategoryItem takeebCategoryItem) {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                TaqeebDetailsResponse taqeebDetailsResponse = (TaqeebDetailsResponse) response;
                takeebDetailsITem = taqeebDetailsResponse.getData();
                accessLoadingBar(View.GONE);
                getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.TAQEEB_DETAILS + takeebCategoryItem.getId(), null, TaqeebDetailsResponse.class);
    }

    public TakeebDetailsITem getTakeebDetailsITem() {
        return takeebDetailsITem;
    }

    public void onZoomInFontClick() {
        getClicksMutableLiveData().setValue(Codes.ZOOM_IN);
    }

    public void onZoomOutFontClick() {
        getClicksMutableLiveData().setValue(Codes.ZOOM_OUT);
    }
}
