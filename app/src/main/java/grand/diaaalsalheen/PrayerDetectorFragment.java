package grand.diaaalsalheen;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import com.github.nisrulz.sensey.ProximityDetector;
import com.github.nisrulz.sensey.Sensey;

import grand.diaaalsalheen.base.utils.ViewOperations;
import grand.diaaalsalheen.databinding.FragmentPrayDetectorBinding;


public class PrayerDetectorFragment extends Fragment {

    private FragmentPrayDetectorBinding fragmentPrayDetectorBinding;


    private boolean doubleBackToExitPressedOnce = true;
    private int r23at = 0;
    private int sgdat = 0;
    private int numberOfR23at=2;
    private ProximityDetector.ProximityListener proximityListener;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup
            container, @Nullable Bundle savedInstanceState) {
        fragmentPrayDetectorBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_pray_detector, container, false);

        int size = ViewOperations.getScreenHeight(getActivity())/4;

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size,size);
        fragmentPrayDetectorBinding.tvRkat.setLayoutParams(params);
        fragmentPrayDetectorBinding.tvRkat.setTextSize(size);
        fragmentPrayDetectorBinding.tvRkat.setTextSize(size/10);

        size = ViewOperations.getScreenHeight(getActivity())/5;
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(size,size);
        fragmentPrayDetectorBinding.tvSgdat.setLayoutParams(params2);

        Log.e("sizesize",size+"");

        fragmentPrayDetectorBinding.tvSgdat.setTextSize(size/10);

        setSensor();
        setEvents();

        selectSlat(fragmentPrayDetectorBinding.tvCurrentSalat, fragmentPrayDetectorBinding.tvFajr,2);
        selectSlat(fragmentPrayDetectorBinding.tvCurrentSalat, fragmentPrayDetectorBinding.tvSunrise,2);
        selectSlat(fragmentPrayDetectorBinding.tvCurrentSalat, fragmentPrayDetectorBinding.tvZohr,4);
        selectSlat(fragmentPrayDetectorBinding.tvCurrentSalat, fragmentPrayDetectorBinding.tvAsr,4);
        selectSlat(fragmentPrayDetectorBinding.tvCurrentSalat, fragmentPrayDetectorBinding.tvMajrib,3);
        selectSlat(fragmentPrayDetectorBinding.tvCurrentSalat, fragmentPrayDetectorBinding.tvAisha,4);

        return fragmentPrayDetectorBinding.getRoot();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Sensey.getInstance().stop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Sensey.getInstance().stop();
    }

    private void selectSlat(TextView baseText, TextView secondText,int numberOfR23at) {

        secondText.setOnClickListener(view -> {
            baseText.setText(secondText.getText());
            PrayerDetectorFragment.this.numberOfR23at=numberOfR23at;
            fragmentPrayDetectorBinding.llSlwatContainer.setVisibility(View.GONE);
        });
    }

    private void setEvents() {
        fragmentPrayDetectorBinding.llPraySlwat.setOnClickListener(view -> {
            fragmentPrayDetectorBinding.llSlwatContainer.setVisibility(fragmentPrayDetectorBinding.llSlwatContainer.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        });
        fragmentPrayDetectorBinding.btnStartPray.setOnClickListener(view -> {
             r23at = 0;
              sgdat = 0;
            fragmentPrayDetectorBinding.tvRkat.setText(r23at + "");
            fragmentPrayDetectorBinding.tvSgdat.setText(sgdat + "");
            Sensey.getInstance().stopProximityDetection(proximityListener);
            Sensey.getInstance().startProximityDetection(proximityListener);
            Toast.makeText(getActivity(), "Start", Toast.LENGTH_SHORT).show();
        });
    }

    private void setSensor() {


         proximityListener = new ProximityDetector.ProximityListener() {
            @Override
            public void onNear() {

                if (doubleBackToExitPressedOnce) {

                    new Handler().postDelayed(() -> doubleBackToExitPressedOnce = true, 2000);
                    if (sgdat == 2) {
                        sgdat = 0;
                    }
                    if (sgdat == 0) {
                        r23at++;
                    }
                    sgdat++;
                    fragmentPrayDetectorBinding.tvRkat.setText(r23at + "");
                    fragmentPrayDetectorBinding.tvSgdat.setText(sgdat + "");

                    if(numberOfR23at==r23at&&sgdat==2){
                        SweetAlertDialogss.prayerDone(getActivity());


                    r23at = 0;
                    sgdat = 0;
                    fragmentPrayDetectorBinding.tvRkat.setText(r23at + "");
                    fragmentPrayDetectorBinding.tvSgdat.setText(sgdat + "");
                    Sensey.getInstance().stopProximityDetection(proximityListener);
                    Sensey.getInstance().startProximityDetection(proximityListener);
                    }
                }
                doubleBackToExitPressedOnce = false;
            }

            @Override
            public void onFar() {


            }
        };


    }
}

