
package grand.diaaalsalheen.home.view;


import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.SweetAlertDialogss;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.activities.SplashScreenActivity;
import grand.diaaalsalheen.base.views.navigationdrawer.MenuViewModel;
import grand.diaaalsalheen.base.volleyutils.MyApplication;
import grand.diaaalsalheen.databinding.ItemHomeBinding;
import grand.diaaalsalheen.home.response.HomeItem;
import grand.diaaalsalheen.home.viewmodel.HomeItemViewModel;
import grand.diaaalsalheen.qibla.CompassActivity;


public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeViewHolder> {

    private List<HomeItem> homeItems;
    private MutableLiveData<HomeItem> itemsOperationsLiveListener;


    public HomeAdapter() {
        getItems();
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home,
                new FrameLayout(parent.getContext()), false);
        return new HomeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HomeViewHolder holder, final int index) {
        HomeItem homeItem = homeItems.get(index);
        HomeItemViewModel homeItemViewModel = new HomeItemViewModel(homeItem);
        homeItemViewModel.getItemsOperationsLiveListener().observeForever(item -> {

            int id = homeItem.getId();
            if (id == 1) {
                MovementManager.startActivity(holder.itemView.getContext(), Codes.DOAA);
            } else if (id == 2) {
                MovementManager.startActivity(holder.itemView.getContext(), Codes.QORAN);
            } else if (id == 3) {
                MovementManager.startActivity(holder.itemView.getContext(), Codes.MAIN_BEADS);
            } else if (id == 4) {
                MovementManager.startActivity(holder.itemView.getContext(), Codes.AZKAR_CATEGORIES);
            } else if (id == 5) {
                MovementManager.startActivity(holder.itemView.getContext(), Codes.ZYARAT);
            } else if (id == 6) {
                MovementManager.startActivity(holder.itemView.getContext(), Codes.MONAGAT_CATEGORIES);
            } else if (id == 7) {
                MovementManager.startActivity(holder.itemView.getContext(), Codes.MUSLIM_CALENDER);
            } else if (id == 8) {
                MovementManager.startActivity(holder.itemView.getContext(), Codes.PRAYER_TIMES);
            } else if (id == 9) {
                MovementManager.startActivity(holder.itemView.getContext(), Codes.WORKS_CATEGORIES);
            } else if (id == 10) {
                MovementManager.startActivity(holder.itemView.getContext(), Codes.PRAYER_DETECTOR);
            } else if (id == 11) {

                if (UserPreferenceHelper.isLogined()) {
                    MovementManager.startActivity(holder.itemView.getContext(), Codes.TASKS);
                } else {
                    SweetAlertDialogss.loginToContinue(holder.itemView.getContext());
                }

            } else if (id == 12) {
                SplashScreenActivity.changeLanguage(holder.itemView.getContext(), UserPreferenceHelper.getCurrentLanguage(holder.itemView.getContext()));
                MenuViewModel.changeLanguage(holder.itemView.getContext(), UserPreferenceHelper.getCurrentLanguage(holder.itemView.getContext()));
                holder.itemView.getContext().startActivity(new Intent(holder.itemView.getContext(), CompassActivity.class));
            }

        });
        holder.setViewModel(homeItemViewModel);
    }


    public MutableLiveData<HomeItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.homeItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull HomeViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull HomeViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


    public void getItems() {
        homeItems = new ArrayList<>();

        if (UserPreferenceHelper.getCurrentLanguage(MyApplication.getInstance().getApplicationContext()).equals("ar")) {
            homeItems.add(new HomeItem(1, R.drawable.img_doaa));
            homeItems.add(new HomeItem(2, R.drawable.img_quran));
            homeItems.add(new HomeItem(3, R.drawable.img_bread));
            homeItems.add(new HomeItem(4, R.drawable.img_azkar));
            homeItems.add(new HomeItem(5, R.drawable.img_visits));
            homeItems.add(new HomeItem(6, R.drawable.mngat));
            homeItems.add(new HomeItem(7, R.drawable.img_calender));
            homeItems.add(new HomeItem(8, R.drawable.image_times));
            homeItems.add(new HomeItem(9, R.drawable.image_works));
            homeItems.add(new HomeItem(10, R.drawable.image_prayers));
            homeItems.add(new HomeItem(11, R.drawable.img_tasks));
            homeItems.add(new HomeItem(12, R.drawable.img_qibla));
        } else {
            homeItems.add(new HomeItem(1, R.drawable.img_doaa_en));
            homeItems.add(new HomeItem(2, R.drawable.img_quran_en));
            homeItems.add(new HomeItem(3, R.drawable.img_bread_en));
            homeItems.add(new HomeItem(4, R.drawable.img_azkar_en));
            homeItems.add(new HomeItem(5, R.drawable.img_visits_en));
            homeItems.add(new HomeItem(6, R.drawable.mngat_en));
            homeItems.add(new HomeItem(7, R.drawable.img_calender_en));
            homeItems.add(new HomeItem(8, R.drawable.image_times_en));
            homeItems.add(new HomeItem(9, R.drawable.image_works_en));
            homeItems.add(new HomeItem(10, R.drawable.image_prayers_en));
            homeItems.add(new HomeItem(11, R.drawable.img_tasks_en));
            homeItems.add(new HomeItem(12, R.drawable.img_qibla_en));
        }

    }

    static class HomeViewHolder extends RecyclerView.ViewHolder {
        ItemHomeBinding itemHomeBinding;

        HomeViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemHomeBinding == null) {
                itemHomeBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemHomeBinding != null) {
                itemHomeBinding.unbind();
            }
        }

        void setViewModel(HomeItemViewModel homeItemViewModel) {
            if (itemHomeBinding != null) {
                itemHomeBinding.setItemHomeViewModel(homeItemViewModel);
            }
        }


    }


}
