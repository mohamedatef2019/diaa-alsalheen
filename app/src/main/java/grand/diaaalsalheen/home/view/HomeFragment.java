package grand.diaaalsalheen.home.view;


import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;

import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.SweetAlertDialogss;
import grand.diaaalsalheen.UserLocation;
import grand.diaaalsalheen.azan.scheduler.SalaatAlarmReceiver;
import grand.diaaalsalheen.azan.util.AppSettings;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.activities.MainActivity;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentHomeBinding;
import grand.diaaalsalheen.favourite.view.FavouritesFragment;
import grand.diaaalsalheen.home.viewmodel.HomeViewModel;
import grand.diaaalsalheen.notifications.view.NotificationsFragment;


public class HomeFragment extends BaseFragment {

    private View rootView;
    private FragmentHomeBinding fragmentHomeBinding;
    private HomeViewModel homeViewModel;



    private static boolean sIsAlarmInit = false;
    private int mIndex = 0;
    private Location mLastLocation;
    TextView mAlarm;
    View mRamadanContainer;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentHomeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();

    }

    private void binding() {
        rootView = fragmentHomeBinding.getRoot();
        UserLocation userLocation = UserPreferenceHelper.getUserLocation();
        assert userLocation != null;
        homeViewModel = new HomeViewModel(userLocation.getLat(), userLocation.getLng());
        fragmentHomeBinding.setHomeViewModel(homeViewModel);
        mLastLocation = new Location("privider");
        mLastLocation.setLatitude(userLocation.getLat());
        mLastLocation.setLongitude(userLocation.getLng());
        initAzanTimes();

        UmmalquraCalendar cal = new UmmalquraCalendar();


        try {
            cal.add(Calendar.DAY_OF_MONTH, Integer.parseInt(UserPreferenceHelper.getCurrentTime(context)));
            Log.e("DATAZ", Integer.parseInt(UserPreferenceHelper.getCurrentTime(context))+"");
        } catch (Exception e) {
            e.getStackTrace();
        }
        String date = "not supported";
        try {
            if (Build.VERSION.SDK_INT > 19)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    date = cal.get(Calendar.DAY_OF_MONTH) + " " + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.forLanguageTag(UserPreferenceHelper.getCurrentLanguage(getActivity()))) + " " + cal.get(Calendar.YEAR) + " " + getResources().getString(R.string.label_hijri);
                }
        } catch (Exception e) {
            e.getStackTrace();
        }

        fragmentHomeBinding.tvMainTitle.setText(date);

        fragmentHomeBinding.ivNavigationDrawerNotification.setOnClickListener(view -> {
            if (UserPreferenceHelper.isLogined()) {
                MovementManager.addFragment(getContext(), new NotificationsFragment(), "NotificationsFragment");
            } else {
                SweetAlertDialogss.loginToContinue(getContext());
            }
        });


        fragmentHomeBinding.ivNavigationDrawerSearch.setOnClickListener(view -> MovementManager.startActivity(getActivity(), Codes.SEARCH));


        fragmentHomeBinding.ivAddPostBaseBack.setOnClickListener(v -> {
            if (((MainActivity) Objects.requireNonNull(getActivity())).getNavigationDrawerView().layoutNavigationDrawerBinding.dlMainNavigationMenu.isDrawerOpen(GravityCompat.START)) {
                ((MainActivity) Objects.requireNonNull(getActivity())).getNavigationDrawerView().layoutNavigationDrawerBinding.dlMainNavigationMenu.closeDrawers();
            } else {
                ((MainActivity) Objects.requireNonNull(getActivity())).getNavigationDrawerView().layoutNavigationDrawerBinding.dlMainNavigationMenu.openDrawer(GravityCompat.START);
            }
        });

        homeViewModel.getClicksMutableLiveData().observe(getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                fragmentHomeBinding.tvAuto.setSelected(true);

            }
        });


    }


    private void initAzanTimes() {
        if (!sIsAlarmInit) {
            AppSettings.getInstance(getActivity());
            AppSettings.getInstance().setLatFor(mIndex, mLastLocation.getLatitude());
            AppSettings.getInstance().setLngFor(mIndex, mLastLocation.getLongitude());
            updateAlarmStatus();
            sIsAlarmInit = true;
        }
    }

    private void updateAlarmStatus() {


        AppSettings settings = AppSettings.getInstance(getActivity());

        SalaatAlarmReceiver sar = new SalaatAlarmReceiver();
        boolean isAlarmSet = settings.isAlarmSetFor(mIndex);
        if (!isAlarmSet) {
            settings.setAlarmFor(0, true);
            sar.cancelAlarm(getActivity());
            sar.setAlarm(getActivity());
        }


    }


}