package grand.diaaalsalheen.home.viewmodel;

import android.annotation.SuppressLint;
import android.location.Address;
import android.location.Geocoder;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import grand.diaaalsalheen.PrayTime;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.base.volleyutils.MyApplication;
import grand.diaaalsalheen.home.response.slider.ImageItem;
import grand.diaaalsalheen.home.response.slider.ImagesResponse;
import grand.diaaalsalheen.home.view.HomeAdapter;
import grand.diaaalsalheen.login.response.UserItem;
import grand.diaaalsalheen.login.response.UserResponse;

@SuppressLint({"SimpleDateFormat","DefaultLocale"})
public class ImagesViewModel extends BaseViewModel {


    private ArrayList<String> images;


    public ImagesViewModel( ) {
        images = new ArrayList<>();
        getSlider();

    }



    private void getSlider() {


            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {

                    ImagesResponse imagesResponse = (ImagesResponse)response;
                    List<ImageItem> imageItems = imagesResponse.getData();

                   for(int i=0;i<imageItems.size(); i++){
                       images.add(imageItems.get(i).getImg());
                   }
                    notifyChange();

                }
            }).requestJsonObject(Request.Method.GET, WebServices.SLIDER,null , ImagesResponse.class);




    }


    @BindingAdapter({"app:images"})
    public static void setSliderImages(SliderLayout sliderLayout, ArrayList<String> images) {

         try {
             sliderLayout.getCurrentSlider();
         }catch (Exception e){
            for (int i = 0; i < images.size(); i++) {
                DefaultSliderView textSliderView = new DefaultSliderView(sliderLayout.getContext());
                // initialize a SliderLayout
                textSliderView
                        .description("")
                        .image(images.get(i))
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop);
                sliderLayout.addSlider(textSliderView);
            }
            sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
            sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            sliderLayout.setCustomAnimation(new DescriptionAnimation());
            sliderLayout.setDuration(4000);
        }

    }


    public ArrayList<String> getImages() {
        return images;
    }
}
