
package grand.diaaalsalheen.home.viewmodel;


import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.home.response.HomeItem;


public class HomeItemViewModel extends BaseItemViewModel {
    private HomeItem homeItem;


    public HomeItemViewModel(HomeItem homeItem) {
        this.homeItem = homeItem;
    }


    @Bindable
    public HomeItem getHomeItem() {
        return homeItem;
    }

    @BindingAdapter({"app:src"})
    public static void setSrc(ImageView view, int image){
        view.setImageResource(image);

    }

    public void onHomeItemClick(){
        getItemsOperationsLiveListener().setValue(null);
    }
}
