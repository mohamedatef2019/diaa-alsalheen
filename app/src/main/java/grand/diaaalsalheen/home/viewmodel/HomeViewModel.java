package grand.diaaalsalheen.home.viewmodel;

import android.annotation.SuppressLint;
import android.location.Address;
import android.location.Geocoder;
import android.os.CountDownTimer;
import android.util.Log;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import grand.diaaalsalheen.GlobalVariables;
import grand.diaaalsalheen.PrayerTimes2;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.base.volleyutils.MyApplication;
import grand.diaaalsalheen.home.view.HomeAdapter;
import grand.diaaalsalheen.muslimcalender.response.MonsbatResponse;
import grand.diaaalsalheen.tasks.viewmodel.TasksViewModel;

@SuppressLint({"SimpleDateFormat","DefaultLocale"})
public class HomeViewModel extends ImagesViewModel {

    private HomeAdapter homeAdapter;


    private String time, timeName,cityName,currentDay;

    private  String monsbaText;


    private SimpleDateFormat sdf;
    private long differenceTime;


    private double lat, lng;

    private int nextPrayerIndex;

    private ArrayList<String> prayerTimes;
    public HomeViewModel(double lat, double lng) {
        sdf = new SimpleDateFormat("HH:mm");

        this.lat = lat;
        this.lng = lng;

        getNextPrayer();
        notifyChange();

        getMonsbats(TasksViewModel.getCurrentDateString2());
    }


    public int getNextPrayerIndex() {
        return nextPrayerIndex;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {

        return time;
    }

    public void setCurrentDay(String currentDay) {
        this.currentDay = currentDay;
    }

    public String getCurrentDay() {
    this.currentDay = new SimpleDateFormat("EEEE", new Locale(UserPreferenceHelper.getCurrentLanguage(MyApplication.getInstance().getApplicationContext()))).format(Calendar.getInstance().getTime().getTime());
    return currentDay;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        if(this.cityName==null) {
            this.cityName = getCurrentCity();
        }
        return cityName;
    }


    private String getCurrentCity(){
        Geocoder gcd = new Geocoder(MyApplication.getInstance().getApplicationContext(),new Locale(UserPreferenceHelper.getCurrentLanguage(MyApplication.getInstance().getApplicationContext())));
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(lat, lng, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0)
            return  addresses.get(0).getLocality();

        return "ERROR";
    }

    @Bindable
    public void setTimeName(String timeName) {
        this.timeName = timeName;
    }

    @Bindable
    public String getTimeName() {
        return timeName;
    }

    private void getNextPrayer() {



        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();

        double offsetFromUtc = tz.getOffset(now.getTime()) / 3600000.0;
        double m2tTimeZoneIs = Double.parseDouble("" + offsetFromUtc);

        PrayerTimes2 prayTime = new PrayerTimes2();
        prayTime.setTimeFormat(PrayerTimes2.TimeFormat.Time24);


        if(UserPreferenceHelper.getMazhab().equals("suny")){
            prayTime.setCalcMethod(PrayerTimes2.Calculation.Makkah);
        }else {
            prayTime.setCalcMethod(PrayerTimes2.Calculation.Tehran);
        }

      // prayTime.setCalcMethod(PrayerTimes2.Calculation.Makkah);

        prayTime.setAsrJuristic(PrayerTimes2.Juristic.Shafii);
        prayTime.setAdjustHighLats(PrayerTimes2.Adjusting.AngleBased);
        prayTime.setOffsets(new int[]{0, 0, 0, 0, 0, 0, 0});

        prayerTimes = prayTime.getPrayerTimes2s(Calendar.getInstance(),
               lat, lng, m2tTimeZoneIs);
             Log.e("PrayerOptions",lat+"  "+lng+"  "+m2tTimeZoneIs);


        for (int i = 0; i < prayerTimes.size(); i++) {
            differenceTime = isNextTime(prayerTimes.get(i));
            if (differenceTime > 0) {
                Log.e("TAS",differenceTime+"");
                timeName = prayTime.getTimeNames().get(i);
                time = prayerTimes.get(i);
                startCountDown();
                nextPrayerIndex = i;
                break;
            }
        }

    }

    private void startCountDown() {

        new CountDownTimer(differenceTime, 1000) {
            public void onTick(long millisUntilFinished) {
                differenceTime-=1000;
                int seconds = (int) (differenceTime / 1000) % 60 ;
                int minutes = (int) ((differenceTime / (1000*60)) % 60);
                int hours   = (int) ((differenceTime / (1000*60*60)) % 24);
                setTime( String.format("%02d",hours)+":"+ String.format("%02d",minutes)+":"+String.format("%02d",seconds) );
                notifyChange();
            }

            public void onFinish() {

            }

        }.start();
    }


    private long isNextTime(String prayTime) {


        Date d1 = null;

        try {
            d1 = sdf.parse(prayTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date d2 = new Date();

        try {
            d2 = sdf.parse(sdf.format(d2));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return d1.getTime() - d2.getTime();
    }


    @BindingAdapter({"app:adapter"})
    public static void getHomeAdapter(RecyclerView recyclerView, HomeAdapter homeAdapter) {
        if (recyclerView.getAdapter() == null) {
            recyclerView.setAdapter(homeAdapter);
        }
    }



    public ArrayList<String> getPrayerTimes() {
        return prayerTimes;
    }

    @Bindable
    public HomeAdapter getHomeAdapter() {
        return this.homeAdapter == null ? this.homeAdapter = new HomeAdapter() : this.homeAdapter;
    }

    public String getMonsbaText() {
        return monsbaText;
    }

    public void setMonsbaText(String monsbaText) {
        this.monsbaText = monsbaText;
    }

    public void getMonsbats(String date) {

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                String monsbats="";

                MonsbatResponse monsbatResponse = (MonsbatResponse) response;

                if(monsbatResponse.getData().getFirstOne()!=null) {
                    monsbats+=ResourcesManager.getString(R.string.label_current)+" : "+monsbatResponse.getData().getFirstOne().getName()+" ( "+monsbatResponse.getData().getFirstOne().getDate()+" ) "+"  ";
                }

                if(monsbatResponse.getData().getSecondOne()!=null) {
                      monsbats+=ResourcesManager.getString(R.string.label_next)+" : "+monsbatResponse.getData().getSecondOne().getName()+" ( "+monsbatResponse.getData().getSecondOne().getDate()+" ) ";

                }

                setMonsbaText(monsbats);
                getClicksMutableLiveData().setValue(Codes.DATA_UPDATED);
                notifyChange();


            }
        }).requestJsonObject(Request.Method.GET, WebServices.MONSBAT+date, null, MonsbatResponse.class);
    }



}
