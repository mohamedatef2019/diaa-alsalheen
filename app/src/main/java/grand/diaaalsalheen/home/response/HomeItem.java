package grand.diaaalsalheen.home.response;

public class HomeItem {
   private int id;
    private int image;



    public HomeItem(int id,int image){
        this.id=id;
        this.image=image;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public int getImage() {
        return image;
    }


}
