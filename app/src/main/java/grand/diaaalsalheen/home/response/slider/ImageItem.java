package grand.diaaalsalheen.home.response.slider;

 import com.google.gson.annotations.SerializedName;


public class ImageItem {

	@SerializedName("img")
	private String img;

	@SerializedName("link")
	private String link;

	@SerializedName("id")
	private int id;

	public void setImg(String img){
		this.img = img;
	}

	public String getImg(){
		return img;
	}

	public void setLink(String link){
		this.link = link;
	}

	public String getLink(){
		return link;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"img = '" + img + '\'' + 
			",link = '" + link + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}