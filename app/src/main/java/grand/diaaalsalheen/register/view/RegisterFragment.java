package grand.diaaalsalheen.register.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Objects;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.azan.Constants;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.filesutils.FileOperations;
import grand.diaaalsalheen.base.filesutils.VolleyFileObject;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.activities.BaseActivity;
import grand.diaaalsalheen.base.views.customviews.spinner.PopUpItem;
import grand.diaaalsalheen.base.views.customviews.spinner.PopUpMenus;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentRegisterBinding;
import grand.diaaalsalheen.login.response.UserItem;
import grand.diaaalsalheen.register.viewmodel.RegisterViewModel;


public class RegisterFragment extends BaseFragment {

    private View rootView;
    private RegisterViewModel registerViewModel;
    private FragmentRegisterBinding fragmentRegisterBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentRegisterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }


    private void binding() {
        rootView = fragmentRegisterBinding.getRoot();
        registerViewModel = new RegisterViewModel();
        fragmentRegisterBinding.setRegisterViewModel(registerViewModel);


        try {
            registerViewModel.getRegisterRequest().setEmail(getArguments().getString("email"));
            registerViewModel.getRegisterRequest().setPhone(getArguments().getString("phone"));
            registerViewModel.getRegisterRequest().setName(getArguments().getString("name"));
            registerViewModel.getRegisterRequest().setSocialId(getArguments().getString("social_id"));
        } catch (Exception e) {
            e.getStackTrace();
        }

    }


    private void liveDataListeners() {
        registerViewModel.getClicksMutableLiveData().observe(this, result -> {


            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.SELECT_PROFILE_IMAGE) {
                FileOperations.pickImage(getActivity());
            } else if (result == Codes.GENDER_SELECT) {
                showUserGender();
            } else if (result == Codes.DOCTRINE_SELECT) {
                showUserDoctrine();
            } else if (result == Codes.SELECT_LOCATION) {
                MovementManager.startActivityForResult(getActivity(), BaseActivity.class, Codes.SELECT_LOCATION, Codes.SELECT_LOCATION);
            } else if (result == Codes.SHOW_MESSAGE) {
                showMessage(registerViewModel.getReturnedMessage());
            } else {
                try{
                    UserItem userItem = UserPreferenceHelper.getUserDetails();
                    userItem.setUsersImg(getArguments().getString("image"));
                    UserPreferenceHelper.saveUserDetails(userItem);
                }catch (Exception e){
                    e.getStackTrace();
                }
                MovementManager.startActivity(getActivity(), Codes.SEND_CODE_SCREEN);
            }

        });
    }

    private void showUserGender() {
        final ArrayList<PopUpItem> popUpItems = new ArrayList<>();
        popUpItems.add(new PopUpItem("0", ResourcesManager.getString(R.string.label_male)));
        popUpItems.add(new PopUpItem("1", ResourcesManager.getString(R.string.label_female)));
        PopUpMenus.showPopUp(getActivity(), fragmentRegisterBinding.etRegisterGender, popUpItems).setOnMenuItemClickListener(item -> {
            registerViewModel.getRegisterRequest().setGender(popUpItems.get(item.getItemId()).getId());
            fragmentRegisterBinding.etRegisterGender.setText(popUpItems.get(item.getItemId()).getName());
            registerViewModel.notifyChange();
            return false;
        });
    }

    private void showUserDoctrine() {
        final ArrayList<PopUpItem> popUpItems = new ArrayList<>();
        popUpItems.add(new PopUpItem("0", ResourcesManager.getString(R.string.label_sony)));
        popUpItems.add(new PopUpItem("1", ResourcesManager.getString(R.string.label_shie)));
        PopUpMenus.showPopUp(getActivity(), fragmentRegisterBinding.etRegisterDoctrine, popUpItems).setOnMenuItemClickListener(item -> {
            registerViewModel.getRegisterRequest().setDoctrine(popUpItems.get(item.getItemId()).getId());
            fragmentRegisterBinding.etRegisterDoctrine.setText(popUpItems.get(item.getItemId()).getName());
            registerViewModel.notifyChange();
            return false;
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       ;
        registerViewModel.getVolleyFileObjects().add(FileOperations.getVolleyFileObject(requireActivity(),data,"image", Codes.FILE_TYPE_IMAGE));
    }
}