package grand.diaaalsalheen.register.request;


import com.google.gson.annotations.SerializedName;

import grand.diaaalsalheen.base.utils.Validate;

public class RegisterRequest {

	@SerializedName("password")
	private String password;

    @SerializedName("cpassword")
    private String cpassword;

	@SerializedName("address")
	private String address;


	@SerializedName("phone")
	private String phone;

	@SerializedName("users_name")
	private String name;

    @SerializedName("firebase_token")
    private String token;

	@SerializedName("email")
	private String email;

	@SerializedName("lat")
	private String lat="0.0";


    @SerializedName("lng")
    private String lng="0.0";

    @SerializedName("users_gender")
    private String gender;

    @SerializedName("users_doctrine")
    private String doctrine;

    @SerializedName("social_token")
    private String socialId;






	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}


	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setLng(String lng){
		this.lng = lng;
	}

	public String getLng(){
		return lng;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setLat(String lat){
		this.lat = lat;
	}

	public String getLat(){
		return lat;
	}

    public void setCpassword(String cpassword) {
        this.cpassword = cpassword;
    }

    public String getCpassword() {
        return cpassword;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setDoctrine(String doctrine) {
        this.doctrine = doctrine;
    }

    public String getDoctrine() {
        return doctrine;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getSocialId() {
        return socialId;
    }


    public boolean haveError(){
        return !Validate.isMail(email)&&!Validate.isAvLen(password,8,12);
    }

    @Override
 	public String toString(){
		return 
			"RegisterResponse{" + 
			"password = '" + password + '\'' +
			",address = '" + address + '\'' + 
			",lng = '" + lng + '\'' + 
			",phone = '" + phone + '\'' + 
			",name = '" + name + '\'' + 
			",email = '" + email + '\'' + 
			",lat = '" + lat + '\'' + 
			"}";
		}
}