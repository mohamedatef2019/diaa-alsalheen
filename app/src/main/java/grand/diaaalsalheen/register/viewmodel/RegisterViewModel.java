package grand.diaaalsalheen.register.viewmodel;

import android.view.View;

import com.android.volley.Request;

import java.util.ArrayList;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.filesutils.VolleyFileObject;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.login.response.UserItem;
import grand.diaaalsalheen.register.request.RegisterRequest;
import grand.diaaalsalheen.register.response.RegisterResponse;


public class RegisterViewModel extends BaseViewModel {

    private RegisterRequest registerRequest;

    private ArrayList<VolleyFileObject> volleyFileObjects;


    public RegisterViewModel() {
        registerRequest = new RegisterRequest();

        registerRequest.setToken(UserPreferenceHelper.getGoogleToken());

        volleyFileObjects = new ArrayList<>();
    }


    public void genderClick() {
        getClicksMutableLiveData().setValue(Codes.GENDER_SELECT);
    }


    public void doctrineClick() {
        getClicksMutableLiveData().setValue(Codes.DOCTRINE_SELECT);
    }


    public RegisterRequest getRegisterRequest() {
        return registerRequest;
    }


    public ArrayList<VolleyFileObject> getVolleyFileObjects() {
        return volleyFileObjects;
    }


    public void registerClick() {
        goRegister();
    }

    private void goRegister() {
        accessLoadingBar(View.VISIBLE);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                RegisterResponse registerResponse = (RegisterResponse) response;
                if (registerResponse.getStatus().equals(WebServices.SUCCESS)) {

                    UserItem userItem = new UserItem();
                    userItem.setEmail(registerRequest.getEmail());
                    userItem.setPhone(registerRequest.getPhone());
                    userItem.setPassword(registerRequest.getPassword());
                    userItem.setUsersGender(registerRequest.getGender());
                    userItem.setUsersDoctrine(registerRequest.getDoctrine());

                    UserPreferenceHelper.saveUserDetails(userItem);
                    getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);

                    getClicksMutableLiveData().setValue(Codes.SEND_CODE_SCREEN);
                } else if (registerResponse.getStatus().equals(WebServices.FAILED)) {
                    setReturnedMessage(registerResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject( Request.Method.POST,WebServices.REGISTER, registerRequest, RegisterResponse.class);

    }


}
