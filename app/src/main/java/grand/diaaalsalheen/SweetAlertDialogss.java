package grand.diaaalsalheen;

import android.content.Context;

import cn.pedant.SweetAlert.SweetAlertDialog;
import grand.diaaalsalheen.base.utils.ResourcesManager;

public class SweetAlertDialogss {

    public static void showFavDialog(Context context) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(ResourcesManager.getString(R.string.favourite))
                .setContentText(ResourcesManager.getString(R.string.msg_added_favourite))
                .setConfirmButton(ResourcesManager.getString(R.string.action_submit), sweetAlertDialog -> {
                    sweetAlertDialog.cancel();
                    sweetAlertDialog.dismiss();
                })
                .show();
    }



    public static void loginToContinue(Context context) {

        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(ResourcesManager.getString(R.string.label_login))
                .setContentText(ResourcesManager.getString(R.string.label_should_login))
                .setConfirmButton(ResourcesManager.getString(R.string.action_close), sweetAlertDialog -> {
                    sweetAlertDialog.cancel();
                    sweetAlertDialog.dismiss();
                })
                .show();
    }

    public static void messageSent(Context context) {

        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(ResourcesManager.getString(R.string.contact_us))
                .setContentText(ResourcesManager.getString(R.string.msg_added_favourite))
                .setConfirmButton(ResourcesManager.getString(R.string.action_submit), sweetAlertDialog -> {
                    sweetAlertDialog.cancel();
                    sweetAlertDialog.dismiss();
                })
                .show();
    }



    public static void prayerDone(Context context) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(ResourcesManager.getString(R.string.label_pray_done))
                .setContentText(ResourcesManager.getString(R.string.msg_pray_done))
                .setConfirmButton(ResourcesManager.getString(R.string.action_submit), sweetAlertDialog -> {
                    sweetAlertDialog.cancel();
                    sweetAlertDialog.dismiss();
                })
                .show();
    }


}
