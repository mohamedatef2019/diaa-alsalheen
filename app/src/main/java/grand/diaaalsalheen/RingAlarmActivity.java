package grand.diaaalsalheen;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.huhx0015.hxaudio.audio.HXMusic;
import com.huhx0015.hxaudio.interfaces.HXMusicListener;
import com.huhx0015.hxaudio.model.HXMusicItem;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;

public class RingAlarmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ring_alarm);


        findViewById(R.id.close).setOnClickListener(view -> {
            try {
                HXMusic.stop();
            }catch (Exception e){
                e.getStackTrace();
            }
            finishAffinity();
        });




        if(UserPreferenceHelper.getMazhab().equals("shia")) {
            HXMusic.music().load(R.raw.azanshi3a).play(this);

        }else {
            HXMusic.music().load(UserPreferenceHelper.getCurrentAzan(this).equals("big") ? R.raw.azanbig : R.raw.azansmall).play(this);

        }


        HXMusic.setListener(new HXMusicListener() {
            @Override
            public void onMusicPrepared(HXMusicItem music) {

            }
            @Override
            public void onMusicCompletion(HXMusicItem music) {

            }
            @Override
            public void onMusicBufferingUpdate(HXMusicItem music, int percent) {

            }
            @Override
            public void onMusicPause(HXMusicItem music) {

            }
            @Override
            public void onMusicStop(HXMusicItem music) {

            }
        });
    }
}
