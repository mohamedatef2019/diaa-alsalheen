package grand.diaaalsalheen;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserLocation {


    @SerializedName("lat")
    private double lat;
   @SerializedName("lng")
   private double lng;



    public UserLocation(double lat,double lng){
        this.lat=lat;
        this.lng=lng;
    }


   public void setLng(double lng){
       this.lng = lng;
   }

   public double getLng(){
       return lng;
   }


   public void setLat(double lat){
       this.lat = lat;
   }

   public double getLat(){
       return lat;
   }



    @Override
    public String toString(){
       return
           "UserLocation{" +
           ",lng = '" + lng + '\'' +
           ",lat = '" + lat + '\'' +
           "}";
       }
}