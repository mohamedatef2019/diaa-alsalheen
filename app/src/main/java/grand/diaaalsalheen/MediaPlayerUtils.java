package grand.diaaalsalheen;

import android.content.Context;
import android.widget.ImageView;

import com.huhx0015.hxaudio.audio.HXMusic;
import com.huhx0015.hxaudio.audio.HXSound;

public class MediaPlayerUtils {

    private enum HXMusicStatus {
        NOT_READY,
        READY,
        PLAYING,
        PAUSED,
        STOPPED
    }

    public static void playAudio(String audio, String name, Context context, ImageView ivPlay) {
        if(audio!=null) {
            if(HXMusic.isPlaying()) {
                HXMusic.pause();
                ivPlay.setImageResource(R.drawable.ic_sound_2);
            }else {
                if(HXMusic.getStatus().equals(HXMusicStatus.PAUSED.toString())){
                    HXMusic.resume(context);
                }else {
                    HXMusic.music()
                            .load(audio)
                            .title(name)
                            .play(context);
                }
                ivPlay.setImageResource(R.drawable.ic_sound);
            }





        }
    }
}
