package grand.diaaalsalheen.mongat.response;


import com.google.gson.annotations.SerializedName;


public class MongatDetailsItem {

	@SerializedName("voice")
	private String voice;

	@SerializedName("monagat_category_id")
	private int monagatCategoryId;

	@SerializedName("id")
	private int id;

	@SerializedName("text")
	private String text;

	public void setVoice(String voice){
		this.voice = voice;
	}

	public String getVoice(){
		return voice;
	}

	public void setMonagatCategoryId(int monagatCategoryId){
		this.monagatCategoryId = monagatCategoryId;
	}

	public int getMonagatCategoryId(){
		return monagatCategoryId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"voice = '" + voice + '\'' + 
			",monagat_category_id = '" + monagatCategoryId + '\'' + 
			",id = '" + id + '\'' + 
			",text = '" + text + '\'' + 
			"}";
		}
}