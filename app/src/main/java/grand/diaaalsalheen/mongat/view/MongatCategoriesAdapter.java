
package grand.diaaalsalheen.mongat.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.databinding.ItemMongatBinding;
import grand.diaaalsalheen.mongat.response.MonagatCategoryItem;
import grand.diaaalsalheen.mongat.viewmodel.MonagatCategoryItemViewModel;


public class MongatCategoriesAdapter extends RecyclerView.Adapter<MongatCategoriesAdapter.CompaniesViewHolder> {

    private List<MonagatCategoryItem> monagatCategoryItems;
    private MutableLiveData<MonagatCategoryItem> itemsOperationsLiveListener;


    public MongatCategoriesAdapter() {
        this.monagatCategoryItems = new ArrayList<>();
    }

    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mongat,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {
        MonagatCategoryItem monagatCategoryItem = monagatCategoryItems.get(position);
        MonagatCategoryItemViewModel companyItemViewModel = new MonagatCategoryItemViewModel(monagatCategoryItem,position);
        companyItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
            itemsOperationsLiveListener.setValue(monagatCategoryItem);
        });
        holder.setViewModel(companyItemViewModel);
    }



    public MutableLiveData<MonagatCategoryItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.monagatCategoryItems.size();
    }

    @Override
    public void onViewAttachedToWindow(CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<MonagatCategoryItem> data) {

            this.monagatCategoryItems.clear();

            this.monagatCategoryItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemMongatBinding itemChatCompanyBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemChatCompanyBinding == null) {
                itemChatCompanyBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemChatCompanyBinding != null) {
                itemChatCompanyBinding.unbind();
            }
        }

        void setViewModel(MonagatCategoryItemViewModel monagatCategoryItemViewModel) {
            if (itemChatCompanyBinding != null) {
                itemChatCompanyBinding.setItemMonagatCategoriesViewModel(monagatCategoryItemViewModel);
            }
        }


    }



}
