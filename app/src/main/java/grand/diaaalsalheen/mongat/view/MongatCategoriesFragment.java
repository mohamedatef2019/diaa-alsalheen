package grand.diaaalsalheen.mongat.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentMonagasBinding;
import grand.diaaalsalheen.mongat.viewmodel.MongatCategoriesViewModel;


public class MongatCategoriesFragment extends BaseFragment {
    private View rootView;
    private MongatCategoriesViewModel mongatCategoriesViewModel;
    private FragmentMonagasBinding fragmentMonagasBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMonagasBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_monagas, container, false);
        assert getArguments() != null;

        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();

    }

    private void binding() {
        rootView = fragmentMonagasBinding.getRoot();
        mongatCategoriesViewModel = new MongatCategoriesViewModel();
        fragmentMonagasBinding.setMonagatsViewModel(mongatCategoriesViewModel);
    }


    private void liveDataListeners() {
        mongatCategoriesViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });

        mongatCategoriesViewModel.getMongatCategoriesAdapter().getItemsOperationsLiveListener().observe(this, monagatCategoryItem -> {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Params.BUNDLE_MONGAT_ITEM, monagatCategoryItem);
            MovementManager.startActivity(getActivity(), Codes.MONGAT_DETAILS, bundle);
        });
    }


}