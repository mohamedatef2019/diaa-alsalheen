package grand.diaaalsalheen.mongat.view;


import android.content.Intent;
import android.os.Bundle;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import org.sufficientlysecure.htmltextview.HtmlFormatter;
import org.sufficientlysecure.htmltextview.HtmlFormatterBuilder;
import grand.diaaalsalheen.BuildConfig;
import grand.diaaalsalheen.MediaPlayerUtils;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.SweetAlertDialogss;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentMonagaDetailsBinding;
import grand.diaaalsalheen.mongat.response.MonagatCategoryItem;
import grand.diaaalsalheen.mongat.viewmodel.MongatDetailsViewModel;


public class MongatDetailsFragment extends BaseFragment {
    private View rootView;
    private MongatDetailsViewModel mongatDetailsViewModel;
    private FragmentMonagaDetailsBinding fragmentMonagaDetailsBinding;
    int textSize = 0;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMonagaDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_monaga_details, container, false);
        assert getArguments() != null;

        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();

    }

    private void binding() {
        rootView = fragmentMonagaDetailsBinding.getRoot();
        assert getArguments() != null;
        mongatDetailsViewModel = new MongatDetailsViewModel((MonagatCategoryItem) getArguments().getSerializable(Params.BUNDLE_MONGAT_ITEM));
        fragmentMonagaDetailsBinding.setMonagatDetailsViewModel(mongatDetailsViewModel);
    }


    private void liveDataListeners() {
        mongatDetailsViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }else if (result == Codes.PLAY_AUDIO) {

                MediaPlayerUtils.playAudio(mongatDetailsViewModel.getMongatDetailsItem().getVoice(), mongatDetailsViewModel.getMongatDetailsItem().getText(),
                        getActivity(),fragmentMonagaDetailsBinding.ivPlay);

            }else if(result == Codes.ADD_TO_FAVOURITE){
                if(UserPreferenceHelper.isLogined()){
                SweetAlertDialogss.showFavDialog(getActivity()); }
                else {
                    SweetAlertDialogss.loginToContinue(getActivity());
                }
            }else if (result == Codes.ZOOM_IN) {
                textSize+=5;
                Log.e("SIZO",textSize+"");
                fragmentMonagaDetailsBinding.tvHtmlViewer.setInitialScale(textSize);


            } else if (result == Codes.ZOOM_OUT) {
                textSize-=5;
                fragmentMonagaDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            } else if (result == Codes.DATA_RECIVED) {
                Spanned formattedHtml = HtmlFormatter.formatHtml(new HtmlFormatterBuilder().setHtml(mongatDetailsViewModel.getMongatDetailsItem().getText()));
                String str=mongatDetailsViewModel.getMongatDetailsItem().getText();
                fragmentMonagaDetailsBinding.tvHtmlViewer.loadDataWithBaseURL(null, str, "text/html", "utf-8", null);
                textSize =  270;
                fragmentMonagaDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            }else if(result == Codes.SHARE){
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, WebServices.SHARE_LINK+"4/"+ mongatDetailsViewModel.getMongatDetailsItem().getId() + "/ar \n\n" + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);

                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }


        });


    }




}