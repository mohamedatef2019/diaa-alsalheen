package grand.diaaalsalheen.mongat.viewmodel;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.home.viewmodel.ImagesViewModel;
import grand.diaaalsalheen.mongat.response.MonagatCategoriesResponse;
import grand.diaaalsalheen.mongat.view.MongatCategoriesAdapter;
 

public class MongatCategoriesViewModel extends ImagesViewModel {

    private MongatCategoriesAdapter mongatCategoriesAdapter;


    public MongatCategoriesViewModel() {
        getMonagats();
    }


    @BindingAdapter({"app:adapter"})
    public static void getCategoriesBinding(RecyclerView recyclerView, MongatCategoriesAdapter mongatCategoriesAdapter) {
        recyclerView.setAdapter(mongatCategoriesAdapter);
    }


    @Bindable
    public MongatCategoriesAdapter getMongatCategoriesAdapter() {
        return this.mongatCategoriesAdapter == null ? this.mongatCategoriesAdapter = new MongatCategoriesAdapter() : this.mongatCategoriesAdapter;
    }


    private void getMonagats() {
         accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                MonagatCategoriesResponse chatListResponse = (MonagatCategoriesResponse) response;
                getMongatCategoriesAdapter().updateData(chatListResponse.getData());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.MONAGAT_CATEGORIES,null,   MonagatCategoriesResponse.class);


    }
}
