package grand.diaaalsalheen.mongat.viewmodel;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import grand.diaaalsalheen.FavouriteRequest;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.favourite.viewmodel.FavouriteViewModel;
import grand.diaaalsalheen.home.viewmodel.ImagesViewModel;
import grand.diaaalsalheen.mongat.response.MonagatCategoriesResponse;
import grand.diaaalsalheen.mongat.response.MonagatCategoryItem;
import grand.diaaalsalheen.mongat.response.MongatDetailsItem;
import grand.diaaalsalheen.mongat.response.MongatDetailsResponse;
import grand.diaaalsalheen.mongat.view.MongatCategoriesAdapter;


public class MongatDetailsViewModel extends BaseViewModel {

    private MongatDetailsItem mongatDetailsItem;

    public MongatDetailsViewModel(MonagatCategoryItem monagatCategoryItem) {
        getMonagats(monagatCategoryItem);
    }

    private void getMonagats(MonagatCategoryItem monagatCategoryItem) {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                MongatDetailsResponse mongatDetailsResponse = (MongatDetailsResponse) response;
                mongatDetailsItem = mongatDetailsResponse.getData();
                accessLoadingBar(View.GONE);
                getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.MONAGAT_DETAILS + monagatCategoryItem.getId(), null, MongatDetailsResponse.class);
    }

    public MongatDetailsItem getMongatDetailsItem() {
        return mongatDetailsItem;
    }

    public void onPlayAudioClick() {
        getClicksMutableLiveData().setValue(Codes.PLAY_AUDIO);
    }


    public void onZoomInFontClick() {
        getClicksMutableLiveData().setValue(Codes.ZOOM_IN);
    }

    public void onZoomOutFontClick() {
        getClicksMutableLiveData().setValue(Codes.ZOOM_OUT);
    }


    public void addToFavouriteClick() {
        getClicksMutableLiveData().setValue(Codes.ADD_TO_FAVOURITE);
        FavouriteViewModel.addToFavourite(new FavouriteRequest(getMongatDetailsItem().getId(), "App\\Models\\Monagat"));
    }

    public void onShareClick() {
        getClicksMutableLiveData().setValue(Codes.SHARE);
    }

}
