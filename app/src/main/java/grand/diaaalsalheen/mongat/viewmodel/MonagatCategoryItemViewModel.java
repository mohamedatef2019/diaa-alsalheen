
package grand.diaaalsalheen.mongat.viewmodel;

import androidx.databinding.Bindable;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.mongat.response.MonagatCategoryItem;


public class MonagatCategoryItemViewModel extends BaseItemViewModel {
    private MonagatCategoryItem monagatCategoryItem;
    private int position;

    public MonagatCategoryItemViewModel(MonagatCategoryItem monagatCategoryItem,int position) {
        this.monagatCategoryItem = monagatCategoryItem;
        this.position=position;
    }


    @Bindable
    public MonagatCategoryItem getMonagatCategoryItem() {
        return monagatCategoryItem;
    }

    public void onMonagatCategoryItemClicked(){
        getItemsOperationsLiveListener().setValue(Codes.MONGAT_DETAILS);
    }

    public boolean isLight() {
        return position%2==0;
    }



}
