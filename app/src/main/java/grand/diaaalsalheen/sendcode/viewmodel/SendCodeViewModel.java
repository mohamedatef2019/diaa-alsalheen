package grand.diaaalsalheen.sendcode.viewmodel;

import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.chaos.view.PinView;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.login.response.UserItem;
import grand.diaaalsalheen.sendcode.request.SendCodeRequest;
import grand.diaaalsalheen.sendcode.response.SendCodeResponse;
import grand.diaaalsalheen.tasks.view.TasksAdapter;


public class SendCodeViewModel extends BaseViewModel {
    private SendCodeRequest sendCodeRequest;
    private boolean isForgotPassword;


    public SendCodeViewModel(boolean isForgotPassword,String phone) {
        this.isForgotPassword = isForgotPassword;
        sendCodeRequest = new SendCodeRequest();
        sendCodeRequest.setEmail(phone);

        sendCodeRequest.setPhone(phone);
    }

    public void sendCodeClick() {
        if (isForgotPassword) {
            getClicksMutableLiveData().setValue(Codes.SET_CODE);
            sendCodeForgotPassword();
        } else {
            getClicksMutableLiveData().setValue(Codes.SET_CODE);
            sendCode();
        }
    }


    public SendCodeRequest getSendCodeRequest() {
        return sendCodeRequest;
    }


    private void sendCode() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                SendCodeResponse sendCodeResponse = (SendCodeResponse) response;
                if (sendCodeResponse.getStatus().equals(WebServices.SUCCESS)) {
                    UserItem userItem = UserPreferenceHelper.getUserDetails();
                    userItem.setId(sendCodeResponse.getData().getId());
                    userItem.setJwt(sendCodeResponse.getData().getJwt());
                    userItem.setEmail(sendCodeResponse.getData().getEmail());
                    userItem.setUsersName(sendCodeResponse.getData().getUsersName());
                    userItem.setPhone(sendCodeResponse.getData().getPhone());

                    UserPreferenceHelper.saveUserDetails(userItem);
                    getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);
                } else {
                    setReturnedMessage(sendCodeResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.CHECK_CODE, sendCodeRequest, SendCodeResponse.class);
    }


    private void sendCodeForgotPassword() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                SendCodeResponse sendCodeResponse = (SendCodeResponse) response;
                if (sendCodeResponse.getStatus().equals(WebServices.SUCCESS)) {
                    UserItem userItem = new UserItem();
                    userItem.setJwt(sendCodeResponse.getData().getJwt());
                    UserPreferenceHelper.saveUserDetails(userItem);
                    getClicksMutableLiveData().setValue(Codes.CHANGE_PASSWORD_SCREEN);
                } else {
                    setReturnedMessage(sendCodeResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.CHECK_CODE, sendCodeRequest, SendCodeResponse.class);
    }


}
