package grand.diaaalsalheen.sendcode.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import java.util.Objects;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentSendCodeBinding;
import grand.diaaalsalheen.sendcode.viewmodel.SendCodeViewModel;

public class SendCodeFragment extends BaseFragment {

    private View rootView;
    private  SendCodeViewModel sendCodeViewModel;
    private FragmentSendCodeBinding fragmentSendCodeBinding;
    private boolean isForgotPassword=false;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSendCodeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_send_code, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentSendCodeBinding.getRoot();

        try {
            assert getArguments() != null;
            isForgotPassword = getArguments().getBoolean(Params.IS_FORGOT_PASSWORD,false);


            sendCodeViewModel = new SendCodeViewModel(isForgotPassword,getArguments().getString(Params.BUNDLE_PHONE,""));
        }catch (Exception e){
            sendCodeViewModel = new SendCodeViewModel(false, UserPreferenceHelper.getUserDetails().getEmail());
            e.getStackTrace();
        }

        fragmentSendCodeBinding.setSendCodeViewModel(sendCodeViewModel);
    }


    private void liveDataListeners() {
        sendCodeViewModel.getClicksMutableLiveData().observe(this, result -> {

            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.HOME_SCREEN) {
                MovementManager.startMainActivity(getActivity(),result);
            }else if(result == Codes.SHOW_MESSAGE){
                showMessage(sendCodeViewModel.getReturnedMessage());
            }else if (result == Codes.CHANGE_PASSWORD_SCREEN) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(Params.IS_FORGOT_PASSWORD,true);
                bundle.putString(Params.BUNDLE_PHONE,getArguments().getString(Params.BUNDLE_PHONE));
                MovementManager.startActivity(getActivity(),result,bundle);
            }else if(result == Codes.SET_CODE){
                sendCodeViewModel.getSendCodeRequest().setCode(Objects.requireNonNull(fragmentSendCodeBinding.pvSentCode.getText()).toString());
            }

        });
    }


}