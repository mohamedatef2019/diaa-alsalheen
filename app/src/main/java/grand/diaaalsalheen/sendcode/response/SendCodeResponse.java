package grand.diaaalsalheen.sendcode.response;


import com.google.gson.annotations.SerializedName;

import grand.diaaalsalheen.login.response.UserItem;


public class SendCodeResponse {

    @SerializedName("msg")
    private String msg;

    @SerializedName("status")
    private String status;

    @SerializedName("data")
    private UserItem data;


    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }


    public void setData(UserItem data) {
        this.data = data;
    }

    public UserItem getData() {
        return data;
    }


}