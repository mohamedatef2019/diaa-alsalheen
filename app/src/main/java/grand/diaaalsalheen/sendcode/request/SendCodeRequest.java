package grand.diaaalsalheen.sendcode.request;


import com.google.gson.annotations.SerializedName;

public class SendCodeRequest  {

    @SerializedName("verifications_code")
    private String code;

    private String phone;

    private String email;



	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}