package grand.diaaalsalheen.quran.viewmodel;

import androidx.databinding.Bindable;

import grand.diaaalsalheen.quran.view.AyatAdapter;


public class AyatViewerViewModel extends QuranViewModel {

    private AyatAdapter ayatAdapter;

    public AyatViewerViewModel(int suraNumber) {
        loadQuran();
        ayatAdapter= new AyatAdapter(getSurahByNumber(suraNumber));
    }


    @Bindable
    public AyatAdapter getAyatAdapter() {
        return ayatAdapter;
    }

}
