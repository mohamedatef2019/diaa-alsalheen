package grand.diaaalsalheen.quran.viewmodel;

import androidx.databinding.Bindable;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.quran.response.ReaderItem;
import grand.diaaalsalheen.quran.view.AyatAdapter;
import grand.diaaalsalheen.quran.view.ReaderAdapter;


public class ReadersViewModel extends BaseViewModel {

    private ReaderAdapter readerAdapter;

    public ReadersViewModel() {
        List<ReaderItem> readerItems = new ArrayList<>();




        readerItems.add(new ReaderItem(0, ResourcesManager.getString(R.string.label_abdel_baskit),"ar.abdulbasitmurattal"));
        readerItems.add(new ReaderItem(1, ResourcesManager.getString(R.string.label_abdul_basfar),"ar.abdullahbasfar"));
        readerItems.add(new ReaderItem(2, ResourcesManager.getString(R.string.labal_sudais),"ar.abdurrahmaansudais"));
        readerItems.add(new ReaderItem(3,  ResourcesManager.getString(R.string.label_abdul_samad),"ar.abdulsamad"));
        readerItems.add(new ReaderItem(4,  ResourcesManager.getString(R.string.label_abu_bakr),"ar.shaatree"));
        readerItems.add(new ReaderItem(5,  ResourcesManager.getString(R.string.label_ahmed_ajmi),"ar.ahmedajamy"));
        readerItems.add(new ReaderItem(6,  ResourcesManager.getString(R.string.label_alafasy),"ar.alafasy"));
        readerItems.add(new ReaderItem(7,  ResourcesManager.getString(R.string.label_hani),"ar.hanirifai"));
        readerItems.add(new ReaderItem(8,  ResourcesManager.getString(R.string.label_husary),"ar.husary"));
        readerItems.add(new ReaderItem(9,  ResourcesManager.getString(R.string.label_hausary_mujawwad),"ar.husarymujawwad"));
        readerItems.add(new ReaderItem(10,  ResourcesManager.getString(R.string.label_hudaify),"ar.hudhaify"));
        readerItems.add(new ReaderItem(11,  ResourcesManager.getString(R.string.label_ibrahim_akhdar),"ar.ibrahimakhbar"));
        readerItems.add(new ReaderItem(12,  ResourcesManager.getString(R.string.label_maher_aliqy),"ar.mahermuaiqly"));
        readerItems.add(new ReaderItem(13,  ResourcesManager.getString(R.string.label_minshawy),"ar.minshawi"));
        readerItems.add(new ReaderItem(14,  ResourcesManager.getString(R.string.label_muhamed_ayoub),"ar.muhammadayyoub"));
        readerItems.add(new ReaderItem(15,  ResourcesManager.getString(R.string.label_muhamed_jibreel),"ar.muhammadjibreel"));
        readerItems.add(new ReaderItem(16,  ResourcesManager.getString(R.string.label_soad_bin_ibrahim),"ar.saoodshuraym"));
        readerItems.add(new ReaderItem(17,  ResourcesManager.getString(R.string.label_parhizgar),"ar.parhizgar"));
        readerItems.add(new ReaderItem(18,  ResourcesManager.getString(R.string.label_ayman_sowaid),"ar.aymanswoaid"));

        readerAdapter = new ReaderAdapter(readerItems);
        notifyChange();
    }


    @Bindable
    public ReaderAdapter getReaderAdapter() {
        return readerAdapter;
    }

}
