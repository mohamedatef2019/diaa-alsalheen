
package grand.diaaalsalheen.quran.viewmodel;


import androidx.databinding.Bindable;

import grand.diaaalsalheen.FavouriteRequest;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.favourite.viewmodel.FavouriteViewModel;
import grand.diaaalsalheen.quran.response.quran.AyahsItem;


public class AyahItemViewModel extends BaseItemViewModel {
    private AyahsItem ayahsItem;
    private int position;

    public AyahItemViewModel(AyahsItem ayahsItem, int position) {
        this.ayahsItem = ayahsItem;
        this.position = position;
    }

    @Bindable
    public AyahsItem getAyahsItem() {
        return ayahsItem;
    }


    public boolean isLight() {
        return position % 2 == 0;
    }

    public void onPlayAudioClick() {
        getItemsOperationsLiveListener().setValue(Codes.PLAY_AUDIO);
    }

    public void onAddToFavouriteClick() {
        getItemsOperationsLiveListener().setValue(Codes.ADD_TO_FAVOURITE);
        FavouriteViewModel.addToFavourite(new FavouriteRequest(ayahsItem.getNumber(), "App\\Models\\Aya"));
    }

    public void onShareClick() {
        getItemsOperationsLiveListener().setValue(Codes.SHARE);
    }


}
