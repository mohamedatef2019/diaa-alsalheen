
package grand.diaaalsalheen.quran.viewmodel;

import androidx.databinding.Bindable;

import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.quran.response.QuranItem;


public class QuranItemViewModel extends BaseItemViewModel {
    private QuranItem quranItem;


    public QuranItemViewModel(QuranItem quranItem) {
        this.quranItem = quranItem;
    }




    @Bindable
    public QuranItem getQuranItem() {
        return quranItem;
    }



    public void onItemClick(){
        getItemsOperationsLiveListener().setValue(null);
    }
}
