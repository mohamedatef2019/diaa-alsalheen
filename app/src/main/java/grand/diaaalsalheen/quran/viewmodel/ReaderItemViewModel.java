
package grand.diaaalsalheen.quran.viewmodel;

import androidx.databinding.Bindable;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.quran.response.ReaderItem;


public class ReaderItemViewModel extends BaseItemViewModel {
    private ReaderItem readerItem;
    private int position;

    public ReaderItemViewModel(ReaderItem readerItem, int position) {
        this.readerItem = readerItem;
        this.position=position;
    }


    @Bindable
    public ReaderItem getReaderItem() {
        return readerItem;
    }


    public void onItemItemClicked(){
        getItemsOperationsLiveListener().setValue(null);
    }

    public boolean isLight() {
        return position%2==0;
    }
}
