package grand.diaaalsalheen.quran.viewmodel;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.quran.view.Quran2Adapter;
import grand.diaaalsalheen.quran.view.QuranAdapter;

public class Quran2ViewModel extends BaseViewModel {

    private Quran2Adapter quranAdapter;

    public Quran2ViewModel() {
        quranAdapter= new Quran2Adapter();
    }


    @BindingAdapter({"app:adapter"})
    public static void getPrayerTimesBinding(RecyclerView recyclerView, Quran2Adapter quranAdapter) {
        if (recyclerView.getAdapter() == null) {
            recyclerView.setAdapter(quranAdapter);
        }
    }


    public Quran2Adapter getQuranAdapter() {
        return quranAdapter;
    }

}
