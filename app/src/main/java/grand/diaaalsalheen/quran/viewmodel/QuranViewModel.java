package grand.diaaalsalheen.quran.viewmodel;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;

import androidx.databinding.BindingAdapter;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.MyApplication;
import grand.diaaalsalheen.quran.response.quran.AyahsItem;
import grand.diaaalsalheen.quran.response.quran.QuranResponse;
import grand.diaaalsalheen.quran.response.quran.SurahsItem;
import grand.diaaalsalheen.quran.view.MyPagerAdapter;
import grand.diaaalsalheen.quran.view.QuranAdapter;

public class QuranViewModel extends BaseViewModel {

    private QuranAdapter quranAdapter;

    public QuranViewModel() {
        quranAdapter = new QuranAdapter();
    }


    @BindingAdapter({"app:adapter"})
    public static void getPrayerTimesBinding(RecyclerView recyclerView, QuranAdapter quranAdapter) {
        if (recyclerView.getAdapter() == null) {
            recyclerView.setAdapter(quranAdapter);
        }

    }

    @BindingAdapter({"app:viewPageradapter"})
    public static void getPrayerTimesBinding(ViewPager viewPager, List<String> ayats) {
    /*    MyPagerAdapter quranPagerAdapter =
                new MyPagerAdapter(((FragmentActivity) viewPager.getContext()).getSupportFragmentManager(), ayats);
        viewPager.setAdapter(quranPagerAdapter);*/
    }


    private SparseArray<List<AyahsItem>> ayatByNumber;


    public void loadQuran() {
        ayatByNumber = new SparseArray<>();
        QuranResponse quranResponse;
        ArrayList<String> ayats = new ArrayList<>();
        String json = getJsonFromAssets(MyApplication.getInstance().getApplicationContext());
        quranResponse = new Gson().fromJson(json, QuranResponse.class);


        String[] ayatsTemp = new String[1000];
        List<SurahsItem> surahs = quranResponse.getData().getSurahs();
        for (int i = 1; i < 1000; i++) {
            ayatsTemp[i] = "";
        }
        for (int i = 0; i < surahs.size(); i++) {
            String temp = "";
            List<AyahsItem> ayahs = surahs.get(i).getAyahs();
            ayatByNumber.put(i,ayahs);
            for (int j = 0; j < ayahs.size(); j++) {
                ayatsTemp[ayahs.get(j).getPage()] += " " + ayahs.get(j).getText() + " (" + (j + 1) + ")";
                if ("بِسْمِ اللَّهِ الرَّحْمَٰنِ الرَّحِيمِ".equals(ayahs.get(j).getText())) {
                    ayatsTemp[ayahs.get(j).getPage()] += "\n";
                }
            }
        }

        for (int i = 1; i < ayatsTemp.length; i++) {
            ayats.add(ayatsTemp[i]);
        }

    }

    public List<AyahsItem> getSurahByNumber(int suraNumber){
        return ayatByNumber.get(suraNumber);
    }

    private String getJsonFromAssets(Context context) {
        String jsonString;
        try {
            InputStream is = context.getResources().openRawResource(R.raw.quran);

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            jsonString = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return jsonString;
    }

    public QuranAdapter getQuranAdapter() {
        return quranAdapter;
    }

}
