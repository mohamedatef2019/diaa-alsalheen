
package grand.diaaalsalheen.quran.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.databinding.ItemQuranBinding;
import grand.diaaalsalheen.quran.DataFiller;
import grand.diaaalsalheen.quran.response.QuranItem;
import grand.diaaalsalheen.quran.viewmodel.QuranItemViewModel;


public class QuranAdapter extends RecyclerView.Adapter<QuranAdapter.ViewHolder> {

    private List<QuranItem> quranItems;

    private MutableLiveData<QuranItem> itemsOperationsLiveListener;


    public QuranAdapter() {
        quranItems = DataFiller.getQuran(quranItems);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quran,
                new FrameLayout(parent.getContext()), false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        QuranItem quranItem = quranItems.get(position);

        QuranItemViewModel prayerTimesItemViewModel = new QuranItemViewModel(quranItem);

        prayerTimesItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
            SewarFragment quranViewerMainFragment = new SewarFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(Params.BUNDLE_QURAN_ITEM,quranItem);
            quranViewerMainFragment.setArguments(bundle);
            MovementManager.startActivity(holder.itemView.getContext(), Codes.QURAN_VIEWER_SCREEN,bundle);
        });

      
        holder.setViewModel(prayerTimesItemViewModel);
    }



    public MutableLiveData<QuranItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.quranItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }






    static class ViewHolder extends RecyclerView.ViewHolder {
        ItemQuranBinding itemQuranBinding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemQuranBinding == null) {
                itemQuranBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemQuranBinding != null) {
                itemQuranBinding.unbind();
            }
        }

        void setViewModel(QuranItemViewModel prayerTimesItemViewModel) {
            if (itemQuranBinding != null) {
                itemQuranBinding.setItemQuranViewModel(prayerTimesItemViewModel);
            }
        }


    }



}
