
package grand.diaaalsalheen.quran.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.databinding.ItemReaderBinding;
import grand.diaaalsalheen.quran.response.ReaderItem;
import grand.diaaalsalheen.quran.viewmodel.ReaderItemViewModel;


public class ReaderAdapter extends RecyclerView.Adapter<ReaderAdapter.ViewHolder> {

    private List<ReaderItem> readerItems;
    private int currentPosition = 3;


    private MutableLiveData<ReaderItem> itemsOperationsLiveListener;


    public ReaderAdapter(List<ReaderItem> readerItems) {
        this.readerItems = readerItems;
        String identifier = UserPreferenceHelper.getCurrentReader();
        for(int i=0; i<readerItems.size(); i++){
            if(readerItems.get(i).getIdentifier().equals(identifier)){
                readerItems.get(i).setSelected(true);
                currentPosition=i;
                break;
            }
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reader,
                new FrameLayout(parent.getContext()), false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        ReaderItem readerItem = readerItems.get(position);

        ReaderItemViewModel readerItemViewModel = new ReaderItemViewModel(readerItem,position);
        readerItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
            readerItems.get(currentPosition).setSelected(false);
            notifyItemChanged(currentPosition);
            readerItems.get(position).setSelected(true);
            currentPosition = position;
            UserPreferenceHelper.setCurrentReader(readerItems.get(position).getIdentifier());
            notifyItemChanged(position);

        });

      
        holder.setViewModel(readerItemViewModel);
    }



    public MutableLiveData<ReaderItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.readerItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ItemReaderBinding itemReaderBinding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemReaderBinding == null) {
                itemReaderBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemReaderBinding != null) {
                itemReaderBinding.unbind();
            }
        }

        void setViewModel(ReaderItemViewModel ayahItemViewModel) {
            if (itemReaderBinding != null) {
                itemReaderBinding.setItemReaderViewModel(ayahItemViewModel);
            }
        }


    }



}
