package grand.diaaalsalheen.quran.view;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import grand.diaaalsalheen.QuranPage;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.login.response.UserItem;
import grand.diaaalsalheen.quran.response.quran.AyahsItem;
import grand.diaaalsalheen.quran.response.quran.QuranResponse;
import grand.diaaalsalheen.quran.response.quran.SurahsItem;

public class MyPagerAdapter extends FragmentPagerAdapter {

    List<QuranPage> ayats;


    public MyPagerAdapter(FragmentManager fragmentManager, List<QuranPage> ayats) {
        super(fragmentManager);
        this.ayats = ayats;

    }


    // Returns total number of pages
    @Override
    public int getCount() {
        return ayats.size();
    }

    // Returns the fragment to display for that page
    @NonNull
    @Override
    public Fragment getItem(int position) {
        return QuranPageFragment.newInstance(position, ayats.get(position).getAyat(),  ayats.get(position).getFrom(),  ayats.get(position).getTo());
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }


}


