package grand.diaaalsalheen.quran.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.Fragment2QuranBinding;
import grand.diaaalsalheen.databinding.FragmentQuranBinding;
import grand.diaaalsalheen.quran.viewmodel.Quran2ViewModel;
import grand.diaaalsalheen.quran.viewmodel.QuranViewModel;


public class Quran2Fragment extends BaseFragment {

    View rootView;
    private Quran2ViewModel quran2ViewModel;
    private Fragment2QuranBinding fragment2QuranBinding;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragment2QuranBinding = DataBindingUtil.inflate(inflater, R.layout.fragment2_quran, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
    }

    private void binding() {
        rootView = fragment2QuranBinding.getRoot();
        quran2ViewModel = new Quran2ViewModel();
        fragment2QuranBinding.setQuran2ViewModel(quran2ViewModel);

    }





}