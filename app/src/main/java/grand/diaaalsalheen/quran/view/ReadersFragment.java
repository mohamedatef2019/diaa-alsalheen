package grand.diaaalsalheen.quran.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentAyatsBinding;
import grand.diaaalsalheen.databinding.FragmentReadersBinding;
import grand.diaaalsalheen.quran.viewmodel.AyatViewerViewModel;
import grand.diaaalsalheen.quran.viewmodel.ReadersViewModel;

public class ReadersFragment extends BaseFragment {

    private View rootView;
    private FragmentReadersBinding fragmentReadersBinding;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentReadersBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_readers, container, false);
        init();
        return rootView;
    }

    private void init() {
        binding();
    }

    private void binding() {
        rootView = fragmentReadersBinding.getRoot();
        ReadersViewModel readersViewModel = new ReadersViewModel();
        fragmentReadersBinding.setReadersViewModel(readersViewModel);

        String values = "ي,و,ه,ن,م,ل,ك,ق,ف,غ,ع,ظ,ط,ض,ص,ش,س,ز,ر,ذ,د,خ,ح,ج,ث,ت,ب,ا";
        values = new StringBuilder(values).reverse().toString();
        ArrayAdapter<String>arrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.layout_letter, values.split(","));
        fragmentReadersBinding.rvReadersLetters.setAdapter(arrayAdapter);
    }


}