package grand.diaaalsalheen.quran.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import java.util.Objects;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentMainQuranBinding;
import grand.diaaalsalheen.quran.response.quran.QuranItem;


public class MainQuranViewerFragment extends BaseFragment {

    View rootView;

    private FragmentMainQuranBinding fragmentMainQuranBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMainQuranBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_quran, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
    }

    private void binding() {
        rootView = fragmentMainQuranBinding.getRoot();

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                Objects.requireNonNull(getActivity()).getSupportFragmentManager(), FragmentPagerItems.with(getActivity())
                .add(R.string.swar, SewarFragment.class,getArguments())
                .add(R.string.ayat, AyatFragment.class,getArguments())
                .add(R.string.readers, ReadersFragment.class,getArguments())
                .create());



        fragmentMainQuranBinding.viewpager.setAdapter(adapter);


        fragmentMainQuranBinding.viewpagertab.setViewPager(fragmentMainQuranBinding.viewpager);


    }





}