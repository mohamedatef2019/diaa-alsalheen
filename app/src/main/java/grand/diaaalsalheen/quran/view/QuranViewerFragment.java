package grand.diaaalsalheen.quran.view;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;

import grand.diaaalsalheen.databinding.FragmentQuranViewerBinding;
import grand.diaaalsalheen.quran.response.quran.AyahsItem;
import grand.diaaalsalheen.quran.response.quran.QuranResponse;
import grand.diaaalsalheen.quran.response.quran.SurahsItem;

public class QuranViewerFragment extends BaseFragment {

    private View rootView;
    private FragmentQuranViewerBinding fragmentQuranViewerBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentQuranViewerBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_quran_viewer, container, false);
        init();

        ViewPager vpPager = fragmentQuranViewerBinding.viewpager;



        return rootView;
    }





    private void init() {
        binding();
    }

    private void binding() {
        rootView = fragmentQuranViewerBinding.getRoot();
    }


}