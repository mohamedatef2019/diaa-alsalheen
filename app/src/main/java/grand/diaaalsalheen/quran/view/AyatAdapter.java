
package grand.diaaalsalheen.quran.view;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.huhx0015.hxaudio.audio.HXMusic;
import com.huhx0015.hxaudio.interfaces.HXMusicListener;
import com.huhx0015.hxaudio.model.HXMusicItem;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import grand.diaaalsalheen.BuildConfig;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.SweetAlertDialogss;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.databinding.Item2QuranBinding;
import grand.diaaalsalheen.databinding.ItemAyahBinding;
import grand.diaaalsalheen.quran.DataFiller;
import grand.diaaalsalheen.quran.response.QuranItem;
import grand.diaaalsalheen.quran.response.quran.AyahsItem;
import grand.diaaalsalheen.quran.viewmodel.AyahItemViewModel;
import grand.diaaalsalheen.quran.viewmodel.QuranItemViewModel;


public class AyatAdapter extends RecyclerView.Adapter<AyatAdapter.ViewHolder> {

    private List<AyahsItem> ayahsItems;

    private MutableLiveData<AyahsItem> itemsOperationsLiveListener;


    public AyatAdapter(List<AyahsItem> ayahsItems) {
        this.ayahsItems = ayahsItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ayah,
                new FrameLayout(parent.getContext()), false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        AyahsItem ayahsItem = ayahsItems.get(position);

        AyahItemViewModel ayahItemViewModel = new AyahItemViewModel(ayahsItem, position);

        ayahItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
            if (aVoid == Codes.PLAY_AUDIO) {
                HXMusic.stop();

                Log.e("Results",WebServices.QURAN_READER_AYAH + "" + UserPreferenceHelper.getCurrentReader() + "/" + ayahsItem.getNumber());

                HXMusic.music().load(WebServices.QURAN_READER_AYAH + "" + UserPreferenceHelper.getCurrentReader() + "/" + ayahsItem.getNumber()).title(ayahsItem.getText()).play(holder.itemView.getContext());
                HXMusic.setListener(new HXMusicListener() {
                    @Override
                    public void onMusicPrepared(HXMusicItem music) {
                    }

                    @Override
                    public void onMusicCompletion(HXMusicItem music) {
                    }

                    @Override
                    public void onMusicBufferingUpdate(HXMusicItem music, int percent) {
                    }

                    @Override
                    public void onMusicPause(HXMusicItem music) {
                    }

                    @Override
                    public void onMusicStop(HXMusicItem music) {
                    }
                });


            } else if (aVoid == Codes.ADD_TO_FAVOURITE) {
                if(UserPreferenceHelper.isLogined()){
              SweetAlertDialogss.showFavDialog(holder.itemView.getContext());
            }else {
                SweetAlertDialogss.loginToContinue(holder.itemView.getContext());
            }
            }else if(aVoid == Codes.SHARE){
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);

                sendIntent.putExtra(Intent.EXTRA_TEXT,ayahsItems.get(position).getText()+" \n\n"+"https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                sendIntent.setType("text/plain");
                holder.itemView.getContext().startActivity(sendIntent);
            }
        });

        holder.setViewModel(ayahItemViewModel);
    }


    public MutableLiveData<AyahsItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.ayahsItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ItemAyahBinding itemAyahBinding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemAyahBinding == null) {
                itemAyahBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemAyahBinding != null) {
                itemAyahBinding.unbind();
            }
        }

        void setViewModel(AyahItemViewModel ayahItemViewModel) {
            if (itemAyahBinding != null) {
                itemAyahBinding.setItemAyahViewModel(ayahItemViewModel);
            }
        }


    }


}
