package grand.diaaalsalheen.quran.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentPrayerTimesBinding;
import grand.diaaalsalheen.databinding.FragmentQuranBinding;
import grand.diaaalsalheen.quran.viewmodel.QuranViewModel;


public class QuranFragment extends BaseFragment {

    View rootView;
    private QuranViewModel quranViewModel;
    private FragmentQuranBinding fragmentQuranBinding;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentQuranBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_quran, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
    }

    private void binding() {
        rootView = fragmentQuranBinding.getRoot();
        quranViewModel = new QuranViewModel();
        fragmentQuranBinding.setQuranViewModel(quranViewModel);

    }





}