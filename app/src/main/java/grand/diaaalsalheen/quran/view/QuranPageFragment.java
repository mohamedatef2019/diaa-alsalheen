package grand.diaaalsalheen.quran.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.huhx0015.hxaudio.audio.HXMusic;
import com.huhx0015.hxaudio.audio.HXSound;
import com.huhx0015.hxaudio.interfaces.HXMusicListener;
import com.huhx0015.hxaudio.model.HXMusicItem;

import java.util.Timer;
import java.util.TimerTask;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentQuranBinding;
import grand.diaaalsalheen.quran.viewmodel.QuranViewModel;
import me.grantland.widget.AutofitHelper;

public class QuranPageFragment extends Fragment {

    private String title;
    private int page;
    private int fromAya, toAya;
    private int currentAya,lastIndex=-30;

    public static QuranPageFragment newInstance(int page, String title, int fromAya, int toAya) {
        QuranPageFragment fragmentFirst = new QuranPageFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        args.putInt("fromAya", fromAya);
        args.putInt("toAya", toAya);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
        fromAya = getArguments().getInt("fromAya", 0);
        toAya = getArguments().getInt("toAya", 0);
        currentAya = fromAya-1;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quran_page, container, false);
        TextView tvLabel = view.findViewById(R.id.tvLabel);

        HXSound.reinitialize(getActivity());

        ImageView playImageView, pointerImageView, nextImageView, previousImageView;
        SeekBar currentTimeSeekBar;

        playImageView = view.findViewById(R.id.iv_play);
        pointerImageView = view.findViewById(R.id.iv_pointer);
        nextImageView = view.findViewById(R.id.iv_next);
        previousImageView = view.findViewById(R.id.iv_previous);
        currentTimeSeekBar = view.findViewById(R.id.sb_loading);

        currentTimeSeekBar.setMax(toAya - fromAya);

        currentTimeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                HXMusic.stop();
                HXMusic.music().load(WebServices.QURAN_READER_AYAH + "" + UserPreferenceHelper.getCurrentReader() + "/" + currentAya).title(title).play(getActivity());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        playImageView.setOnClickListener(view1 -> {
            if (HXMusic.isPlaying()) {

                    HXMusic.pause();
                    playImageView.setImageResource(R.drawable.ic_play);

            }else {
                 if(lastIndex==currentAya){
                HXMusic.resume(getActivity());
                     playImageView.setImageResource(R.drawable.ic_pause);

                 }else {
                playImageView.setImageResource(R.drawable.ic_pause);
                playQuran();
                    }
            }
        });

        pointerImageView.setOnClickListener(view12 -> {
            if (HXMusic.isPlaying()) {

                playQuran();
            }
        });

        nextImageView.setOnClickListener(view13 -> {
            if (HXMusic.isPlaying()) {
                currentAya++;
                playQuran();
            }
        });

        previousImageView.setOnClickListener(view14 -> {
            if (HXMusic.isPlaying()) {
                currentAya--;
                playQuran();
            }
        });


        AutofitHelper.create(tvLabel);
        tvLabel.setText(title);

        Typeface font = null;
        font = Typeface.createFromAsset(getContext().getAssets(), "maddina.otf");
        tvLabel.setTypeface(font);


        return view;
    }

    @Override
    public void onDestroy() {
        HXMusic.stop();
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        HXMusic.stop();
        super.onDestroyView();
    }

    private void playQuran() {
        lastIndex = currentAya;
        HXMusic.stop();
        Log.e("Results",WebServices.QURAN_READER_AYAH + "" + UserPreferenceHelper.getCurrentReader() + "/" + currentAya);

        HXMusic.music().load(WebServices.QURAN_READER_AYAH + "" + UserPreferenceHelper.getCurrentReader() + "/" + currentAya).title(title).play(getActivity());
        HXMusic.setListener(new HXMusicListener() {
            @Override
            public void onMusicPrepared(HXMusicItem music) {

            }

            @Override
            public void onMusicCompletion(HXMusicItem music) {
                if (currentAya < toAya) {
                    currentAya++;
                    playQuran();
                } else {
                    currentAya = 0;
                }
            }

            @Override
            public void onMusicBufferingUpdate(HXMusicItem music, int percent) {

            }

            @Override
            public void onMusicPause(HXMusicItem music) {

            }

            @Override
            public void onMusicStop(HXMusicItem music) {

            }
        });
    }
}