package grand.diaaalsalheen.quran.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentAyatsBinding;
import grand.diaaalsalheen.quran.response.QuranItem;
import grand.diaaalsalheen.quran.viewmodel.AyatViewerViewModel;

public class AyatFragment extends BaseFragment {

    private View rootView;
    private FragmentAyatsBinding fragmentAyatsBinding;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAyatsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_ayats, container, false);
        init();
        return rootView;
    }

    private void init() {
        binding();
    }

    private void binding() {
        rootView = fragmentAyatsBinding.getRoot();
        assert getArguments() != null;
        QuranItem quranItem = (QuranItem) getArguments().getSerializable(Params.BUNDLE_QURAN_ITEM);
        assert quranItem != null;
        AyatViewerViewModel ayatViewerViewModel = new AyatViewerViewModel(quranItem.getIndex());
        fragmentAyatsBinding.setAyatsViewModel(ayatViewerViewModel);
    }


}