package grand.diaaalsalheen.quran.view;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import grand.diaaalsalheen.QuranPage;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentQuranViewerBinding;
import grand.diaaalsalheen.quran.response.QuranItem;
import grand.diaaalsalheen.quran.response.quran.AyahsItem;
import grand.diaaalsalheen.quran.response.quran.QuranResponse;
import grand.diaaalsalheen.quran.response.quran.SurahsItem;

public class SewarFragment extends BaseFragment {

    private  View rootView;
    private FragmentQuranViewerBinding fragmentQuranViewerBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentQuranViewerBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_quran_viewer, container, false);
        init();

        ViewPager vpPager = fragmentQuranViewerBinding.viewpager;

        QuranResponse quranResponse;

        assert getArguments() != null;
        QuranItem quranItem = (QuranItem) getArguments().getSerializable(Params.BUNDLE_QURAN_ITEM);

        String json = getJsonFromAssets(getActivity());
        quranResponse = new Gson().fromJson(json, QuranResponse.class);

        String[] ayatsTemp = new String[1000];
        List<SurahsItem> surahs = quranResponse.getData().getSurahs();
        for (int i = 1; i < 1000; i++) {
            ayatsTemp[i]="";
        }
       // for (int i = 0; i < surahs.size(); i++) {


        assert quranItem != null;
        List<AyahsItem> ayahs = surahs.get(quranItem.getIndex()).getAyahs();
            for (int j = 0; j < ayahs.size(); j++) {
                ayatsTemp[ayahs.get(j).getPage()] += " " +ayahs.get(j).getText() +" ("+(j+1)+")";
                if("بِسْمِ اللَّهِ الرَّحْمَٰنِ الرَّحِيمِ".equals(ayahs.get(j).getText())){
                    ayatsTemp[ayahs.get(j).getPage()] +="\n";
                }
                try {
                    if (ayahs.get(j).getSajda()) {
                        ayatsTemp[ayahs.get(j).getPage()] += "(اسجد)";
                    }
                }catch (Exception e){
                    e.getStackTrace();
                }
            }
       // }

        ArrayList<QuranPage> ayats = new ArrayList<>();

        for (int i = 1; i < ayatsTemp.length; i++) {
            if(!ayatsTemp[i].equals(""))
            ayats.add(new QuranPage(ayatsTemp[i],ayahs.get(0).getNumber(),ayahs.get(ayahs.size()-1).getNumber()));
        }


        MyPagerAdapter adapterViewPager = new MyPagerAdapter(getChildFragmentManager(), ayats);
        vpPager.setAdapter(adapterViewPager);




        return rootView;
    }


    public static String getJsonFromAssets(Context context) {
        String jsonString;
        try {
            InputStream is = context.getResources().openRawResource(R.raw.quran);

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            jsonString = new String(buffer, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return jsonString;
    }


    private void init() {
        binding();
    }

    private void binding() {
        rootView = fragmentQuranViewerBinding.getRoot();
    }


}