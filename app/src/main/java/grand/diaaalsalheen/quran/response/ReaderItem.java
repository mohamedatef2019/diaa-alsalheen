package grand.diaaalsalheen.quran.response;

import com.google.gson.annotations.Expose;

public class ReaderItem {
    private int id;
    private String readerName;
    private String identifier;

    @Expose
    private boolean isSelected;

    public ReaderItem(int id, String readerName,String identifier) {
        this.id = id;
        this.readerName = readerName;
        this.identifier=identifier;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setReaderName(String readerName) {
        this.readerName = readerName;
    }

    public int getId() {
        return id;
    }

    public String getReaderName() {
        return readerName;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public String getIdentifier() {
        return identifier;
    }
}
