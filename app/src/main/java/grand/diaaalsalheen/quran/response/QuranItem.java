package grand.diaaalsalheen.quran.response;

import java.io.Serializable;

public class QuranItem implements Serializable {

  private String name,part,type,number;
  private int index=0;


    public QuranItem(String name,String part,String type,String number){
        this.name=name;
        this.part=part;
        this.type=type;
        this.number=number;
        this.index=Integer.parseInt(number)-1;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getPart() {
        return part;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
