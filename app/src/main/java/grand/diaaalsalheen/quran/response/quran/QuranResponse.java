package grand.diaaalsalheen.quran.response.quran;


import com.google.gson.annotations.SerializedName;


public class QuranResponse {

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private QuranItem data;

	@SerializedName("status")
	private String status;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(QuranItem data){
		this.data = data;
	}

	public QuranItem getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"code = '" + code + '\'' + 
			",data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}