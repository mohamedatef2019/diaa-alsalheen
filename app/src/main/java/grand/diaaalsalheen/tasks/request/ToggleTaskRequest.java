package grand.diaaalsalheen.tasks.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ToggleTaskRequest {

    @SerializedName("status")
    int status;

    @SerializedName("task_id")
    int taskId;


    @Expose
    @SerializedName("date")
    private String date;



    public ToggleTaskRequest(int taskId , int status ,String date){
        this.status = status;
        this.taskId = taskId;
        this.date=date;
    }
}
