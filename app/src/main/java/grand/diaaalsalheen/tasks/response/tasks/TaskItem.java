package grand.diaaalsalheen.tasks.response.tasks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaskItem {

	@SerializedName("name")
	private String name;
	@SerializedName("id")
	private int id;




    @Expose
    private int status;
    @Expose
    private int action;
	@Expose
    private int progress;
    @Expose
    private String day;

    @Expose
    private boolean isSelected;




    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getProgress() {
        return progress;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDay() {
        return day;
    }

    public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

    public void setAction(int action) {
        this.action = action;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAction() {
        return action;
    }

    public int getStatus() {
        return status;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }


}