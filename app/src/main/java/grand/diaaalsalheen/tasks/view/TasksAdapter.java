
package grand.diaaalsalheen.tasks.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.databinding.ItemTaskBinding;
import grand.diaaalsalheen.tasks.response.tasks.TaskItem;
import grand.diaaalsalheen.tasks.viewmodel.TasksItemViewModel;

public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.CompaniesViewHolder> {

    private List<TaskItem> taskItems;
    private MutableLiveData<TaskItem> itemsOperationsLiveListener;


    public TasksAdapter() {
        this.taskItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {
        TaskItem taskItem = taskItems.get(position);
        TasksItemViewModel tasksItemViewModel = new TasksItemViewModel(taskItem,position);
        tasksItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {


            taskItem.setAction(aVoid);
            itemsOperationsLiveListener.setValue(taskItem);

            if(aVoid!=null && aVoid == Codes.DELETE_TASK) {

                taskItems.remove(position);
                notifyDataSetChanged();
            }
        });
        holder.setViewModel(tasksItemViewModel);
    }



    public MutableLiveData<TaskItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.taskItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<TaskItem> data) {

            this.taskItems.clear();

        assert data != null;
        this.taskItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemTaskBinding itemTaskBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemTaskBinding == null) {
                itemTaskBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemTaskBinding != null) {
                itemTaskBinding.unbind();
            }
        }

        void setViewModel(TasksItemViewModel taskItemViewModel) {
            if (itemTaskBinding != null) {
                itemTaskBinding.setItemTaskViewModel(taskItemViewModel);
            }
        }


    }



}
