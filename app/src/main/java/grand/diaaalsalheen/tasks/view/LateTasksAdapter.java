
package grand.diaaalsalheen.tasks.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.databinding.ItemLateTaskBinding;
import grand.diaaalsalheen.databinding.ItemProgressTaskBinding;
import grand.diaaalsalheen.tasks.response.tasks.TaskItem;
import grand.diaaalsalheen.tasks.viewmodel.TasksItemViewModel;
import grand.diaaalsalheen.tasks.viewmodel.TasksProgressItemViewModel;

public class LateTasksAdapter extends RecyclerView.Adapter<LateTasksAdapter.CompaniesViewHolder> {

    private List<TaskItem> taskItems;
    private MutableLiveData<TaskItem> itemsOperationsLiveListener;


    public LateTasksAdapter() {
        this.taskItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_late_task,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {
        TaskItem taskItem = taskItems.get(position);
        TasksItemViewModel tasksItemViewModel = new TasksItemViewModel(taskItem, position);
        tasksItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
            getItemsOperationsLiveListener().setValue(taskItem);
        });
        holder.setViewModel(tasksItemViewModel);
    }


    public MutableLiveData<TaskItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.taskItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<TaskItem> data) {
        this.taskItems.clear();
        assert data != null;
        this.taskItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemLateTaskBinding itemProgressTaskBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemProgressTaskBinding == null) {
                itemProgressTaskBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemProgressTaskBinding != null) {
                itemProgressTaskBinding.unbind();
            }
        }

        void setViewModel(TasksItemViewModel tasksProgressItemViewModel) {
            if (itemProgressTaskBinding != null) {
                itemProgressTaskBinding.setItemTaskViewModel(tasksProgressItemViewModel);
            }
        }


    }


}
