
package grand.diaaalsalheen.tasks.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.databinding.ItemProgressTaskBinding;
import grand.diaaalsalheen.tasks.response.tasks.TaskItem;
import grand.diaaalsalheen.tasks.viewmodel.TasksProgressItemViewModel;

public class TasksProgressAdapter extends RecyclerView.Adapter<TasksProgressAdapter.CompaniesViewHolder> {

    private List<TaskItem> taskItems;
    private MutableLiveData<TaskItem> itemsOperationsLiveListener;
    private int currentPosition = 3;

    public TasksProgressAdapter() {
        this.taskItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress_task,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {
        TaskItem taskItem = taskItems.get(position);
        TasksProgressItemViewModel tasksItemViewModel = new TasksProgressItemViewModel(taskItem, position);
        tasksItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
            taskItems.get(currentPosition).setSelected(false);
            notifyItemChanged(currentPosition);
            taskItems.get(position).setSelected(true);
            currentPosition = position;
            notifyItemChanged(position);
            itemsOperationsLiveListener.setValue(taskItem);
        });
        holder.setViewModel(tasksItemViewModel);
    }


    public MutableLiveData<TaskItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.taskItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<TaskItem> data) {
        this.taskItems.clear();
        assert data != null;
        this.taskItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemProgressTaskBinding itemProgressTaskBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemProgressTaskBinding == null) {
                itemProgressTaskBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemProgressTaskBinding != null) {
                itemProgressTaskBinding.unbind();
            }
        }

        void setViewModel(TasksProgressItemViewModel tasksProgressItemViewModel) {
            if (itemProgressTaskBinding != null) {
                itemProgressTaskBinding.setItemProgressTaskViewModel(tasksProgressItemViewModel);
            }
        }


    }


}
