package grand.diaaalsalheen.tasks.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.beads.view.TasbehFragment;
import grand.diaaalsalheen.beads.view.ZahrahTasbehFragment;
import grand.diaaalsalheen.databinding.FragmentAzkarDetailsBinding;
import grand.diaaalsalheen.databinding.FragmentMainTasksBinding;


public class MainTasksFragment extends BaseFragment {
    private View rootView;

    private FragmentMainTasksBinding fragmentMainTasksBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMainTasksBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_tasks, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getChildFragmentManager(), FragmentPagerItems.with(getActivity())
                .add(R.string.label_tasks, TasksFragment.class)
                .add(R.string.label_late_tasks, LateTasksFragment.class)
                .create());

        fragmentMainTasksBinding.viewpager.setAdapter(adapter);
        fragmentMainTasksBinding.viewpagertab.setViewPager(fragmentMainTasksBinding.viewpager);

    }

    private void binding() {
        rootView = fragmentMainTasksBinding.getRoot();

    }







}