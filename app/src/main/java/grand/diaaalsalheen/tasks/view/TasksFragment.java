package grand.diaaalsalheen.tasks.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentTasksBinding;
import grand.diaaalsalheen.tasks.viewmodel.TasksViewModel;


public class TasksFragment extends BaseFragment {
    private View rootView;

    private FragmentTasksBinding fragmentTasksBinding;
    private TasksViewModel tasksViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentTasksBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_tasks, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
    }

    private void binding() {
        rootView = fragmentTasksBinding.getRoot();
        tasksViewModel = new TasksViewModel(false);
        fragmentTasksBinding.setTasksViewModel(tasksViewModel);
        liveDataListeners();
    }

    private void liveDataListeners() {
        tasksViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });

        if(tasksViewModel.getTasksAdapter()!=null) {
            tasksViewModel.getTasksAdapter().getItemsOperationsLiveListener().observe(this, taskItem -> {
                if (taskItem.getAction() == Codes.DELETE_TASK || taskItem.getAction() == Codes.ADD_TASK) {
                    tasksViewModel.toggleTask(taskItem);
                }
            });
        }
        if(tasksViewModel.getTasksProgressAdapter()!=null) {
            tasksViewModel.getTasksProgressAdapter().getItemsOperationsLiveListener().observe(this, taskItem -> {

                tasksViewModel.setCurrentDate(taskItem.getName());
                tasksViewModel.notifyChange();

            });
        }
    }


}