package grand.diaaalsalheen.tasks.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentLateTasksBinding;
import grand.diaaalsalheen.tasks.viewmodel.TasksViewModel;


public class LateTasksFragment extends BaseFragment {
    private View rootView;

    private FragmentLateTasksBinding fragmentLateTasksBinding;

    private TasksViewModel tasksViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentLateTasksBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_late_tasks, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
    }

    private void binding() {
        rootView = fragmentLateTasksBinding.getRoot();
        tasksViewModel = new TasksViewModel(true);
        fragmentLateTasksBinding.setLateTasksViewModel(tasksViewModel);
        liveDataListeners();
    }

    private void liveDataListeners() {
        tasksViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });

        if( tasksViewModel.getTasksAdapter()!=null) {
            tasksViewModel.getTasksAdapter().getItemsOperationsLiveListener().observe(this, taskItem -> {
                if (taskItem.getAction() == Codes.DELETE_TASK || taskItem.getAction() == Codes.ADD_TASK) {
                    tasksViewModel.toggleTask(taskItem);
                }
            });
        }


        if( tasksViewModel.getTasksProgressAdapter()!=null) {
            tasksViewModel.getTasksProgressAdapter().getItemsOperationsLiveListener().observe(this, taskItem -> {
                tasksViewModel.setCurrentDate(taskItem.getName());
                tasksViewModel.notifyChange();

            });
        }
    }








}