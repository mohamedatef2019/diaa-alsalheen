
package grand.diaaalsalheen.tasks.viewmodel;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.skydoves.progressview.ProgressView;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.tasks.response.tasks.TaskItem;
import grand.diaaalsalheen.tasks.view.TasksProgressAdapter;


public class TasksProgressItemViewModel extends BaseItemViewModel {

    private TaskItem taskItem;
    private int position;

    public TasksProgressItemViewModel(TaskItem taskItem, int position) {
        this.taskItem = taskItem;
        this.position=position;
    }


    @Bindable
    public TaskItem getTaskItem() {
        return taskItem;
    }

    public void onTaskItemClicked(){
        getItemsOperationsLiveListener().setValue(Codes.ZYARAT_DETAILS);

    }

    public boolean isLight() {
        return position%2==0;
    }



    @BindingAdapter({"app:progress_performance"})
    public static void getProgressBinding(ProgressView progressView, int progress) {
        progressView.setProgress(progress);
    }
}
