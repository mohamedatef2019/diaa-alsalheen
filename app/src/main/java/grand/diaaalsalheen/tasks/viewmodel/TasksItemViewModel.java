
package grand.diaaalsalheen.tasks.viewmodel;

import androidx.databinding.Bindable;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.tasks.response.tasks.TaskItem;


public class TasksItemViewModel extends BaseItemViewModel {

    private TaskItem taskItem;
    private int position;

    public TasksItemViewModel(TaskItem taskItem,int position) {
        this.taskItem = taskItem;
        this.position=position;
    }


    @Bindable
    public TaskItem getTaskItem() {
        return taskItem;
    }

    public void onTaskItemClicked(){
        getItemsOperationsLiveListener().setValue(Codes.ZYARAT_DETAILS);
    }

    public void onTaskItemDelete(){

        getItemsOperationsLiveListener().setValue(Codes.DELETE_TASK);
    }

    public void onTaskItemAdded(){
        getItemsOperationsLiveListener().setValue(Codes.ADD_TASK);
    }



    public boolean isLight() {
        return position%2==0;
    }

}
