package grand.diaaalsalheen.tasks.viewmodel;

import android.annotation.SuppressLint;
import android.os.Build;
import android.util.Log;
import android.view.View;

import androidx.annotation.RequiresApi;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.model.DefaultResponse;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.base.volleyutils.MyApplication;
import grand.diaaalsalheen.home.viewmodel.ImagesViewModel;
import grand.diaaalsalheen.tasks.request.ToggleTaskRequest;
import grand.diaaalsalheen.tasks.response.tasks.TaskItem;
import grand.diaaalsalheen.tasks.response.tasks.TasksResponse;
import grand.diaaalsalheen.tasks.view.LateTasksAdapter;
import grand.diaaalsalheen.tasks.view.LateTasksDatesAdapter;
import grand.diaaalsalheen.tasks.view.TasksAdapter;
import grand.diaaalsalheen.tasks.view.TasksDatesAdapter;
import grand.diaaalsalheen.tasks.view.TasksProgressAdapter;


public class TasksViewModel extends ImagesViewModel {
    private TasksAdapter tasksAdapter;
    private TasksProgressAdapter tasksProgressAdapter;
    private TasksDatesAdapter tasksDatesAdapter;
    private LateTasksAdapter lateTasksAdapter;
    private LateTasksDatesAdapter lateTasksDatesAdapter;
    private String currentDate;
    private Calendar cal;
    private List<TaskItem> dates;

    @SuppressLint("NewApi")
    public TasksViewModel(boolean isLate) {
        cal = new UmmalquraCalendar();
        dates = new ArrayList<>();
        addDates(3, -1);
        addDates(1, 3);
        addDates(100, 1);

        if (isLate) {
            lateTasksAdapter = new LateTasksAdapter();
            lateTasksDatesAdapter = new LateTasksDatesAdapter();
            lateTasksDatesAdapter.updateData(dates);
            getLateTasks();
        } else {
            tasksAdapter = new TasksAdapter();
            tasksProgressAdapter = new TasksProgressAdapter();
            tasksDatesAdapter = new TasksDatesAdapter();
            tasksProgressAdapter.updateData(dates);
            getTasks();
        }


        notifyChange();
    }


    @BindingAdapter({"app:tasks_adapter"})
    public static void getLateTasksBinding(RecyclerView recyclerView, TasksAdapter tasksAdapter) {
        if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(tasksAdapter);
    }


    @BindingAdapter({"app:tasks_progress_adapter"})
    public static void getTasksProgressBinding(RecyclerView recyclerView, TasksProgressAdapter tasksProgressAdapter) {
        if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(tasksProgressAdapter);
    }

    @BindingAdapter({"app:late_tasks_date_adapter"})
    public static void getLateTasksProgressBinding(RecyclerView recyclerView, LateTasksDatesAdapter lateTasksDatesAdapter) {
        if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(lateTasksDatesAdapter);
    }

    @BindingAdapter({"app:late_tasks_adapter"})
    public static void getLateTasksBinding(RecyclerView recyclerView, LateTasksAdapter lateTasksAdapter) {
        if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(lateTasksAdapter);
    }


    @Bindable
    public TasksAdapter getTasksAdapter() {
        return this.tasksAdapter;
    }

    @Bindable
    public TasksDatesAdapter getTasksDatesAdapter() {
        return this.tasksDatesAdapter;
    }

    @Bindable
    public TasksProgressAdapter getTasksProgressAdapter() {
        return tasksProgressAdapter;
    }

    public LateTasksDatesAdapter getLateTasksDatesAdapter() {
        return lateTasksDatesAdapter;
    }


    private void getTasks() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                TasksResponse tasksResponse = (TasksResponse) response;
                tasksAdapter.updateData(tasksResponse.getData());
                accessLoadingBar(View.GONE);
                notifyChange();


            }
        }).requestJsonObject(Request.Method.GET, WebServices.TASKS + getCurrentDateString(), null, TasksResponse.class);
    }

    public static String getCurrentDateString() {


        Date now = Calendar.getInstance().getTime();


        return (now.getMonth() + 1) + "-" + now.getDate() + "-" + (1900 + now.getYear());
    }


    public static String getCurrentDateString2() {
        Calendar now = Calendar.getInstance();
        return (now.get(Calendar.YEAR)) + "-" + ((now.get(Calendar.MONTH) + 1) < 10 ? "0" : "") + (now.get(Calendar.MONTH) + 1) + "-" + (now.get(Calendar.DAY_OF_MONTH) < 10 ? "0" : "") +  now.get(Calendar.DAY_OF_MONTH);
    }

    private void getLateTasks() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                TasksResponse tasksResponse = (TasksResponse) response;

                getLateTasksAdapter().updateData(tasksResponse.getData());
                Log.e("LATETASKS", tasksResponse.getData().size() + "");
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.FINISHED_TASKS + getCurrentDateString(), null, TasksResponse.class);
    }

    public LateTasksAdapter getLateTasksAdapter() {
        return lateTasksAdapter;
    }

    public void toggleTask(TaskItem taskItem) {


        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.TOOGLE_TASK, new ToggleTaskRequest(taskItem.getId(), taskItem.getAction() == Codes.DELETE_TASK ? 1 : 0, getCurrentDateString()), DefaultResponse.class);

    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void addDates(int numberOfDates, int addOrSub) {
        for (int i = 0; i < numberOfDates; i++) {
            cal.add(Calendar.DATE, addOrSub);
            TaskItem taskItem = new TaskItem();
            taskItem.setSelected(addOrSub == 3);

            taskItem.setProgress(i + 1);
            taskItem.setId(i + 1);
            taskItem.setDay("" + cal.get(Calendar.DAY_OF_MONTH));
            taskItem.setName(cal.get(Calendar.DAY_OF_MONTH) + " " + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.forLanguageTag(UserPreferenceHelper.getCurrentLanguage(MyApplication.getInstance().getApplicationContext()))) + " " + cal.get(Calendar.YEAR) + " ه ");
            dates.add(taskItem);

            if (addOrSub == 3) {
                currentDate = taskItem.getName();
            }
            Log.e("ONDATE", cal.get(Calendar.DAY_OF_MONTH) + " " + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.forLanguageTag(UserPreferenceHelper.getCurrentLanguage(MyApplication.getInstance().getApplicationContext()))));
        }
    }


}
