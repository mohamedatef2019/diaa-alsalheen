package grand.diaaalsalheen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;

public class ZoomAbleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_able);

        ImageView imageView = findViewById(R.id.myZoomageView);

        ConnectionHelper.loadImage(imageView,getIntent().getStringExtra("image"));

    }
}