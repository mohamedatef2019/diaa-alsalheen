package grand.diaaalsalheen.fcmmanager;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.huhx0015.hxaudio.audio.HXMusic;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.activities.MainActivity;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.MyApplication;


public class MyFirebaseMessagingService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        String title = "", messsage = "", image = null, type = "1";
        if (remoteMessage.getData().size() > 0) {


            title = remoteMessage.getData().get("title");
            messsage = remoteMessage.getData().get("body");
            type = remoteMessage.getData().get("type");

            try {
                image = remoteMessage.getData().get("details");
            } catch (Exception e) {
                e.getStackTrace();
            }

            Log.e("Testings", remoteMessage.getData().toString());


            sendNotification(title, messsage, image, type,remoteMessage.getData().get("id"));

        }

        if (remoteMessage.getNotification() != null) {
            sendNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), image, type,"0");
        }


    }


    @Override
    public void onNewToken(String token) {
        UserPreferenceHelper.saveGoogleToken(token);
        System.out.println("Refreshed token:" + token);
    }


    NotificationCompat.Builder notificationBuilder;

    private void sendNotification(String title, String body, String image, String type, String id) {


        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = "channelId";

        Uri defaultSoundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + MyApplication.getInstance().getPackageName() + "/" + R.raw.azansmall);

        if (type.equals("6")) {
            try {
                if (UserPreferenceHelper.getPrayerAvailable(Integer.parseInt(id))) {
                    if (UserPreferenceHelper.getMazhab().equals("shia")) {
                        HXMusic.music().load(R.raw.azanshi3a).play(this);

                    } else {
                        HXMusic.music().load(UserPreferenceHelper.getCurrentAzan(this).equals("big") ? R.raw.azanbig : R.raw.azansmall).play(this);

                    }
                }
            } catch (Exception e) {
                e.getStackTrace();
            }

        }


        ConnectionHelper.loadImage(image, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                notificationBuilder =
                        new NotificationCompat.Builder(MyFirebaseMessagingService.this, channelId)
                                .setSmallIcon(R.mipmap.ic_launcher)

                                .setContentTitle(title)
                                .setContentText(body)

                                .setAutoCancel(true)
                                .setSound(defaultSoundUri)
                                .setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                // Since android Oreo notification channel is needed.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel channel = new NotificationChannel(channelId,
                            "Channel human readable title",
                            NotificationManager.IMPORTANCE_DEFAULT);
                    notificationManager.createNotificationChannel(channel);
                }

                notificationManager.notify(0, notificationBuilder.build());
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                Log.e("Testings", title + "  " + image);


                notificationBuilder =
                        new NotificationCompat.Builder(MyFirebaseMessagingService.this, channelId)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentTitle(title)
                                .setContentText(body)
                                .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(loadedImage))
                                .setAutoCancel(true)
                                .setSound(defaultSoundUri)
                                .setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                // Since android Oreo notification channel is needed.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel channel = new NotificationChannel(channelId,
                            "Channel human readable title",
                            NotificationManager.IMPORTANCE_DEFAULT);
                    notificationManager.createNotificationChannel(channel);
                }

                notificationManager.notify(0, notificationBuilder.build());
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });


    }
}