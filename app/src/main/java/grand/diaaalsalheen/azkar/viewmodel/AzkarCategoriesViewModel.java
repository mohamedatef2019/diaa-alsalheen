package grand.diaaalsalheen.azkar.viewmodel;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import grand.diaaalsalheen.azkar.response.AzkarCategoriesResponse;
import grand.diaaalsalheen.azkar.view.AzkarCategoriesAdapter;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.home.viewmodel.ImagesViewModel;


public class AzkarCategoriesViewModel extends ImagesViewModel {

    private AzkarCategoriesAdapter azkarCategoriesAdapter;


    public AzkarCategoriesViewModel() {
        getAzkar();
    }


    @BindingAdapter({"app:adapter"})
    public static void getCategoriesBinding(RecyclerView recyclerView, AzkarCategoriesAdapter azkarCategoriesAdapter) {
        recyclerView.setAdapter(azkarCategoriesAdapter);
    }


    @Bindable
    public AzkarCategoriesAdapter getAzkarCategoriesAdapter() {
        return this.azkarCategoriesAdapter == null ? this.azkarCategoriesAdapter = new AzkarCategoriesAdapter() : this.azkarCategoriesAdapter;
    }


    private void getAzkar() {
         accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                AzkarCategoriesResponse azkarCategoriesResponse = (AzkarCategoriesResponse) response;
                getAzkarCategoriesAdapter().updateData(azkarCategoriesResponse.getData());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.AZKAR_CATEGORIES,null,   AzkarCategoriesResponse.class);


    }
}
