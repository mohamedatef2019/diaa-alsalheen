
package grand.diaaalsalheen.azkar.viewmodel;

import android.util.Log;
import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import grand.diaaalsalheen.azkar.response.AzkarCategoriesItem;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;


public class AzkarCategoriesItemViewModel extends BaseItemViewModel {
    private AzkarCategoriesItem azkarCategoriesItem;


    public AzkarCategoriesItemViewModel(AzkarCategoriesItem azkarCategoriesItem) {
        this.azkarCategoriesItem = azkarCategoriesItem;
    }


    @Bindable
    public AzkarCategoriesItem getAzkarCategoriesItem() {
        return azkarCategoriesItem;
    }

    public void onMonagatCategoryItemClicked(){
        getItemsOperationsLiveListener().setValue(Codes.MONGAT_DETAILS);
    }

    @BindingAdapter({"imageUrl"})
    public static void setImageUrl(ImageView view, String imagePath){

        ConnectionHelper.loadImage(view,  imagePath);
    }


}
