package grand.diaaalsalheen.azkar.viewmodel;

import android.view.View;

import com.android.volley.Request;

import java.util.List;

import grand.diaaalsalheen.FavouriteRequest;
import grand.diaaalsalheen.azkar.response.AzkarCategoriesItem;
import grand.diaaalsalheen.azkar.response.azkardetailsresponse.AzkarDetailsItem;
import grand.diaaalsalheen.azkar.response.azkardetailsresponse.AzkarDetailsResponse;
import grand.diaaalsalheen.azkar.response.azkardetailsresponse.ZekrItem;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.doaa.response.DoaaDetailsItem;
import grand.diaaalsalheen.doaa.response.DoaaDetailsResponse;
import grand.diaaalsalheen.doaa.response.SubcategoriesItem;
import grand.diaaalsalheen.favourite.viewmodel.FavouriteViewModel;


public class AzkarDetailsViewModel extends BaseViewModel {


    private int currentZekr = 0;
    private int numberOfZekr=1;
    private int count=0;
    private List<ZekrItem> zekrItems;
    public AzkarDetailsViewModel(AzkarCategoriesItem azkarCategoriesItem) {

        getAzkarDetails(azkarCategoriesItem);
    }

    private void getAzkarDetails(AzkarCategoriesItem azkarCategoriesItem) {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                AzkarDetailsResponse azkarDetailsResponse = (AzkarDetailsResponse) response;
                zekrItems = azkarDetailsResponse.getData().getData();
                numberOfZekr=azkarDetailsResponse.getData().getData().size();
                getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.AZKAR_DETAILS + azkarCategoriesItem.getId(), null, AzkarDetailsResponse.class);
    }

    public ZekrItem getZekrItem() {
        if(zekrItems==null||zekrItems.size()==0)return null;
        return zekrItems.get(currentZekr%zekrItems.size());
    }

    public void onZoomInFontClick() {
        getClicksMutableLiveData().setValue(Codes.ZOOM_IN);
    }

    public void onZoomOutFontClick() {
        getClicksMutableLiveData().setValue(Codes.ZOOM_OUT);
    }

    public String currentZekr(){

        return ((currentZekr%numberOfZekr)+1)+"/"+numberOfZekr;
    }

    public String getCount() {
        return count+"";
    }

    public void onPlayAudioClick() {
        getClicksMutableLiveData().setValue(Codes.PLAY_AUDIO);
    }

    public void onAddToFavouriteClick() {

        getClicksMutableLiveData().setValue(Codes.ADD_TO_FAVOURITE);
        FavouriteViewModel.addToFavourite(new FavouriteRequest( getZekrItem().getId(),"App\\Models\\Azkar"));
    }

    public void onTasbehClick(){
        count++;
        if(count==zekrItems.get(currentZekr%numberOfZekr).getTimes()+1){
            count=0;
            currentZekr= (currentZekr%numberOfZekr)+1;
        }
        notifyChange();
    }

}
