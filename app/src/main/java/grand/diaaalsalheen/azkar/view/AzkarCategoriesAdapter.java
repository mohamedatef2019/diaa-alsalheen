
package grand.diaaalsalheen.azkar.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.azkar.response.AzkarCategoriesItem;
import grand.diaaalsalheen.azkar.viewmodel.AzkarCategoriesItemViewModel;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.databinding.ItemAzkarBinding;
import grand.diaaalsalheen.databinding.ItemMongatBinding;


public class AzkarCategoriesAdapter extends RecyclerView.Adapter<AzkarCategoriesAdapter.CompaniesViewHolder> {

    private List<AzkarCategoriesItem> azkarCategoriesItems;
    private MutableLiveData<AzkarCategoriesItem> itemsOperationsLiveListener;


    public AzkarCategoriesAdapter() {
        this.azkarCategoriesItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_azkar,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {
        AzkarCategoriesItem monagatCategoryItem = azkarCategoriesItems.get(position);
        AzkarCategoriesItemViewModel companyItemViewModel = new AzkarCategoriesItemViewModel(monagatCategoryItem);
        companyItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
                itemsOperationsLiveListener.setValue(monagatCategoryItem);
        });
        holder.setViewModel(companyItemViewModel);
    }



    public MutableLiveData<AzkarCategoriesItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.azkarCategoriesItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<AzkarCategoriesItem> data) {

            this.azkarCategoriesItems.clear();

        assert data != null;
        this.azkarCategoriesItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemAzkarBinding itemAzkarBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemAzkarBinding == null) {
                itemAzkarBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemAzkarBinding != null) {
                itemAzkarBinding.unbind();
            }
        }

        void setViewModel(AzkarCategoriesItemViewModel azkarCategoriesItemViewModel) {
            if (itemAzkarBinding != null) {
                itemAzkarBinding.setItemAzkarCateory(azkarCategoriesItemViewModel);
            }
        }


    }



}
