package grand.diaaalsalheen.azkar.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.azkar.viewmodel.AzkarCategoriesViewModel;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentAzkarBinding;
import grand.diaaalsalheen.databinding.FragmentMonagasBinding;


public class AzkarCategoriesFragment extends BaseFragment {
    private View rootView;
    private AzkarCategoriesViewModel azkarCategoriesViewModel;
    private FragmentAzkarBinding fragmentAzkarBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAzkarBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_azkar, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();

    }

    private void binding() {
        rootView = fragmentAzkarBinding.getRoot();
        azkarCategoriesViewModel = new AzkarCategoriesViewModel();
        fragmentAzkarBinding.setAzkarViewModel(azkarCategoriesViewModel);

    }


    private void liveDataListeners() {
        azkarCategoriesViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });

        azkarCategoriesViewModel.getAzkarCategoriesAdapter().getItemsOperationsLiveListener().observe(this, azkarCategoriesItem -> {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Params.BUNDLE_ZEKR_ITEM, azkarCategoriesItem);
            MovementManager.startActivity(getActivity(), Codes.ZEKR_DETAILS, bundle);

        });
    }


}