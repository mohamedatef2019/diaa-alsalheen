package grand.diaaalsalheen.azkar.view;

import android.os.Bundle;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.huhx0015.hxaudio.audio.HXMusic;

import org.sufficientlysecure.htmltextview.HtmlFormatter;
import org.sufficientlysecure.htmltextview.HtmlFormatterBuilder;

import cn.pedant.SweetAlert.SweetAlertDialog;
import grand.diaaalsalheen.MediaPlayerUtils;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.SweetAlertDialogss;
import grand.diaaalsalheen.azkar.response.AzkarCategoriesItem;
import grand.diaaalsalheen.azkar.viewmodel.AzkarCategoriesViewModel;
import grand.diaaalsalheen.azkar.viewmodel.AzkarDetailsViewModel;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentAzkarBinding;
import grand.diaaalsalheen.databinding.FragmentAzkarDetailsBinding;


public class AzkarDetailsFragment extends BaseFragment {
    private View rootView;
    private AzkarDetailsViewModel azkarDetailsViewModel;
    private FragmentAzkarDetailsBinding fragmentAzkarDetailsBinding;
    int textSize = 0;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAzkarDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_azkar_details, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();

    }

    private void binding() {
        rootView = fragmentAzkarDetailsBinding.getRoot();

        assert getArguments() != null;
        azkarDetailsViewModel = new AzkarDetailsViewModel((AzkarCategoriesItem) getArguments().getSerializable(Params.BUNDLE_ZEKR_ITEM));
        fragmentAzkarDetailsBinding.setAzkarDetailsViewModel(azkarDetailsViewModel);

    }


    @Override
    public void onDestroyView() {
        HXMusic.stop();
        super.onDestroyView();
    }

   private boolean isPlayedBefore=false;


    private void liveDataListeners() {
        azkarDetailsViewModel.getClicksMutableLiveData().observe(this, result -> {

            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.PLAY_AUDIO) {

                MediaPlayerUtils.playAudio(azkarDetailsViewModel.getZekrItem().getVoice(), azkarDetailsViewModel.getZekrItem().getText(), getActivity(),fragmentAzkarDetailsBinding.ivPlay);


            } else if (result == Codes.ADD_TO_FAVOURITE) {
                if(UserPreferenceHelper.isLogined()) {
                    SweetAlertDialogss.showFavDialog(getActivity());
                  }else {
                SweetAlertDialogss.loginToContinue(getActivity());
            }
            } else if (result == Codes.ZOOM_IN) {
                textSize+=5;
                fragmentAzkarDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            } else if (result == Codes.ZOOM_OUT) {
                textSize-=5;
                fragmentAzkarDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            }else if (result == Codes.DATA_RECIVED) {
                Spanned formattedHtml = HtmlFormatter.formatHtml(new HtmlFormatterBuilder().setHtml(azkarDetailsViewModel.getZekrItem().getText()));
                String str=azkarDetailsViewModel.getZekrItem().getText();
                fragmentAzkarDetailsBinding.tvHtmlViewer.loadDataWithBaseURL(null, str, "text/html", "utf-8", null);
                textSize =  270;
                fragmentAzkarDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            }


        });

    }


}