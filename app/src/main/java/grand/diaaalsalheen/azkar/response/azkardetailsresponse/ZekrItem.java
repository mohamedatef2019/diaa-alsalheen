package grand.diaaalsalheen.azkar.response.azkardetailsresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.utils.ResourcesManager;


public class ZekrItem {

    @SerializedName("voice")
    private String voice;

    @SerializedName("times")
    private int times;

    @Expose
    private String timesString;

    @SerializedName("id")
    private int id;

    @SerializedName("text")
    private String text;

    @SerializedName("azkar_category_id")
    private int azkarCategoryId;

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public String getVoice() {
        return voice;
    }

    public void setTimes(int times) {
        this.times = times;
    }

    public int getTimes() {
        return times;
    }

    public void setTimesString(String timesString) {
        this.timesString = timesString;
    }

    public String getTimesString() {
        return times+" "+ ResourcesManager.getString(R.string.label_times);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setAzkarCategoryId(int azkarCategoryId) {
        this.azkarCategoryId = azkarCategoryId;
    }

    public int getAzkarCategoryId() {
        return azkarCategoryId;
    }

    @Override
    public String toString() {
        return
                "DataItem{" +
                        "voice = '" + voice + '\'' +
                        ",times = '" + times + '\'' +
                        ",id = '" + id + '\'' +
                        ",text = '" + text + '\'' +
                        ",azkar_category_id = '" + azkarCategoryId + '\'' +
                        "}";
    }
}