package grand.diaaalsalheen.azkar.response;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class AzkarCategoriesItem implements Serializable {

	@SerializedName("name")
	private String name;

    @SerializedName("img")
    private String img;

	@SerializedName("id")
	private int id;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}