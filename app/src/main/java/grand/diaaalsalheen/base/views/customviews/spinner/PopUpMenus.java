package grand.diaaalsalheen.base.views.customviews.spinner;

import android.content.Context;
import android.view.View;
import android.widget.PopupMenu;

import java.util.ArrayList;


public class PopUpMenus {

    public static PopupMenu showPopUp(Context context, View view, ArrayList<PopUpItem> types) {
        PopupMenu typesPopUps;
        typesPopUps = new PopupMenu(context, view);
        for (int i = 0; i < types.size(); i++) {
            typesPopUps.getMenu().add(i, i, i, types.get(i).name);
        }
        typesPopUps.show();

        return typesPopUps;

    }




}
