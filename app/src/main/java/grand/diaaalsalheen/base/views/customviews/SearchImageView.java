package grand.diaaalsalheen.base.views.customviews;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;


public class SearchImageView extends AppCompatImageView
{


    public SearchImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setRotation(context);
        try{
            setOnClickListener(view -> MovementManager.startActivity(getContext(), Codes.SEARCH));
        }catch (Exception e){
            e.getStackTrace();
        }
    }

    public void setRotation(Context context){
        if(UserPreferenceHelper.getCurrentLanguage(context).equals("ar")){
           // setRotation(180);
        }


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }


}