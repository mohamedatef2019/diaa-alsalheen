package grand.diaaalsalheen.base.views.activities;



import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.CancellationToken;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnTokenCanceledListener;
import com.google.android.gms.tasks.Task;
import java.util.Locale;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.UserLocation;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.viewmodel.SplashScreenViewModel;
import grand.diaaalsalheen.databinding.ActivitySplashScreenBinding;
import grand.diaaalsalheen.base.views.navigationdrawer.MenuViewModel;
import grand.diaaalsalheen.fcmmanager.MyFirebaseMessagingService;


public class SplashScreenActivity extends ParentActivity {
    ActivitySplashScreenBinding activitySplashScreenBinding;
    SplashScreenViewModel splashScreenViewModel;
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




            try {
                startService(new Intent(this, MyFirebaseMessagingService.class));
            }catch (Exception e){
                e.getStackTrace();
            }


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
        }else {
            startApp();
        }


        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);



            fusedLocationClient.getCurrentLocation( LocationRequest.PRIORITY_HIGH_ACCURACY, new CancellationToken() {
                @Override
                public boolean isCancellationRequested() {
                    return false;
                }

                @NonNull
                @Override
                public CancellationToken onCanceledRequested(@NonNull OnTokenCanceledListener onTokenCanceledListener) {
                    return null;
                }
            })
                .addOnSuccessListener(this, location -> {
                    Log.e("PrayerOptions",lat+"  "+lang);

                    if (location != null) {
                        SplashScreenActivity.this.lat= location.getLatitude();
                        SplashScreenActivity.this.lang= location.getLongitude();
                        Log.e("PrayerOptions",lat+"  "+lang);
                        UserPreferenceHelper.saveUserLocation(new UserLocation(lat,lang));
                    }
                });



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        startApp();
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void startApp(){




        SplashScreenActivity.changeLanguage(this, UserPreferenceHelper.getCurrentLanguage(this));
        MenuViewModel.changeLanguage(this,UserPreferenceHelper.getCurrentLanguage(this));

        activitySplashScreenBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen);
        if(UserPreferenceHelper.isFirstTime()){
            for(int i=0; i<10; i++){
                UserPreferenceHelper.setPrayerAvailable(i,true);
            }
            MovementManager.startActivity(SplashScreenActivity.this, Codes.CHANGE_LANGUAGE);

            //UserPreferenceHelper.setCurrentMazhab(userRequest.get);


        }else if(UserPreferenceHelper.getUserLocation()==null){
            MovementManager.startActivity(SplashScreenActivity.this, Codes.LOCATION_SCREEN);
        }else if(UserPreferenceHelper.getUserDetails()!=null){
            liveDataListeners();

        }else {
            MovementManager.startMainActivity(SplashScreenActivity.this, Codes.HOME_SCREEN);
        }
        enableLocationDialog();
    }

    private double lat, lang;

    private LocationRequest getLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }



    private void enableLocationDialog() {


        LocationRequest locationRequest = getLocationRequest();
        LocationSettingsRequest settingsRequest = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest).build();
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client
                .checkLocationSettings(settingsRequest);

        task.addOnFailureListener((AppCompatActivity) this, e -> {
            int statusCode = ((ApiException) e).getStatusCode();
            if (statusCode == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                try {
                    ResolvableApiException resolvable =
                            (ResolvableApiException) e;
                    resolvable.startResolutionForResult(
                           this,
                                    1019);
                } catch (IntentSender.SendIntentException ignored) {
                }
            }
        });





    }

    private void liveDataListeners() {
        splashScreenViewModel = new SplashScreenViewModel();
        splashScreenViewModel.getClicksMutableLiveData().observe(this, result -> {

                if (result == Codes.LOGIN_SCREEN || result == Codes.SEND_CODE_SCREEN) {
                    finish();
                    MovementManager.startActivity(SplashScreenActivity.this, result);
                } else if (result == Codes.HOME_SCREEN) {
                    finish();
                    MovementManager.startMainActivity(SplashScreenActivity.this, result);
                } else {
                    Toast.makeText(this, "" + splashScreenViewModel.getReturnedMessage(), Toast.LENGTH_SHORT).show();
                }
        });
    }


    public static void changeLanguage(Context context, String languageToLoad){
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
        UserPreferenceHelper.setLanguage(context, languageToLoad);


        locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
        UserPreferenceHelper.setLanguage(context, languageToLoad);
    }

}
