package grand.diaaalsalheen.base.views.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

import grand.diaaalsalheen.base.utils.Validate;


public class CustomButton extends AppCompatButton {


    public CustomButton(Context context) {
        super(context);
        init();
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);


        init();
    }


    private void init() {


        Typeface font = null;
        font = Typeface.createFromAsset(getContext().getAssets(), "maddina.otf");
        setTypeface(font);
    }


}
