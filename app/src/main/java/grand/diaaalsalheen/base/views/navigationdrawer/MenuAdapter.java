
package grand.diaaalsalheen.base.views.navigationdrawer;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.databinding.ItemNavMenuBinding;


public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.CategoriesViewHolder> {

    private List<NavigationDrawerItem> menuItems;
    private MutableLiveData<NavigationDrawerItem> itemsOperationsLiveListener;
    private MutableLiveData<Void> closeDrawer;

    public MenuAdapter() {
        this.menuItems = new ArrayList<>();
        itemsOperationsLiveListener = new MutableLiveData<>();
        updateData();
    }


    public void closeDrawer() {
        closeDrawer.setValue(null);
    }

    public MutableLiveData<Void> getCloseDrawer() {
        if (closeDrawer == null) {
            closeDrawer = new MutableLiveData<>();
        }
        return closeDrawer;
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nav_menu,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {

        NavigationDrawerItem menuItem = menuItems.get(position);
        MenuItemViewModel menuItemViewModel = new MenuItemViewModel(menuItem);
        menuItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
            itemsOperationsLiveListener.setValue(menuItem);
        });
        holder.setViewModel(menuItemViewModel);
    }

    @Override
    public int getItemCount() {

        return this.menuItems.size();
    }

    public MutableLiveData<NavigationDrawerItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener;
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void getItems() {
        menuItems = new ArrayList<>();
        menuItems.add(new NavigationDrawerItem(1,  ResourcesManager.getString(R.string.label_home), R.drawable.ic_home));
       // menuItems.add(new NavigationDrawerItem(10, ResourcesManager.getString(R.string.takebat), R.drawable.ic_prayers));
        menuItems.add(new NavigationDrawerItem(2,  ResourcesManager.getString(R.string.favourite), R.drawable.ic_fav_white));
        menuItems.add(new NavigationDrawerItem(3,  ResourcesManager.getString(R.string.contact_us), R.drawable.ic_contact_us));
        menuItems.add(new NavigationDrawerItem(4,  ResourcesManager.getString(R.string.social_media), R.drawable.ic_social_media));
        menuItems.add(new NavigationDrawerItem(5,  ResourcesManager.getString(R.string.share), R.drawable.ic_share));
        menuItems.add(new NavigationDrawerItem(6,  ResourcesManager.getString(R.string.label_votes), R.drawable.ic_rate));
        menuItems.add(new NavigationDrawerItem(7,  ResourcesManager.getString(R.string.terms_conditions), R.drawable.ic_terms_conditions));
        menuItems.add(new NavigationDrawerItem(8,  ResourcesManager.getString(R.string.language), R.drawable.ic_language));
        menuItems.add(new NavigationDrawerItem(14, ResourcesManager.getString(R.string.label_change_time), R.drawable.ic_change_location));

        //  menuItems.add(new NavigationDrawerItem(12,  ResourcesManager.getString(R.string.label_change_size), R.drawable.ic_font_size));
        menuItems.add(new NavigationDrawerItem(11, ResourcesManager.getString(R.string.label_change_azan), R.drawable.ic_azan));
        menuItems.add(new NavigationDrawerItem(13, ResourcesManager.getString(R.string.label_select_location), R.drawable.ic_change_location));

        if(UserPreferenceHelper.isLogined()) {
            menuItems.add(new NavigationDrawerItem(9, ResourcesManager.getString(R.string.label_log_out), R.drawable.ic_login));
        }else {
            menuItems.add(new NavigationDrawerItem(9, ResourcesManager.getString(R.string.label_login), R.drawable.ic_login));
        }
    }

    public void updateData() {
        getItems();
        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemNavMenuBinding itemMenuBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(MenuItemViewModel menuItemViewModel) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setMenuItemViewModel(menuItemViewModel);
            }
        }


    }


}
