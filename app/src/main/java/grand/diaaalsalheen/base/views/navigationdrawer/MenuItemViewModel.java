
package grand.diaaalsalheen.base.views.navigationdrawer;


import android.util.Log;
import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class MenuItemViewModel extends BaseItemViewModel {
    private NavigationDrawerItem menuItem;

    public MenuItemViewModel(NavigationDrawerItem menuItem ) {
        this.menuItem = menuItem;

    }




    @Bindable
    public NavigationDrawerItem getMenuItem(){
        Log.e("MenuItms",menuItem.getTitle());
        return menuItem;
    }

    @BindingAdapter({"src"})
    public static void setSrc(ImageView view, int image){
        view.setImageResource(image);

    }
    public void itemMenuClick(){
        getItemsOperationsLiveListener().setValue(null);
    }




}
