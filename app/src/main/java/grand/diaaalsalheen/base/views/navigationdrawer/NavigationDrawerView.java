package grand.diaaalsalheen.base.views.navigationdrawer;



import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import java.util.Objects;
import grand.diaaalsalheen.BuildConfig;
import grand.diaaalsalheen.SelectLocationFragment;
import grand.diaaalsalheen.SweetAlertDialogss;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.views.activities.MainActivity;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.views.activities.SplashScreenActivity;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.MyApplication;
import grand.diaaalsalheen.databinding.LayoutNavigationDrawerBinding;
import grand.diaaalsalheen.favourite.view.FavouritesFragment;
import grand.diaaalsalheen.home.view.HomeFragment;
import grand.diaaalsalheen.login.response.UserItem;
import grand.diaaalsalheen.profile.view.UpdateProfileFragment;
import grand.diaaalsalheen.sendmessage.view.SendMessageFragment;
import grand.diaaalsalheen.socialmedia.view.SocialMediaFragment;
import grand.diaaalsalheen.takeeb.view.TakeebCategoriesFragment;
import grand.diaaalsalheen.termsandconditions.view.TermsFragment;
import grand.diaaalsalheen.votes.view.VotesFragment;


public class NavigationDrawerView extends RelativeLayout {
    public LayoutNavigationDrawerBinding layoutNavigationDrawerBinding;


    public NavigationDrawerView(Context context) {
        super(context);
        init();
    }

    public NavigationDrawerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public NavigationDrawerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        init();
    }

    private void init() {

        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutNavigationDrawerBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_navigation_drawer, this, true);
        setEvents();

        setUserDetails();
    }


    private void setEvents() {
        MenuAdapter menuAdapter = new MenuAdapter();

     /*   layoutNavigationDrawerBinding.ivAddPostBaseBack.setOnClickListener(v -> {
            if (layoutNavigationDrawerBinding.dlMainNavigationMenu.isDrawerOpen(layoutNavigationDrawerBinding.dlMainNavigationMenu)) {
                layoutNavigationDrawerBinding.dlMainNavigationMenu.closeDrawers();
            } else {
                layoutNavigationDrawerBinding.dlMainNavigationMenu.openDrawer(layoutNavigationDrawerBinding.dlMainNavigationMenu);
            }
        });*/
        setupGrandStyle();



        menuAdapter.getItemsOperationsLiveListener().observeForever(menuItem -> {


            ((MainActivity) getContext()).isOrder = false;

            assert menuItem != null;

            if(menuItem.getId()!=8&&menuItem.getId()!=9)
            layoutNavigationDrawerBinding.dlMainNavigationMenu.closeDrawers();


            if (menuItem.getId() == 1) {
                MovementManager.addFragment(getContext(), new HomeFragment(), "HomeFragment");
            } else if (menuItem.getId() == 2) {
                if (UserPreferenceHelper.isLogined()) {
                    MovementManager.addFragment(getContext(), new FavouritesFragment(), "FavouritesFragment");
                } else {
                    SweetAlertDialogss.loginToContinue(getContext());
                }


            } else if (menuItem.getId() == 3) {
                MovementManager.addFragment(getContext(), new SendMessageFragment(), "SendMessageFragment");
            } else if (menuItem.getId() == 4) {
                MovementManager.addFragment(getContext(), new SocialMediaFragment(), "SocialMediaFragment");
            } else if (menuItem.getId() == 5) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                sendIntent.setType("text/plain");
                getContext().startActivity(sendIntent);
            } else if (menuItem.getId() == 6) {

                if (UserPreferenceHelper.isLogined()) {
                    MovementManager.addFragment(getContext(), new VotesFragment(), "VotesFragment");
                } else {
                    SweetAlertDialogss.loginToContinue(getContext());
                }

                //MenuViewModel.rateApp(getContext());
            } else if (menuItem.getId() == 7) {
                MovementManager.addFragment(getContext(), new TermsFragment(), "TermsFragment");
            } else if (menuItem.getId() == 8) {
                changeLanguageDialog();
            }else if(menuItem.getId()==12){
               // changeFontSizeDialog();
            }
            else if (menuItem.getId() == 11) {
                changeAzanDialog();
            } else if (menuItem.getId() == 9) {

                if(UserPreferenceHelper.isLogined()) {
                    logOutDialog();
                }else {
                    MovementManager.startActivity(getContext(), Codes.LOGIN_SCREEN);
                }

            }else if (menuItem.getId() == 10) {
                MovementManager.addFragment(getContext(), new TakeebCategoriesFragment(), "TakeebCategoriesFragment");
            }else if (menuItem.getId() == 13) {
                MovementManager.addFragment(getContext(), new SelectLocationFragment(), "SelectLocationFragment");
            }else if(menuItem.getId()==14){
                changeTimeDialog();
            }

        });

        layoutNavigationDrawerBinding.rvNavigationDrawerList.setAdapter(menuAdapter);


        layoutNavigationDrawerBinding.rlNavigationDrawerHeader.setOnClickListener(view -> {

            layoutNavigationDrawerBinding.dlMainNavigationMenu.closeDrawers();

            if(UserPreferenceHelper.isLogined()) {
                MovementManager.addFragment(getContext(), new UpdateProfileFragment(), "UpdateProfileFragment");
            }else {
                SweetAlertDialogss.loginToContinue(getContext());
            }

        });


    }


    public void setUserDetails() {
        UserItem userItem = UserPreferenceHelper.getUserDetails();
      if(UserPreferenceHelper.isLogined()) {
          assert userItem != null;
          layoutNavigationDrawerBinding.tvNavigationDrawerUserName.setText(userItem.getUsersName());
          ConnectionHelper.loadImage(layoutNavigationDrawerBinding.ivNavigationDrawerUserImage, userItem.getUsersImg());
      }else{
          layoutNavigationDrawerBinding.tvNavigationDrawerUserName.setText("");
      }
    }


    public void setupGrandStyle() {
        layoutNavigationDrawerBinding.bygrand.setOnClickListener(v -> {
        });
    }


    public void logOutDialog() {
        Dialog loadingBar = new Dialog(getContext(), android.R.style.Theme_NoTitleBar);
        loadingBar.setCancelable(true);
        loadingBar.setCanceledOnTouchOutside(true);
        loadingBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View loadingView = inflater.inflate(R.layout.dialog_log_out, null);
        loadingView.findViewById(R.id.btn_log_out_yes).setOnClickListener(view -> {
            UserPreferenceHelper.clearUserDetails();
            ((AppCompatActivity) getContext()).finish();
            loadingBar.cancel();
            loadingBar.dismiss();
        });

        loadingView.findViewById(R.id.btn_log_out_no).setOnClickListener(view -> {
            loadingBar.cancel();
            loadingBar.dismiss();
        });


        loadingBar.setContentView(loadingView);

        Objects.requireNonNull(loadingBar.getWindow()).setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0effffff")));


        loadingBar.show();
    }

    public void changeAzanDialog() {
        Dialog loadingBar = new Dialog(getContext(), android.R.style.Theme_NoTitleBar);
        loadingBar.setCancelable(true);
        loadingBar.setCanceledOnTouchOutside(true);
        loadingBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View loadingView = inflater.inflate(R.layout.dialog_change_language, null);
        TextView arabicTextView = loadingView.findViewById(R.id.tv_change_language_arabic);
        TextView changeLanguage = loadingView.findViewById(R.id.tv_change_language);

        changeLanguage.setText(ResourcesManager.getString(R.string.label_change_azan));
        arabicTextView.setText(ResourcesManager.getString(R.string.label_full_azan));
        arabicTextView.setOnClickListener(view -> {
            UserPreferenceHelper.setAzan(view.getContext(),"big");
            loadingBar.cancel();
            loadingBar.dismiss();
        });

        TextView englishTextView = loadingView.findViewById(R.id.tv_change_language_english);
        englishTextView.setText(ResourcesManager.getString(R.string.label_small_azan));
        englishTextView.setOnClickListener(view -> {

            UserPreferenceHelper.setAzan(view.getContext(),"small");
            loadingBar.cancel();
            loadingBar.dismiss();
        });

        if(UserPreferenceHelper.getCurrentAzan(getContext()).equals("big")) {
            arabicTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checked_radio, 0);
        }else {
            englishTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checked_radio, 0);
        }

        loadingBar.setContentView(loadingView);

        Objects.requireNonNull(loadingBar.getWindow()).setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0effffff")));


        loadingBar.show();
    }


    public void changeLanguageDialog() {
        Dialog loadingBar = new Dialog(getContext(), android.R.style.Theme_NoTitleBar);
        loadingBar.setCancelable(true);
        loadingBar.setCanceledOnTouchOutside(true);
        loadingBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View loadingView = inflater.inflate(R.layout.dialog_change_language, null);
        TextView arabicTextView = loadingView.findViewById(R.id.tv_change_language_arabic);

        arabicTextView.setOnClickListener(view -> {
            ((AppCompatActivity) getContext()).finishAffinity();
            MenuViewModel.changeLanguage(getContext(), "ar");
            MenuViewModel.changeLanguage(MyApplication.getInstance().getApplicationContext(), "ar");
            MovementManager.startActivity(getContext(), SplashScreenActivity.class);
        });

        TextView englishTextView = loadingView.findViewById(R.id.tv_change_language_english);

        englishTextView.setOnClickListener(view -> {
            ((AppCompatActivity) getContext()).finishAffinity();
            MenuViewModel.changeLanguage(getContext(), "en");
            MenuViewModel.changeLanguage(MyApplication.getInstance().getApplicationContext(), "en");
            MovementManager.startActivity(getContext(), SplashScreenActivity.class);
            loadingBar.cancel();
            loadingBar.dismiss();
        });

        if(UserPreferenceHelper.getCurrentLanguage(getContext()).equals("ar")) {
            arabicTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checked_radio, 0);
        }else {
            englishTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checked_radio, 0);
        }

        loadingBar.setContentView(loadingView);

        Objects.requireNonNull(loadingBar.getWindow()).setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0effffff")));


        loadingBar.show();
    }




    public void changeTimeDialog() {
        Dialog loadingBar = new Dialog(getContext(), android.R.style.Theme_NoTitleBar);
        loadingBar.setCancelable(true);
        loadingBar.setCanceledOnTouchOutside(true);
        loadingBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View changeTimeView = inflater.inflate(R.layout.dialog_change_time, null);


        TextView normalTextView = changeTimeView.findViewById(R.id.tv_change_time_normal);

        TextView oneDayAfterTextView = changeTimeView.findViewById(R.id.tv_change_one_day_after);

        TextView oneDayBeforeTextView = changeTimeView.findViewById(R.id.tv_change_one_day_before);

        TextView twoDayBeforeTextView = changeTimeView.findViewById(R.id.tv_change_time_two_day_before);

        TextView twoDayAfterTextView = changeTimeView.findViewById(R.id.tv_change_two_day_after);


        normalTextView.setOnClickListener(view -> {
            changeTime("0",loadingBar);
        });


        oneDayAfterTextView.setOnClickListener(view -> {
            changeTime("1",loadingBar);
        });

        oneDayBeforeTextView.setOnClickListener(view -> {
            changeTime("-1",loadingBar);
        });


        twoDayBeforeTextView.setOnClickListener(view -> {
            changeTime("-2",loadingBar);
        });

        twoDayAfterTextView.setOnClickListener(view -> {

            changeTime("2",loadingBar);

        });


        String currentTime = UserPreferenceHelper.getCurrentTime(getContext());

        if(currentTime.equals("0")) {
            normalTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checked_radio, 0);
        }else if(currentTime.equals("1")) {
            oneDayAfterTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checked_radio, 0);
        }else if(currentTime.equals("-1")) {
            oneDayBeforeTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checked_radio, 0);
        }else if(currentTime.equals("2")) {
            twoDayAfterTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checked_radio, 0);
        }else if(currentTime.equals("-2")) {
            twoDayBeforeTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checked_radio, 0);
        }

        loadingBar.setContentView(changeTimeView);

        Objects.requireNonNull(loadingBar.getWindow()).setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0effffff")));


        loadingBar.show();
    }



    private void changeTime(String time,Dialog dialog){
        UserPreferenceHelper.setCurrentTime(getContext(),time);
          dialog.cancel();
          dialog.dismiss();
        layoutNavigationDrawerBinding.dlMainNavigationMenu.closeDrawers();
        MovementManager.addFragment(getContext(), new HomeFragment(), "HomeFragment");
    }


    public void changeFontSizeDialog() {
        Dialog loadingBar = new Dialog(getContext(), android.R.style.Theme_NoTitleBar);
        loadingBar.setCancelable(true);
        loadingBar.setCanceledOnTouchOutside(true);
        loadingBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View loadingView = inflater.inflate(R.layout.dialog_change_font_size, null);
        SeekBar seekBar = loadingView.findViewById(R.id.seekBar);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                ((MainActivity)getContext()).adjustFontScale(((float)seekBar.getProgress()/10.0f));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                ((MainActivity)getContext()).recreate();
            }
        });



        /*if(UserPreferenceHelper.getCurrentLanguage(getContext()).equals("ar")) {
            arabicTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checked_radio, 0);
        }else {
            englishTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checked_radio, 0);
        }*/

        loadingBar.setContentView(loadingView);

        Objects.requireNonNull(loadingBar.getWindow()).setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0effffff")));


        loadingBar.show();
    }


}
