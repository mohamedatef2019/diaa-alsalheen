package grand.diaaalsalheen.base.views.customviews;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import cn.pedant.SweetAlert.SweetAlertDialog;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;


public class FavouriteImageView extends AppCompatImageView {


    public FavouriteImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }



    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

}