package grand.diaaalsalheen.base.views.customviews;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.google.android.material.textfield.TextInputEditText;

import grand.diaaalsalheen.base.utils.Validate;


public class EmailEditText extends AppCompatEditText {

    boolean valid=false;
    public EmailEditText(Context context) {
        super(context);
        init();
    }

    public EmailEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EmailEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    private void init(){

        setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence c, int start, int before, int count) {

                if(getInputType()==InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS) {

                    if (Validate.isEmpty(c.toString())) {

                        setError("Empty");

                    } else if (!Validate.isMail(c.toString())) {

                        setError("Wrong email");

                    } else {
                        valid=true;
                        setError(null);

                    }
                }

            }

            public void beforeTextChanged(CharSequence c, int start, int count, int after) {

            }

            public void afterTextChanged(Editable c) {

            }
        });

    }


}
