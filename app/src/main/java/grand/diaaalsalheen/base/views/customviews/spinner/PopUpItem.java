package grand.diaaalsalheen.base.views.customviews.spinner;

public class PopUpItem {
    String id,name;

    public PopUpItem(String id,String name){
        this.id=id;
        this.name=name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
}
