package grand.diaaalsalheen.base.views.activities;

import android.Manifest;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import java.util.Calendar;
import java.util.Locale;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.utils.BaseContextWrapper;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.views.navigationdrawer.MenuViewModel;
import grand.diaaalsalheen.databinding.ActivityMainBinding;
import grand.diaaalsalheen.home.view.HomeFragment;
import grand.diaaalsalheen.base.views.navigationdrawer.NavigationDrawerView;
import grand.diaaalsalheen.termsandconditions.view.TermsFragment;


public class MainActivity extends ParentActivity {

    public ActivityMainBinding activityMainBinding;
    NavigationDrawerView navigationDrawerView;

    public boolean isOrder = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                   Manifest.permission.READ_EXTERNAL_STORAGE,
                   Manifest.permission.ACCESS_FINE_LOCATION,
                   Manifest.permission.ACCESS_COARSE_LOCATION,
                   Manifest.permission.CAMERA,
                   Manifest.permission.READ_PHONE_STATE,
                   Manifest.permission.WAKE_LOCK},12412);
        }

        SplashScreenActivity.changeLanguage(this, UserPreferenceHelper.getCurrentLanguage(this));
        MenuViewModel.changeLanguage(this,UserPreferenceHelper.getCurrentLanguage(this));

        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1001);

        navigationDrawerView = new NavigationDrawerView(this);
        activityMainBinding.llBaseContainer.addView(navigationDrawerView);

        getHijriDate();


        if (getIntent().hasExtra(Params.INTENT_PAGE)) {
            addFragment(getIntent().getIntExtra(Params.INTENT_PAGE, 0));
        } else {
            MovementManager.addFragment(this, new HomeFragment(), "HomeFragment");
        }


    }

    public  void adjustFontScale(float scale) {
        Log.e("Tago",scale+"");
        getResources().getConfiguration().fontScale = scale;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = getResources().getConfiguration().fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(getResources().getConfiguration(), metrics);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getHijriDate(){
/*        UmmalquraCalendar cal = new UmmalquraCalendar();
        String date = cal.get(Calendar.DAY_OF_MONTH)+" "+cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH) +" "+cal.get(Calendar.YEAR)+" "+getResources().getString(R.string.label_hijri);
        navigationDrawerView.layoutNavigationDrawerBinding.tvMainTitle.setText(date);*/

    }

    boolean doubleBackToExitPressedOnce = true;

    @Override
    public void onBackPressed() {
        if (!doubleBackToExitPressedOnce) {
            finish();
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
                this.doubleBackToExitPressedOnce = false;
                Toast.makeText(this, getResources().getString(R.string.msg_double_click_to_close), Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(() -> doubleBackToExitPressedOnce = true, 2000);
                if (!(getSupportFragmentManager().findFragmentById(R.id.fl_home_container) instanceof HomeFragment)) {
                    MovementManager.popAllFragments(MainActivity.this);
                    MovementManager.addFragment(MainActivity.this, new HomeFragment(), "HomeFragment");
                    //navigationDrawerView.layoutNavigationDrawerBinding.tvMainTitle.setText(ResourcesManager.getString(R.string.label_home));
                    isOrder = false;
                }
            } else {
                super.onBackPressed();
            }

        }

    }


    private void addFragment(int page) {


        if(page == Codes.CHANGE_PASSWORD_SCREEN){
            MovementManager.addFragment(this, new TermsFragment(), "TermsFragment");

        }else {
            MovementManager.addFragment(this, new HomeFragment(), "HomeFragment");
        }

    }




    public NavigationDrawerView getNavigationDrawerView() {
        return navigationDrawerView;
    }



}
