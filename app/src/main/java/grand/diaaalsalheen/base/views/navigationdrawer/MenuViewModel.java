
package grand.diaaalsalheen.base.views.navigationdrawer;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;


import androidx.annotation.RequiresApi;

import java.util.Locale;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.views.activities.SplashScreenActivity;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.volleyutils.MyApplication;


public class MenuViewModel extends BaseViewModel {




    public MenuViewModel() {
        notifyChange();
    }





    public static void shareApp(Context context) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Here is the share content body";
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void rateApp(Context context) {
        Uri uri = Uri.parse("market://details?id=" +  context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    public static void provideService(Context context) {
        Uri uri = Uri.parse("market://details?id="+context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    public static void changeLanguage(Context context){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(context.getResources().getString(R.string.label_change_language))
                .setMessage(context.getResources().getString(R.string.msg_choose_change_language))
                .setPositiveButton(context.getResources().getString(R.string.label_arabic), (dialog, which) -> {
                    changeLanguage(context, "ar");
                    MovementManager.startActivity(context, SplashScreenActivity.class);
                })
                .setNegativeButton(context.getResources().getString(R.string.label_english), (dialog, which) -> {
                    changeLanguage(context, "en");
                    MovementManager.startActivity(context, SplashScreenActivity.class);
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static void changeLanguage(Context context, String languageToLoad){
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        MyApplication.getInstance().getApplicationContext().getResources().updateConfiguration(config,
                MyApplication.getInstance().getApplicationContext().getResources().getDisplayMetrics());
        UserPreferenceHelper.setLanguage(context, languageToLoad);
    }


}

