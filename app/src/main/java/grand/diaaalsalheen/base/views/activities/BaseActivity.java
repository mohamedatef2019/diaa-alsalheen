package grand.diaaalsalheen.base.views.activities;


import android.Manifest;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;

import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.github.nisrulz.sensey.Sensey;

import grand.diaaalsalheen.PrayerDetectorFragment;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.SelectLocationFragment;
import grand.diaaalsalheen.azkar.view.AzkarCategoriesFragment;
import grand.diaaalsalheen.azkar.view.AzkarDetailsFragment;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.views.navigationdrawer.MenuViewModel;
import grand.diaaalsalheen.base.volleyutils.MyApplication;
import grand.diaaalsalheen.beads.view.MainBeadsFragment;
import grand.diaaalsalheen.changelanguage.ChangeLanguageFragment;
import grand.diaaalsalheen.changepassword.view.ChangePasswordFragment;
import grand.diaaalsalheen.databinding.ActivityBaseBinding;
import grand.diaaalsalheen.doaa.view.DoaaCategoriesFragment;
import grand.diaaalsalheen.doaa.view.DoaaDetailsFragment;
import grand.diaaalsalheen.favourite.view.FavouritesFragment;
import grand.diaaalsalheen.forgotpassword.view.ForgotPasswordFragment;
import grand.diaaalsalheen.login.view.LoginFragment;
import grand.diaaalsalheen.mongat.view.MongatCategoriesFragment;
import grand.diaaalsalheen.mongat.view.MongatDetailsFragment;
import grand.diaaalsalheen.muslimcalender.view.MuslimCalenderFragment;
import grand.diaaalsalheen.prayertimes.view.PrayerTimesFragment;
import grand.diaaalsalheen.profile.view.UpdateProfileFragment;
import grand.diaaalsalheen.quran.view.MainQuranFragment;
import grand.diaaalsalheen.quran.view.MainQuranViewerFragment;
import grand.diaaalsalheen.quran.view.SewarFragment;
import grand.diaaalsalheen.register.view.RegisterFragment;
import grand.diaaalsalheen.search.view.SearchFragment;
import grand.diaaalsalheen.sendcode.view.SendCodeFragment;
import grand.diaaalsalheen.takeeb.view.TakeebCategoriesFragment;
import grand.diaaalsalheen.takeeb.view.TakeebDetailsFragment;
import grand.diaaalsalheen.tasks.view.MainTasksFragment;
import grand.diaaalsalheen.works.view.AmalCategoriesFragment;
import grand.diaaalsalheen.works.view.AmalSubCategoriesFragment;
import grand.diaaalsalheen.works.view.WorksDetailsFragment;
import grand.diaaalsalheen.zyarat.view.ZyaratDetailsFragment;
import grand.diaaalsalheen.zyarat.view.ZyaratMainFragment;

public class BaseActivity extends ParentActivity {
    public ActivityBaseBinding activityBaseBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Sensey.getInstance().init(this);


        SplashScreenActivity.changeLanguage(this, UserPreferenceHelper.getCurrentLanguage(this));
        MenuViewModel.changeLanguage(this,UserPreferenceHelper.getCurrentLanguage(this));

        activityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1001);

        if (getIntent().hasExtra(Params.INTENT_PAGE)) {
            addFragment(getIntent().getIntExtra(Params.INTENT_PAGE, 0));
        }


    }





    private void addFragment(int page) {

        Fragment fragment = null;

        if (page == Codes.REGISTER_SCREEN) {
            activityBaseBinding.rltMainHeader.setVisibility(View.GONE);
            activityBaseBinding.tvMainTitle.setText(ResourcesManager.getString(R.string.label_register));
            fragment = new RegisterFragment();
        } else if (page == Codes.MONAGAT_CATEGORIES) {
            activityBaseBinding.tvMainTitle.setText(ResourcesManager.getString(R.string.label_monagat));
            fragment = new MongatCategoriesFragment();
        } else if (page == Codes.MONGAT_DETAILS) {
            fragment = new MongatDetailsFragment();
        } else if (page == Codes.ADAIA_CATEGORIES) {
            fragment = new DoaaCategoriesFragment();
        } else if (page == Codes.AZKAR_CATEGORIES) {
            fragment = new AzkarCategoriesFragment();
        } else if (page == Codes.WORKS_CATEGORIES) {
            fragment = new AmalCategoriesFragment();
        } else if (page == Codes.TAQEEB_CATEGORIES) {
            fragment = new TakeebCategoriesFragment();
        } else if (page == Codes.QORAN) {
            fragment = new MainQuranFragment();
        } else if (page == Codes.DOAA) {
            fragment = new DoaaCategoriesFragment();
        } else if (page == Codes.DOAA_DETAILS) {
            fragment = new DoaaDetailsFragment();
        } else if (page == Codes.MAIN_BEADS) {
            fragment = new MainBeadsFragment();
        } else if (page == Codes.ZEKR_DETAILS) {
            fragment = new AzkarDetailsFragment();
        } else if (page == Codes.PRAYER_TIMES) {
            fragment = new PrayerTimesFragment();
        } else if (page == Codes.ZYARAT) {
            fragment = new ZyaratMainFragment();
        } else if (page == Codes.ZYARAT_DETAILS) {
            fragment = new ZyaratDetailsFragment();
        } else if (page == Codes.AMAL_CATEGORIES) {
            fragment = new AmalCategoriesFragment();
        }  else if (page == Codes.AMAL_SUB_CATEGORIES) {
            fragment = new AmalSubCategoriesFragment();
        }else if (page == Codes.AMAL_DETAILS) {
            fragment = new WorksDetailsFragment();
        }else if(page == Codes.MUSLIM_CALENDER){
            fragment = new MuslimCalenderFragment();
        }else if(page == Codes.TASKS){
            fragment = new MainTasksFragment();
        }else if(page == Codes.LOGIN_SCREEN){
            fragment = new LoginFragment();
        }else if(page == Codes.SEND_CODE_SCREEN){
            fragment = new SendCodeFragment();
        }else if(page == Codes.LOCATION_SCREEN){
            fragment = new SelectLocationFragment();
        }else if(page == Codes.QURAN_VIEWER_SCREEN){
            fragment = new MainQuranViewerFragment();
        }else if(page == Codes.PROFILE_PAGE){
            fragment = new UpdateProfileFragment();
        }else if(page == Codes.PRAYER_DETECTOR){
            fragment = new PrayerDetectorFragment();
        }else if(page == Codes.FAVOURITES){
            fragment = new FavouritesFragment();
        }else if(page == Codes.FORGOT_PASSWORD_SCREEN){
            fragment = new ForgotPasswordFragment();
        }else if(page == Codes.CHANGE_PASSWORD_SCREEN){
            fragment = new ChangePasswordFragment();
        } else if(page == Codes.TAQEEB_DETAILS){
            fragment = new TakeebDetailsFragment();
        }else if(page == Codes.SEARCH){
            fragment = new SearchFragment();
        }else if(page == Codes.CHANGE_LANGUAGE){
            activityBaseBinding.tvMainTitle.setText(ResourcesManager.getString(R.string.label_change_language));
            fragment = new ChangeLanguageFragment();
        }



        if (fragment != null) {
            fragment.setArguments(getIntent().getBundleExtra(Params.INTENT_BUNDLE));
        }

        MovementManager.addFragment(this, fragment, "");
    }


    @Override
    public void onBackPressed() {
        finish();
    }


}
