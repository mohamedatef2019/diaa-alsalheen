package grand.diaaalsalheen.base.constantsutils;

/**
 * Created by sameh on 4/2/18.
 */

public class WebServices {

    //Codes


    //Status
    public static final String SUCCESS = "success";
    public static final String NOT_ACTIVE = "not_active";
    public static final String FAILED="failed";

    //URLS
    public static String BASE_URL ="http://deyaa.net/api/";
    public static String QURAN_READER_URL="https://cdn.alquran.cloud/media/audio/";
    public static String QURAN_READER_AYAH=QURAN_READER_URL+"ayah/";
    public static String QURAN_READER_PAGE=QURAN_READER_URL+"page/";
    public static String LOGIN ="login";
    public static String SLIDER="slider";
    public static String REGISTER = "signup";
    public static String UPDATE_PROFILE= "updateprofile";
    public static String UPDATE_PASSWORD="changepassword";
    public static String RENEW_PASSWORD="changepassword";
    public static String CHECK_CODE = "verifycode";
    public static String ABOUT_US="about_us";
    public static String SOCIAL_MEDIA="socials";
    public static String MONAGAT_CATEGORIES="monagatcategories";
    public static String AZKAR_CATEGORIES="azkarcategories";
    public static String AMAL_CATEGORIES="aamalcategories";
    public static String AMAL_SUB_CATEGORIES="amaal-subcategories/";
    public static String MONAGAT_DETAILS="monagat/";
    public static String TAQEEB_DETAILS="taqeb/";
    public static String DOAA_DETAILS="doaadetails/";
    public static String AMAL_DETAILS="amal/";
    public static String DOAA_CATEGORIES="doaacategories/";
    public static String TAQEEB_CATEGORIES="taqebatcategories/";
    public static String DOAA_SUB_CATEGORIES="doaasubcategories/";
    public static String FORGOT_PASSWORD="sendverifycode";
    public static String TERMS_AND_CONDITIONS="terms";
    public static String AZKAR_DETAILS="zeker/";
    public static String TASKS="tasks/";
    public static String FINISHED_TASKS="finishedtasks/";
    public static String LATE_TASKS="tasks/";
    public static String MONSBAT="monasbat?date=";
    public static String ZYARAT_CATEGORIES="zyaratcategories/";
    public static String ZYARAT_DETAILS="zyara/";
    public static String TOOGLE_TASK="toggletask";
    public static String CONTACT_US="contactus";
    public static String NOTIFICATIONS="notifications";
    public static String FAVOURITE="favorites";
    public static String SEARCH="search";
    public static String ADD_TO_FAVOURITE="addtofavorite";
    public static String POLLS="polls";
    public static String CREATE_POLLS="create-poll";
    public static String SHARE_LINK ="http://deyaa.net/doaa/";


    //params
    public static String IMAGE= "users_img";



}
