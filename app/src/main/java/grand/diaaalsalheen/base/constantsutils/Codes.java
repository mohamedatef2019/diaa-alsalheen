package grand.diaaalsalheen.base.constantsutils;

public class Codes {

    //RequestsCodes
    public static int FILE_TYPE_IMAGE = 10;
    public static int FILE_TYPE_VIDEO = 11;
    public static int FILE_TYPE_PDF = 12;

    //ACTIONS
    public static int REGISTER_SCREEN = 25;
    public static int SHOW_MESSAGE = 22;
    public static int FORGOT_PASSWORD_SCREEN = 26;
    public static int ENTER_CODE_SCREEN = 27;
    public static int CHANGE_PASSWORD_SCREEN = 28;
    public static int SEND_CODE_SCREEN = 29;
    public static int HOME_SCREEN = 30;
    public static int LOGIN_SCREEN = 31;
    public static int SELECT_LOCATION = 34;
    public static int DATA_UPDATED = 35;
    public static int DATA_RECIVED = 36;
    public static int MONAGAT_CATEGORIES = 37;
    public static int MONGAT_DETAILS = 38;
    public static int ADAIA_CATEGORIES = 39;
    public static int ADAIA_SUB_CATEGORIES = 40;
    public static int AZKAR_CATEGORIES = 41;
    public static int WORKS_CATEGORIES = 42;
    public static int TAQEEB_CATEGORIES = 43;
    public static int QORAN = 44;
    public static int DOAA = 45;
    public static int PLAY_AUDIO = 46;
    public static int DOAA_DETAILS = 47;
    public static int MAIN_BEADS = 48;
    public static int ZEKR_DETAILS = 49;
    public static int PRAYER_TIMES = 50;
    public static int ZYARAT_DETAILS = 51;
    public static int ZYARAT = 52;
    public static int AMAL_CATEGORIES = 53;
    public static int AMAL_DETAILS = 54;
    public static int MUSLIM_CALENDER = 55;
    public static int TASKS = 56;
    public static int DELETE_TASK = 57;
    public static int ADD_TASK = 58;
    public static int VISIT_SOCIAL_MEDIA = 59;
    public static int GENDER_SELECT = 60;
    public static int DOCTRINE_SELECT = 61;
    public static int SET_CODE = 62;
    public static int RETURNED_DATA = 63;
    public static int LOCATION_SCREEN = 64;
    public static int QURAN_VIEWER_SCREEN = 65;
    public static int PRAYER_DETECTOR = 66;
    public static int ADD_TO_FAVOURITE = 67;
    public static int FAVOURITES = 68;
    public static int DELETE_TO_FAVOURITE = 69;
    public static int TAQEEB_DETAILS = 70;
    public static int SHARE = 71;

    public static int SOCIAL_REGISTER = 72;
    public static int CHANGE_LANGUAGE = 73;
    public static int ZOOM_IN = 74;
    public static int ZOOM_OUT = 75;
    public static int AMAL_SUB_CATEGORIES = 76;
    public static int SEARCH = 77;
    //Requested Code

    public static int SELECT_COMMERICAL_IMAGE = 229;
    public static int SELECT_PROFILE_IMAGE = 210;
    public static int SELECT_ACCOUNT_TYPE = 211;
    public static int PROFILE_PAGE = 212;

}
