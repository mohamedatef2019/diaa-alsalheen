package grand.diaaalsalheen.base.utils;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import grand.diaaalsalheen.base.views.activities.MainActivity;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.views.activities.BaseActivity;



public class MovementManager {


    //---------Fragments----------//
    private static final int CONTAINER_ID = R.id.fl_home_container;

    public static void popAllFragments(Context context) {
        FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    public static void addFragment(Context context, Fragment fragment, String backStackText) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().add(CONTAINER_ID, fragment);

        if (!backStackText.equals("")) {
            fragmentTransaction.addToBackStack(backStackText);
        }
        fragmentTransaction.commit();
    }


    public static void addFragment(Context context, Fragment fragment, String backStackText,Bundle bundle) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().add(CONTAINER_ID, fragment);
        fragment.setArguments(bundle);
        if (!backStackText.equals("")) {
            fragmentTransaction.addToBackStack(backStackText);
        }
        fragmentTransaction.commit();
    }


    public static void replaceFragment(Context context, Fragment fragment, String backStackText) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(CONTAINER_ID, fragment);
        if (!backStackText.equals("")) {
            fragmentTransaction.addToBackStack(backStackText);
        }
        fragmentTransaction.commit();
    }


    public static void popLastFragment(Context context) {
        ((FragmentActivity) context).getSupportFragmentManager().popBackStack();
    }


    //-----------Activities-----------------//

    public static void startActivity(Context context, Class<?> activity) {
        Intent intent = new Intent(context, activity);
        context.startActivity(intent);
    }

    public static void startActivityForResult(Context context, Class<?> activity , int requestCode, int page) {
        Intent intent = new Intent(context, activity);
        intent.putExtra(Params.INTENT_PAGE,page);
        ((AppCompatActivity)context).startActivityForResult(intent,requestCode);
    }

    public static void startActivity(Context context,int page) {
        Intent intent = new Intent(context, BaseActivity.class);
        intent.putExtra(Params.INTENT_PAGE,page);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int page, Bundle bundle) {
        Intent intent = new Intent(context, BaseActivity.class);
        intent.putExtra(Params.INTENT_PAGE,page);
        intent.putExtra(Params.INTENT_BUNDLE,bundle);
        context.startActivity(intent);
    }

    public static void startMainActivity(Context context,int page) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(Params.INTENT_PAGE,page);
        context.startActivity(intent);
    }

    public static void startWebPage(Context context, String page) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(page)));
        }
        catch (Exception e){
            e.getStackTrace();
        }
    }
}
