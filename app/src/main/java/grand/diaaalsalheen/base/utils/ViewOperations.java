package grand.diaaalsalheen.base.utils;


import android.app.Activity;
import android.content.Context;

import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


public class ViewOperations {


   // public static void setGlideImage(Context context, int image, ImageView imageView) {
     //   Glide.with(context).load(image).into(imageView);
   // }

    public static void setRVHorzontial(Context context, RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
    }


    public static void setRVHVertical(Context context, RecyclerView recyclerView) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(linearLayoutManager);
    }

    public static LinearLayoutManager getLayoutWithsetRVHVertical(Context context, RecyclerView recyclerView) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(linearLayoutManager);

        return  linearLayoutManager;
    }

    public static LinearLayoutManager setRVHVertical2(Context context, RecyclerView recyclerView) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        return linearLayoutManager;
    }


    public static void setRVHVerticalGrid(Context context, RecyclerView recyclerView, int numOfColumns) {
        recyclerView.setLayoutManager(new GridLayoutManager(context, numOfColumns));
    }

    public static void setRVHorzontialGrid(Context context, RecyclerView recyclerView, int numOfColumns) {
        new GridLayoutManager(context, numOfColumns, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(new GridLayoutManager(context, numOfColumns));
    }


    public static void setRelativeViewSize(View view, int width, int height) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height);
        view.setLayoutParams(layoutParams);
    }

    public static void setLinearViewSize(View view, int width, int height) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, height);
        view.setLayoutParams(layoutParams);
    }


    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }


    public static int getScreenWidth(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;

    }

    public static int getScreenHeight(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.heightPixels;
    }


    public static String replaceArabicNumbers(String original) {
        return original.replaceAll("١", "1")
                .replaceAll("٢", "2")
                .replaceAll("٣", "3")
                .replaceAll("٤", "4")
                .replaceAll("٥", "5")
                .replaceAll("٦", "6")
                .replaceAll("٧", "7")
                .replaceAll("٨", "8")
                .replaceAll("٩", "9")
                .replaceAll("٠", "0")
                .replaceAll("٫", ".");

       /* NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
        nf.setMaximumFractionDigits(10);
        String st= nf.format(original);
        return original; */
    }

    private PopupMenu showGraphTypePopUp(Context context,ArrayList<String>list,View view) {

        PopupMenu popupMenu = new PopupMenu(context, view);
            for (int i = 0; i < list.size(); i++) {
                popupMenu.getMenu().add(i, i, i, list.get(i));
            }


        popupMenu.show();

            return popupMenu ;
    }


}
