package grand.diaaalsalheen.base.viewmodel;


import androidx.databinding.BaseObservable;
import androidx.lifecycle.MutableLiveData;

public class BaseItemViewModel extends BaseObservable {

    private MutableLiveData<Integer> itemsOperationsLiveListener;


    public MutableLiveData<Integer> getItemsOperationsLiveListener() {
        return this.itemsOperationsLiveListener = this.itemsOperationsLiveListener == null ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }
}