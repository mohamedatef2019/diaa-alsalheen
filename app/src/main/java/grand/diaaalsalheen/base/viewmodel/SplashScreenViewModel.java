package grand.diaaalsalheen.base.viewmodel;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import com.android.volley.Request;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.base.volleyutils.MyApplication;
import grand.diaaalsalheen.login.request.UserRequest;
import grand.diaaalsalheen.login.response.UserItem;
import grand.diaaalsalheen.login.response.UserResponse;


public class SplashScreenViewModel extends BaseViewModel {

    public SplashScreenViewModel() {
        startApp();
    }

    private void startApp() {
        Handler handler = new Handler();
        handler.postDelayed(() -> {

            if (UserPreferenceHelper.isLogined()) {
                autoLogin();
            } else {
                 getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);

            }
        }, 2000);

    }


    private void autoLogin(){
    UserRequest userRequest = new UserRequest();
        UserItem userItem = UserPreferenceHelper.getUserDetails();
        userRequest.setPhone(userItem.getEmail());
        userRequest.setPassword(userItem.getPassword());


        try {
            userRequest.setToken(UserPreferenceHelper.getGoogleToken());
        }catch (Exception e){
            e.getStackTrace();
        }
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                UserResponse userResponse = (UserResponse) response;
                if (userResponse.getStatus().equals(WebServices.SUCCESS)) {
                    UserItem userItem = userResponse.getData();
                    userItem.setPassword(userRequest.getPassword());
                    UserPreferenceHelper.saveUserDetails(userItem);
                    getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);
                } else if (userResponse.getStatus().equals(WebServices.NOT_ACTIVE)) {
                    getClicksMutableLiveData().setValue(Codes.SEND_CODE_SCREEN);
                } else if (userResponse.getStatus().equals(WebServices.FAILED)) {
                    setReturnedMessage(userResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                }
            }
        }).requestJsonObject(Request.Method.POST, WebServices.LOGIN, userRequest, UserResponse.class);
    }
}