package grand.diaaalsalheen.notifications.viewmodel;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.notifications.request.NotificationRequest;
import grand.diaaalsalheen.notifications.response.NotificationsResponse;
import grand.diaaalsalheen.notifications.view.NotificationsAdapter;


public class NotificationsViewModel extends BaseViewModel {

    private NotificationsAdapter notificationsAdapter;
    private int visiablity = View.GONE;
    public NotificationsViewModel() {
        getServices();
    }


    @BindingAdapter({"app:adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, NotificationsAdapter notificationsAdapter) {
        recyclerView.setAdapter(notificationsAdapter);
    }


    public void setVisiablity(int visiablity) {
        this.visiablity = visiablity;
    }

    public int getVisiablity() {
        return visiablity;
    }

    @Bindable
    public NotificationsAdapter getNotificationsAdapter() {
        return this.notificationsAdapter == null ? this.notificationsAdapter = new NotificationsAdapter() : this.notificationsAdapter;
    }

    private void getServices() {

        NotificationRequest orderRequest1 = new NotificationRequest(UserPreferenceHelper.getUserDetails().getId());

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                NotificationsResponse servicesResponse = (NotificationsResponse)response;

                if(servicesResponse==null||servicesResponse.getData()==null||servicesResponse.getData().size()<1){
                    setVisiablity(View.VISIBLE);
                }

                assert servicesResponse != null;
                getNotificationsAdapter().updateData(servicesResponse.getData());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.NOTIFICATIONS, orderRequest1, NotificationsResponse.class);
    }
}
