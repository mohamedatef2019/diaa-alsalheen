package grand.diaaalsalheen.notifications.response;

 import com.google.gson.annotations.SerializedName;


public class NotificationItem {

	@SerializedName("FK_users_id")
	private String fKUsersId;

	@SerializedName("notifications_img")
	private String notificationsImg;

	@SerializedName("notifications_created_at")
	private String notificationsCreatedAt;

	@SerializedName("id")
	private int id;

	@SerializedName("notifications_desc")
	private String notificationsDesc;

	@SerializedName("notifications_title")
	private String notificationsTitle;

	public void setFKUsersId(String fKUsersId){
		this.fKUsersId = fKUsersId;
	}

	public String getFKUsersId(){
		return fKUsersId;
	}

	public void setNotificationsImg(String notificationsImg){
		this.notificationsImg = notificationsImg;
	}

	public String getNotificationsImg(){
		return notificationsImg;
	}

	public void setNotificationsCreatedAt(String notificationsCreatedAt){
		this.notificationsCreatedAt = notificationsCreatedAt;
	}

	public String getNotificationsCreatedAt(){
		return notificationsCreatedAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setNotificationsDesc(String notificationsDesc){
		this.notificationsDesc = notificationsDesc;
	}

	public String getNotificationsDesc(){
		return notificationsDesc;
	}

	public void setNotificationsTitle(String notificationsTitle){
		this.notificationsTitle = notificationsTitle;
	}

	public String getNotificationsTitle(){
		return notificationsTitle;
	}

	@Override
 	public String toString(){
		return 
			"NotifcationItem{" + 
			"fK_users_id = '" + fKUsersId + '\'' + 
			",notifications_img = '" + notificationsImg + '\'' + 
			",notifications_created_at = '" + notificationsCreatedAt + '\'' + 
			",id = '" + id + '\'' + 
			",notifications_desc = '" + notificationsDesc + '\'' + 
			",notifications_title = '" + notificationsTitle + '\'' + 
			"}";
		}
}