package grand.diaaalsalheen.notifications.request;

import com.google.gson.annotations.SerializedName;

public class NotificationRequest {

    @SerializedName("user_id")
    int userId;

    public NotificationRequest(int userId) {
        this.userId = userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }
}
