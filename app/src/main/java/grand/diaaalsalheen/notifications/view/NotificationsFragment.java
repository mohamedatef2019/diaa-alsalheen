package grand.diaaalsalheen.notifications.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentNotificationsBinding;
import grand.diaaalsalheen.notifications.request.NotificationRequest;
import grand.diaaalsalheen.notifications.viewmodel.NotificationsViewModel;


public class NotificationsFragment extends BaseFragment {
    private View rootView;
    private NotificationsViewModel notificationsViewModel;
    private FragmentNotificationsBinding fragmentNotificationsBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentNotificationsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notifications, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentNotificationsBinding.getRoot();
        notificationsViewModel = new NotificationsViewModel();
        fragmentNotificationsBinding.setNotificationsViewModel(notificationsViewModel);
    }


    private void liveDataListeners() {
        notificationsViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });
    }




}