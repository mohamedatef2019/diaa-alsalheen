
package grand.diaaalsalheen.notifications.view;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.ZoomAbleActivity;
import grand.diaaalsalheen.databinding.ItemNotificationBinding;
import grand.diaaalsalheen.notifications.response.NotificationItem;
import grand.diaaalsalheen.notifications.viewmodel.NotificationItemViewModel;



public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.CategoriesViewHolder> {

    private List<NotificationItem> notificationItems;


    public NotificationsAdapter() {
        this.notificationItems = new ArrayList<>();
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification,
                new FrameLayout(parent.getContext()), false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, final int position) {
        NotificationItem dataModel = notificationItems.get(position);
        NotificationItemViewModel orderItemViewModel = new NotificationItemViewModel(dataModel);
        orderItemViewModel.getItemsOperationsLiveListener().observeForever(new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {

            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dataModel.getNotificationsImg()!=null&&!dataModel.getNotificationsImg().equals("")){

                    Intent intent = new Intent(holder.itemView.getContext(), ZoomAbleActivity.class);
                    intent.putExtra("image",dataModel.getNotificationsImg());
                    holder.itemView.getContext().startActivity(intent);
                }
            }
        });
        holder.setViewModel(orderItemViewModel);
    }

    @Override
    public int getItemCount() {
        return this.notificationItems.size();
    }

    @Override
    public void onViewAttachedToWindow(CategoriesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CategoriesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<NotificationItem> data) {

            this.notificationItems.clear();

            this.notificationItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {
        ItemNotificationBinding itemNotificationBinding;

        CategoriesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemNotificationBinding == null) {
                itemNotificationBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemNotificationBinding != null) {
                itemNotificationBinding.unbind();
            }
        }

        void setViewModel(NotificationItemViewModel notificationItemViewModel) {
            if (itemNotificationBinding != null) {
                itemNotificationBinding.setNotificationItemViewModel(notificationItemViewModel);
            }
        }


    }



}
