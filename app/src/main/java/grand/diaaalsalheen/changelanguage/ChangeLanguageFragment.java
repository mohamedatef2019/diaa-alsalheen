package grand.diaaalsalheen.changelanguage;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.activities.SplashScreenActivity;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.base.views.navigationdrawer.MenuViewModel;
import grand.diaaalsalheen.databinding.FragmentChangeLanguageBinding;


public class ChangeLanguageFragment extends BaseFragment {

    private View rootView;

   private FragmentChangeLanguageBinding fragmentChangeLanguageBindingl;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentChangeLanguageBindingl = DataBindingUtil.inflate(inflater, R.layout.fragment_change_language, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
    }

    private void binding() {
        rootView = fragmentChangeLanguageBindingl.getRoot();
        fragmentChangeLanguageBindingl.rgChangeLanguageOptions.setOnCheckedChangeListener((group, checkedId) -> {

            if(checkedId == R.id.rb_change_language_arabic){
                MenuViewModel.changeLanguage(context, "ar");
            }else {
                MenuViewModel.changeLanguage(context, "en");
            }


        });

        fragmentChangeLanguageBindingl.rgChangeMzahebOptions.setOnCheckedChangeListener((group, checkedId) -> {

            if(checkedId == R.id.rb_change_language_shia){
                UserPreferenceHelper.setCurrentMazhab(  "shia");
            }else {
                UserPreferenceHelper.setCurrentMazhab( "suny");

            }


        });


        fragmentChangeLanguageBindingl.btnChangeLanguage.setOnClickListener(v -> {
            getActivity().finish();
            MovementManager.startActivity(getActivity(), Codes.LOCATION_SCREEN);
        });
    }



}