package grand.diaaalsalheen.login.view;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Objects;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentLoginBinding;
import grand.diaaalsalheen.login.viewmodel.LoginViewModel;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;


public class LoginFragment extends BaseFragment {

    private View rootView;

    private LoginViewModel loginViewModel;
    private FragmentLoginBinding fragmentLoginBinding;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);

        initGoogle();


        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = getActivity().getPackageManager().getPackageInfo(
                    getActivity().getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.getStackTrace();
        }


        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentLoginBinding.getRoot();
        loginViewModel = new LoginViewModel();
        fragmentLoginBinding.setLoginViewModel(loginViewModel);
    }

    private static final String EMAIL = "email";
    CallbackManager callbackManager;

    private void liveDataListeners() {
        loginViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.SOCIAL_REGISTER) {

                Bundle bundle = new Bundle();
                bundle.putString("name", name);
                bundle.putString("email", email);
                bundle.putString("image", image);
                bundle.putString("social_id", social_id);
                bundle.putBoolean("is_social", true);
                MovementManager.startActivity(getActivity(), Codes.REGISTER_SCREEN,bundle);

            } else if (result == Codes.SEND_CODE_SCREEN || result == Codes.REGISTER_SCREEN || result == Codes.FORGOT_PASSWORD_SCREEN) {

                MovementManager.startActivity(getActivity(), result);

            } else if (result == Codes.HOME_SCREEN) {

                Objects.requireNonNull(getActivity()).finish();
                MovementManager.startMainActivity(getActivity(), result);

            } else if (result == Codes.SHOW_MESSAGE) {

                showMessage(Objects.requireNonNull(getActivity()).getString(R.string.msg_wrong_password));

            }
        });


        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });



        LoginButton loginButton = (LoginButton) fragmentLoginBinding.lbLoginFacebook;
        loginButton.setReadPermissions(Arrays.asList(EMAIL, "public_profile"));



        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        (object, response) -> {

                            try {

                                social_id = object.getString("id");
                                email = object.getString("email");
                                name = object.getString("name");
                                image = "https://graph.facebook.com/v3.2/"+object.getString("id")+"/picture";

                                loginViewModel.goLogin(social_id);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        fragmentLoginBinding.ivLoginGoogle.setOnClickListener(view -> signOut());

        fragmentLoginBinding.ivLoginFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginButton.performClick();
            }
        });


    }

    GoogleSignInClient mGoogleSignInClient;
    public static final int RC_SIGN_IN = 1001;

    String social_id = "", email = "", name = "", image = "";

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {

        GoogleSignInAccount account = null;
        try {
            account = completedTask.getResult(ApiException.class);

        } catch (ApiException e) {
            e.printStackTrace();
        }

        if (account != null) {
            if (account.getId() != null)
                social_id = account.getId();

            if (account.getEmail() != null) {
                email = account.getEmail();
            }

            if (account.getDisplayName() != null)
                name = account.getDisplayName();

            if (account.getPhotoUrl() != null) {
                Log.e("image", account.getPhotoUrl().toString());
                image = account.getPhotoUrl().toString();
            } else {
                image = "https://www.qualiscare.com/wp-content/uploads/2017/08/default-user.png";
            }
        }

        loginViewModel.goLogin(social_id);

        //loginSocialMedia(email, social_id);

    }


    private void signOut() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void initGoogle() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestId()
                .requestEmail()
                .requestProfile()
                .build();


        mGoogleSignInClient = GoogleSignIn.getClient(context, gso);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RC_SIGN_IN) {

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }


        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


}