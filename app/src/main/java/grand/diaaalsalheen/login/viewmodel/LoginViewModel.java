package grand.diaaalsalheen.login.viewmodel;

import android.util.Log;
import android.view.View;

import androidx.databinding.Bindable;

import com.android.volley.Request;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.base.volleyutils.MyApplication;
import grand.diaaalsalheen.login.request.UserRequest;
import grand.diaaalsalheen.login.response.UserItem;
import grand.diaaalsalheen.login.response.UserResponse;


public class LoginViewModel extends BaseViewModel {
    private UserRequest userRequest;


    public LoginViewModel() {
        userRequest = new UserRequest();
    }

    @Bindable
    public UserRequest getUserDetails() {
        return userRequest;
    }


    public void loginClick() {
        goLogin();
    }

    public void skipClick() {
        getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);
    }

    public void newAccountClick() {
        getClicksMutableLiveData().setValue(Codes.REGISTER_SCREEN);
    }


    public void forgotPassword() {
        getClicksMutableLiveData().setValue(Codes.FORGOT_PASSWORD_SCREEN);
    }

    private void goLogin() {

        if (validate()) {

            userRequest.setToken(UserPreferenceHelper.getGoogleToken());
            accessLoadingBar(View.VISIBLE);
            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    accessLoadingBar(View.GONE);
                    UserResponse userResponse = (UserResponse) response;
                    if (userResponse.getStatus().equals(WebServices.SUCCESS)) {
                        UserItem userItem = userResponse.getData();
                        userItem.setPassword(userRequest.getPassword());
                        UserPreferenceHelper.saveUserDetails(userItem);
                         getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);
                    } else if (userResponse.getStatus().equals(WebServices.NOT_ACTIVE)) {
                        getClicksMutableLiveData().setValue(Codes.SEND_CODE_SCREEN);
                    } else if (userResponse.getStatus().equals(WebServices.FAILED)) {
                        getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                    }
                }
            }).requestJsonObject(Request.Method.POST, WebServices.LOGIN, userRequest, UserResponse.class);

        }


    }

    public void goLogin(String socialId) {

            userRequest.setSocialId(socialId);

            userRequest.setToken(UserPreferenceHelper.getGoogleToken());
            accessLoadingBar(View.VISIBLE);
            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    accessLoadingBar(View.GONE);
                    UserResponse userResponse = (UserResponse) response;
                    if (userResponse.getStatus().equals(WebServices.SUCCESS)) {
                        UserItem userItem = userResponse.getData();
                        userItem.setPassword(userRequest.getPassword());
                        UserPreferenceHelper.saveUserDetails(userItem);
                         getClicksMutableLiveData().setValue(Codes.HOME_SCREEN);
                    } else if (userResponse.getStatus().equals(WebServices.NOT_ACTIVE)) {
                        getClicksMutableLiveData().setValue(Codes.SEND_CODE_SCREEN);
                    } else if (userResponse.getStatus().equals(WebServices.FAILED)) {
                        getClicksMutableLiveData().setValue(Codes.SOCIAL_REGISTER);
                    }
                }
            }).requestJsonObject(Request.Method.POST, WebServices.LOGIN, userRequest, UserResponse.class);
    }


    private boolean validate() {

        return true;
    }

}
