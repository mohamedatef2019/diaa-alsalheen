package grand.diaaalsalheen.login.response;

 import com.google.gson.annotations.Expose;
 import com.google.gson.annotations.SerializedName;

 public class UserItem{

	@SerializedName("users_img")
	private String usersImg;

	@SerializedName("users_created_at")
	private String usersCreatedAt;

	@SerializedName("verified_status")
	private String verifiedStatus;

	@SerializedName("lng")
	private String lng;

	@SerializedName("users_gender")
	private String usersGender;

	@SerializedName("jwt")
	private String jwt;

	@SerializedName("firebase_token")
	private String firebaseToken;

	@SerializedName("social_token")
	private Object socialToken;

	@SerializedName("users_name")
	private String usersName;

	@SerializedName("phone")
	private String phone;

	@SerializedName("users_id")
	private int usersId;

	@SerializedName("users_doctrine")
	private String usersDoctrine;

	@SerializedName("email")
	private String email;


     @Expose
     private String password;

	@SerializedName("lat")
	private String lat;

	public void setUsersImg(String usersImg){
		this.usersImg = usersImg;
	}

	public String getUsersImg(){
		return usersImg;
	}

	public void setUsersCreatedAt(String usersCreatedAt){
		this.usersCreatedAt = usersCreatedAt;
	}

	public String getUsersCreatedAt(){
		return usersCreatedAt;
	}

	public void setVerifiedStatus(String verifiedStatus){
		this.verifiedStatus = verifiedStatus;
	}

	public String getVerifiedStatus(){
		return verifiedStatus;
	}

	public void setLng(String lng){
		this.lng = lng;
	}

	public String getLng(){
		return lng;
	}

	public void setUsersGender(String usersGender){
		this.usersGender = usersGender;
	}

	public String getUsersGender(){
		return usersGender;
	}

	public void setJwt(String jwt){
		this.jwt = jwt;
	}

	public String getJwt(){
		return jwt;
	}

	public void setFirebaseToken(String firebaseToken){
		this.firebaseToken = firebaseToken;
	}

	public String getFirebaseToken(){
		return firebaseToken;
	}

	public void setSocialToken(Object socialToken){
		this.socialToken = socialToken;
	}

	public Object getSocialToken(){
		return socialToken;
	}

	public void setUsersName(String usersName){
		this.usersName = usersName;
	}

	public String getUsersName(){
		return usersName;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setId(int usersId){
		this.usersId = usersId;
	}

	public int getId(){
		return usersId;
	}

	public void setUsersDoctrine(String usersDoctrine){
		this.usersDoctrine = usersDoctrine;
	}

	public String getUsersDoctrine(){
		return usersDoctrine;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setLat(String lat){
		this.lat = lat;
	}

	public String getLat(){
		return lat;
	}

     public void setPassword(String password) {
         this.password = password;
     }

     public String getPassword() {
         return password;
     }

     @Override
 	public String toString(){
		return 
			"UserItem{" + 
			"users_img = '" + usersImg + '\'' + 
			",users_created_at = '" + usersCreatedAt + '\'' + 
			",verified_status = '" + verifiedStatus + '\'' + 
			",lng = '" + lng + '\'' + 
			",users_gender = '" + usersGender + '\'' + 
			",jwt = '" + jwt + '\'' + 
			",firebase_token = '" + firebaseToken + '\'' + 
			",social_token = '" + socialToken + '\'' + 
			",users_name = '" + usersName + '\'' + 
			",phone = '" + phone + '\'' + 
			",users_id = '" + usersId + '\'' + 
			",users_doctrine = '" + usersDoctrine + '\'' + 
			",email = '" + email + '\'' + 
			",lat = '" + lat + '\'' + 
			"}";
		}
}