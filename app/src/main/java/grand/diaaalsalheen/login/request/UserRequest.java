package grand.diaaalsalheen.login.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class  UserRequest {

    @SerializedName("password")
    private String password;

    @SerializedName("loginkey")
    private String phone;

    @SerializedName("social_token")
    private String socialId;

    @SerializedName("firebase_token")
    private String token="ddd";


    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getSocialId() {
        return socialId;
    }
}
