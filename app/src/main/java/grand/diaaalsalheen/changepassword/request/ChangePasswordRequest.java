package grand.diaaalsalheen.changepassword.request;


import com.google.gson.annotations.SerializedName;

public class ChangePasswordRequest {
	@SerializedName("user_id")
    private  int userId;

	@SerializedName("old_password")
    private String oldPassword;

	@SerializedName("password")
    private  String newPassword;


	@SerializedName("phone")
    private  String phone;






	public String getNewPassword() {
		return newPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }
}

