package grand.diaaalsalheen.changepassword.viewmodel;


import android.view.View;
import androidx.databinding.Bindable;
import com.android.volley.Request;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.base.volleyutils.MyApplication;
import grand.diaaalsalheen.changepassword.request.ChangePasswordRequest;
import grand.diaaalsalheen.changepassword.response.ChangePasswordResponse;


public class ChangePasswordViewModel extends BaseViewModel {
    private ChangePasswordRequest changePasswordRequest;
    private boolean isForgotPassword;

    public ChangePasswordViewModel(boolean isForgotPassword) {
        this.isForgotPassword = isForgotPassword;
        changePasswordRequest = new ChangePasswordRequest();
    }


    @Bindable
    public ChangePasswordRequest getChangePasswordRequest() {
        return changePasswordRequest;
    }

    public void setChangePasswordRequest(ChangePasswordRequest changePasswordRequest) {
        this.changePasswordRequest = changePasswordRequest;
    }


    public boolean isForgotPassword() {
        return isForgotPassword;
    }

    public void setForgotPassword(boolean forgotPassword) {
        isForgotPassword = forgotPassword;
    }

    public void changePasswordClick() {
        if (isForgotPassword) {
            changePasswordFromForget();
        } else {
            changePassword();
        }
    }

    private void changePassword() {
        changePasswordRequest.setUserId(UserPreferenceHelper.getUserDetails().getId());
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                ChangePasswordResponse sendCodeResponse = (ChangePasswordResponse) response;
                if (sendCodeResponse.getStatus().equals(WebServices.SUCCESS)) {
                    getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
                } else {
                    setReturnedMessage(sendCodeResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.UPDATE_PASSWORD, changePasswordRequest, ChangePasswordResponse.class);
    }

    private void changePasswordFromForget() {
        changePasswordRequest.setUserId(UserPreferenceHelper.getUserDetails().getId());
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                ChangePasswordResponse sendCodeResponse = (ChangePasswordResponse) response;
                if (sendCodeResponse.getStatus().equals(WebServices.SUCCESS)) {
                    getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
                } else {
                    setReturnedMessage(sendCodeResponse.getMsg());
                    getClicksMutableLiveData().setValue(Codes.SHOW_MESSAGE);
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.RENEW_PASSWORD, changePasswordRequest, ChangePasswordResponse.class);
    }

}


