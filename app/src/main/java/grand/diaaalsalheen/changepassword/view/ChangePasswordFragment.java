package grand.diaaalsalheen.changepassword.view;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.changepassword.viewmodel.ChangePasswordViewModel;
import grand.diaaalsalheen.databinding.FragmentUpdatePasswordBinding;


public class ChangePasswordFragment extends BaseFragment {

    View rootView;
    ChangePasswordViewModel changePasswordViewModel;
    FragmentUpdatePasswordBinding fragmentUpdatePasswordBinding;
    boolean isForgotPassword = false;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentUpdatePasswordBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_update_password, container, false);
        init();

        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentUpdatePasswordBinding.getRoot();

        try {
            assert getArguments() != null;
            isForgotPassword = getArguments().getBoolean(Params.IS_FORGOT_PASSWORD, false);
            changePasswordViewModel = new ChangePasswordViewModel(isForgotPassword);
            changePasswordViewModel.getChangePasswordRequest().setPhone(getArguments().getString(Params.BUNDLE_PHONE, ""));
            fragmentUpdatePasswordBinding.tvMainTitle.setText(ResourcesManager.getString(R.string.hint_change_password));
        } catch (Exception e) {
            changePasswordViewModel = new ChangePasswordViewModel(false);
            e.getStackTrace();
        }


        fragmentUpdatePasswordBinding.setChangePasswordViewModel(changePasswordViewModel);
    }


    private void liveDataListeners() {
        changePasswordViewModel.getClicksMutableLiveData().observe(this, result -> {

            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.DATA_RECIVED) {

                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                sweetAlertDialog.setTitleText(ResourcesManager.getString(R.string.hint_change_password));
                sweetAlertDialog.setContentText(ResourcesManager.getString(R.string.label_done));
                sweetAlertDialog.setConfirmButton(ResourcesManager.getString(R.string.action_close), sweetAlertDialog2 -> {

                    if (isForgotPassword) {
                        Objects.requireNonNull(getActivity()).finishAffinity();
                        MovementManager.startActivity(getActivity(), Codes.LOGIN_SCREEN);
                    } else {
                        ((AppCompatActivity) context).finish();
                    }


                    sweetAlertDialog2.cancel();
                    sweetAlertDialog2.dismiss();
                });
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.setCanceledOnTouchOutside(false);
                sweetAlertDialog.show();


            } else if (result == Codes.SHOW_MESSAGE) {
                showMessage(changePasswordViewModel.getReturnedMessage());

            }

        });
    }


}