package grand.diaaalsalheen.socialmedia.viewmodel;


import android.util.Log;
import android.view.View;

import androidx.databinding.Bindable;

import com.android.volley.Request;

import java.util.List;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.socialmedia.response.SocialMediaItem;
import grand.diaaalsalheen.socialmedia.response.SocialMediaResponse;


public class SocialMediaViewModel extends BaseViewModel {

    private List<SocialMediaItem> socialMediaItems;
    private String selectedSocialMedia;

    public SocialMediaViewModel() {
        getSocialMedia();
    }


    private void getSocialMedia() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                socialMediaItems = ((SocialMediaResponse)response).getData();
                notifyChange();
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.SOCIAL_MEDIA, new Object(), SocialMediaResponse.class);
    }

    public void onSocialMediaClick(int i){
        setSelectedSocialMedia(socialMediaItems.get(i).getSocialsLink());
        getClicksMutableLiveData().setValue(Codes.VISIT_SOCIAL_MEDIA);
    }


    public void setSelectedSocialMedia(String selectedSocialMedia) {
        this.selectedSocialMedia = selectedSocialMedia;
    }

    public String getSelectedSocialMedia() {
        return selectedSocialMedia;
    }
}
