package grand.diaaalsalheen.socialmedia.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentAboutUsBinding;
import grand.diaaalsalheen.databinding.FragmentSocialMediaBinding;
import grand.diaaalsalheen.socialmedia.viewmodel.SocialMediaViewModel;


public class SocialMediaFragment extends BaseFragment {

    private View rootView;
    private SocialMediaViewModel socialMediaViewModel;
    private FragmentSocialMediaBinding fragmentSocialMediaBinding;



    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSocialMediaBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_social_media, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentSocialMediaBinding.getRoot();
        socialMediaViewModel = new SocialMediaViewModel();
        fragmentSocialMediaBinding.setSocialMediaViewModel(socialMediaViewModel);
    }

    
    private void liveDataListeners() {
        socialMediaViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }else if(result == Codes.VISIT_SOCIAL_MEDIA){
                MovementManager.startWebPage(getActivity(),socialMediaViewModel.getSelectedSocialMedia());
            }
        });
    }






}