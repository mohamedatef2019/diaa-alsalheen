package grand.diaaalsalheen.socialmedia.response;


import com.google.gson.annotations.SerializedName;


public class SocialMediaItem {

	@SerializedName("socials_img")
	private String socialsImg;

	@SerializedName("socials_id")
	private int socialsId;

	@SerializedName("socials_link")
	private String socialsLink;

	public void setSocialsImg(String socialsImg){
		this.socialsImg = socialsImg;
	}

	public String getSocialsImg(){
		return socialsImg;
	}

	public void setSocialsId(int socialsId){
		this.socialsId = socialsId;
	}

	public int getSocialsId(){
		return socialsId;
	}

	public void setSocialsLink(String socialsLink){
		this.socialsLink = socialsLink;
	}

	public String getSocialsLink(){
		return socialsLink;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"socials_img = '" + socialsImg + '\'' + 
			",socials_id = '" + socialsId + '\'' + 
			",socials_link = '" + socialsLink + '\'' + 
			"}";
		}
}