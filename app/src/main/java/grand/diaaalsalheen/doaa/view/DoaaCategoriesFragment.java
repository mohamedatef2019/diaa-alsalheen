package grand.diaaalsalheen.doaa.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentAdaiaBinding;
import grand.diaaalsalheen.doaa.viewmodel.DoaaViewModel;


public class DoaaCategoriesFragment extends BaseFragment {
    private View rootView;
    private DoaaViewModel doaaViewModel;
    private FragmentAdaiaBinding fragmentAdaiaBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAdaiaBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_adaia, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();

    }

    private void binding() {
        rootView = fragmentAdaiaBinding.getRoot();
        doaaViewModel = new DoaaViewModel();
        fragmentAdaiaBinding.setDoaaViewModel(doaaViewModel);
    }


    private void liveDataListeners() {
        doaaViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });

        doaaViewModel.getAdaiaCategoriesAdapter().getItemsOperationsLiveListener().observe(this, categoriesItem -> {

             doaaViewModel.getAdiaSubCategories(categoriesItem.getId());
        });

        doaaViewModel.getAdaiaSubCategoriesAdapter().getItemsOperationsLiveListener().observe(this, categoriesItem -> {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Params.BUNDLE_DOAA_ITEM, categoriesItem);
            MovementManager.startActivity(getActivity(), Codes.DOAA_DETAILS, bundle);
        });
    }


}