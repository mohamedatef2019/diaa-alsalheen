
package grand.diaaalsalheen.doaa.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.databinding.ItemDoaaSubCategoryBinding;
import grand.diaaalsalheen.doaa.response.SubcategoriesItem;
import grand.diaaalsalheen.doaa.viewmodel.SubCategoriesItemViewModel;


public class AdaiaSubCategoriesAdapter extends RecyclerView.Adapter<AdaiaSubCategoriesAdapter.CompaniesViewHolder> {

    private List<SubcategoriesItem> subcategoriesItems;
    private MutableLiveData<SubcategoriesItem> itemsOperationsLiveListener;


    public AdaiaSubCategoriesAdapter() {
        this.subcategoriesItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_doaa_sub_category,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {
        SubcategoriesItem subcategoriesItem = subcategoriesItems.get(position);
        SubCategoriesItemViewModel subCategoriesItemViewModel = new SubCategoriesItemViewModel(subcategoriesItem,position);
        subCategoriesItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
            itemsOperationsLiveListener.setValue(subcategoriesItem);
        });
        holder.setViewModel(subCategoriesItemViewModel);
    }



    public MutableLiveData<SubcategoriesItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.subcategoriesItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<SubcategoriesItem> data) {
        this.subcategoriesItems.clear();
        this.subcategoriesItems.addAll(data);
        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemDoaaSubCategoryBinding itemDoaaSubCategoryBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemDoaaSubCategoryBinding == null) {
                itemDoaaSubCategoryBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemDoaaSubCategoryBinding != null) {
                itemDoaaSubCategoryBinding.unbind();
            }
        }

        void setViewModel(SubCategoriesItemViewModel subCategoriesItemViewModel) {
            if (itemDoaaSubCategoryBinding != null) {
                itemDoaaSubCategoryBinding.setItemDoaaSubCateory(subCategoriesItemViewModel);
            }
        }


    }



}
