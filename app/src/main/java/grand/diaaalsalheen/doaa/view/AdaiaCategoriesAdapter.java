
package grand.diaaalsalheen.doaa.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.databinding.ItemDoaaCategoryBinding;
import grand.diaaalsalheen.databinding.ItemMongatBinding;
import grand.diaaalsalheen.doaa.response.CategoriesItem;
import grand.diaaalsalheen.doaa.viewmodel.CategoriesItemViewModel;



public class AdaiaCategoriesAdapter extends RecyclerView.Adapter<AdaiaCategoriesAdapter.CompaniesViewHolder> {

    private List<CategoriesItem> adiaCategoryItems;
    private MutableLiveData<CategoriesItem> itemsOperationsLiveListener;
    private int currentPosition = 0;

    public AdaiaCategoriesAdapter() {
        this.adiaCategoryItems = new ArrayList<>();
    }

    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_doaa_category,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {
        CategoriesItem adiaCategoryItem = adiaCategoryItems.get(position);
        CategoriesItemViewModel companyItemViewModel = new CategoriesItemViewModel(adiaCategoryItem);
        companyItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {


            adiaCategoryItems.get(currentPosition).setSelected(false);
            notifyItemChanged(currentPosition);
            adiaCategoryItems.get(position).setSelected(true);
            currentPosition = position;
            notifyItemChanged(position);


           // if(position==0) {
              //  MovementManager.startActivity(holder.itemView.getContext(), Codes.TAQEEB_CATEGORIES);
           // }else {
                itemsOperationsLiveListener.setValue(adiaCategoryItem);
           // }



        });
        holder.setViewModel(companyItemViewModel);
    }



    public MutableLiveData<CategoriesItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.adiaCategoryItems.size();
    }

    @Override
    public void onViewAttachedToWindow(CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<CategoriesItem> data) {

            this.adiaCategoryItems.clear();

            this.adiaCategoryItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemDoaaCategoryBinding itemDoaaCategoryBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemDoaaCategoryBinding == null) {
                itemDoaaCategoryBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemDoaaCategoryBinding != null) {
                itemDoaaCategoryBinding.unbind();
            }
        }

        void setViewModel(CategoriesItemViewModel adiaCategoryItemViewModel) {
            if (itemDoaaCategoryBinding != null) {
                itemDoaaCategoryBinding.setItemDoaaCateory(adiaCategoryItemViewModel);
            }
        }


    }



}
