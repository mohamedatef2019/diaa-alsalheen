package grand.diaaalsalheen.doaa.view;


import android.content.Intent;
import android.os.Bundle;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.huhx0015.hxaudio.audio.HXMusic;

import org.sufficientlysecure.htmltextview.HtmlFormatter;
import org.sufficientlysecure.htmltextview.HtmlFormatterBuilder;

import java.util.Objects;
import grand.diaaalsalheen.BuildConfig;
import grand.diaaalsalheen.MediaPlayerUtils;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.SweetAlertDialogss;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentDoaaDetailsBinding;
import grand.diaaalsalheen.doaa.response.SubcategoriesItem;
import grand.diaaalsalheen.doaa.viewmodel.DoaaDetailsViewModel;


public class DoaaDetailsFragment extends BaseFragment {
    private View rootView;
    private DoaaDetailsViewModel doaaDetailsViewModel;
    private FragmentDoaaDetailsBinding fragmentDoaaDetailsBinding;
    SubcategoriesItem subcategoriesItem;
    int textSize = 0;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentDoaaDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_doaa_details, container, false);
        assert getArguments() != null;

        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();

    }

    private void binding() {
        rootView = fragmentDoaaDetailsBinding.getRoot();
        subcategoriesItem = (SubcategoriesItem) Objects.requireNonNull(getArguments()).getSerializable(Params.BUNDLE_DOAA_ITEM);
        doaaDetailsViewModel = new DoaaDetailsViewModel(subcategoriesItem);
        fragmentDoaaDetailsBinding.setDoaaDetailsViewModel(doaaDetailsViewModel);
    }

    @Override
    public void onDestroyView() {
        HXMusic.stop();
        super.onDestroyView();
    }

    private boolean isPlayedBefore=false;
    private void liveDataListeners() {
        doaaDetailsViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.PLAY_AUDIO) {

                MediaPlayerUtils.playAudio(doaaDetailsViewModel.getDoaaDetailsItem().getVoice(), subcategoriesItem.getName(), getActivity(),fragmentDoaaDetailsBinding.ivPlay);

            } else if (result == Codes.ADD_TO_FAVOURITE) {

                if (UserPreferenceHelper.isLogined()) {
                    SweetAlertDialogss.showFavDialog(getActivity());
                } else {
                    SweetAlertDialogss.loginToContinue(getActivity());
                }

            } else if (result == Codes.ZOOM_IN) {
                textSize+=5;
                fragmentDoaaDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            } else if (result == Codes.ZOOM_OUT) {
                textSize-=5;
                fragmentDoaaDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            } else if (result == Codes.DATA_RECIVED) {
                Spanned formattedHtml = HtmlFormatter.formatHtml(new HtmlFormatterBuilder().setHtml(doaaDetailsViewModel.getDoaaDetailsItem().getText()));
                String str=doaaDetailsViewModel.getDoaaDetailsItem().getText();
                fragmentDoaaDetailsBinding.tvHtmlViewer.loadDataWithBaseURL(null, str, "text/html", "utf-8", null);
               // fragmentDoaaDetailsBinding.tvHtmlViewer.getSettings().setBuiltInZoomControls(true);
               // fragmentDoaaDetailsBinding.tvHtmlViewer.setText(formattedHtml);
                textSize =  270;
                fragmentDoaaDetailsBinding.tvHtmlViewer.setInitialScale(textSize);




            } else if (result == Codes.SHARE) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, WebServices.SHARE_LINK+"3/"+ doaaDetailsViewModel.getDoaaDetailsItem().getId() + "/ar \n\n" + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }

        });


    }


}