package grand.diaaalsalheen.doaa.viewmodel;

import android.view.View;

import com.android.volley.Request;
import com.huhx0015.hxaudio.audio.HXMusic;

import grand.diaaalsalheen.FavouriteRequest;
import grand.diaaalsalheen.SweetAlertDialogss;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.doaa.response.DoaaDetailsItem;
import grand.diaaalsalheen.doaa.response.DoaaDetailsResponse;
import grand.diaaalsalheen.doaa.response.DoaaItem;
import grand.diaaalsalheen.doaa.response.SubcategoriesItem;
import grand.diaaalsalheen.favourite.viewmodel.FavouriteViewModel;
import grand.diaaalsalheen.mongat.response.MonagatCategoryItem;
import grand.diaaalsalheen.mongat.response.MongatDetailsItem;
import grand.diaaalsalheen.mongat.response.MongatDetailsResponse;


public class DoaaDetailsViewModel extends BaseViewModel {

    private DoaaDetailsItem doaaDetailsItem;


    public DoaaDetailsViewModel(SubcategoriesItem doaaItem) {
        getDoaaDetails(doaaItem);
    }

    private void getDoaaDetails(SubcategoriesItem doaaItem) {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                DoaaDetailsResponse doaaDetailsResponse = (DoaaDetailsResponse) response;
                doaaDetailsItem = doaaDetailsResponse.getData();
                accessLoadingBar(View.GONE);
                getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.DOAA_DETAILS + doaaItem.getId(), null, DoaaDetailsResponse.class);
    }

    public DoaaDetailsItem getDoaaDetailsItem() {
        return doaaDetailsItem;
    }


    public void onZoomInFontClick() {
        getClicksMutableLiveData().setValue(Codes.ZOOM_IN);
    }

    public void onZoomOutFontClick() {
        getClicksMutableLiveData().setValue(Codes.ZOOM_OUT);
    }

    public void onPlayAudioClick() {
        getClicksMutableLiveData().setValue(Codes.PLAY_AUDIO);
    }

    public void addToFavouriteClick() {
     
        getClicksMutableLiveData().setValue(Codes.ADD_TO_FAVOURITE);
        FavouriteViewModel.addToFavourite(new FavouriteRequest(getDoaaDetailsItem().getId(), "App\\Models\\Doaa"));

    }

    public void onShareClick() {
        getClicksMutableLiveData().setValue(Codes.SHARE);
    }


}
