package grand.diaaalsalheen.doaa.viewmodel;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.doaa.response.CategoriesItem;
import grand.diaaalsalheen.doaa.response.SubcategoriesItem;
import grand.diaaalsalheen.home.viewmodel.ImagesViewModel;
import grand.diaaalsalheen.mongat.response.MonagatCategoriesResponse;
import grand.diaaalsalheen.mongat.view.MongatCategoriesAdapter;


public class SubCategoriesItemViewModel extends BaseItemViewModel {

    private SubcategoriesItem subcategoriesItem;
    private int position;

    public SubCategoriesItemViewModel(SubcategoriesItem subcategoriesItem,int position) {
        this.subcategoriesItem = subcategoriesItem;
        this.position=position;
    }


    @Bindable
    public SubcategoriesItem getSubcategoriesItem() {
        return subcategoriesItem;
    }


    public void onSubCateoryClick(){
        getItemsOperationsLiveListener().setValue(Codes.ADAIA_SUB_CATEGORIES);
    }

    public boolean isLight() {
        return position%2==0;
    }


}
