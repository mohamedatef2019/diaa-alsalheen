
package grand.diaaalsalheen.doaa.viewmodel;

import androidx.databinding.Bindable;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.doaa.response.CategoriesItem;
import grand.diaaalsalheen.mongat.response.MonagatCategoryItem;


public class CategoriesItemViewModel extends BaseItemViewModel {
    private CategoriesItem categoriesItem;


    public CategoriesItemViewModel(CategoriesItem categoriesItem) {
        this.categoriesItem = categoriesItem;
    }


    @Bindable
    public CategoriesItem getCategoriesItem() {
        return categoriesItem;
    }


    public void onCategoryClick(){
        getItemsOperationsLiveListener().setValue(Codes.ADAIA_SUB_CATEGORIES);
    }


}
