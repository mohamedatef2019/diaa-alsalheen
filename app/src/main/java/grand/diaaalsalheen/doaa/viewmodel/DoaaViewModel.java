package grand.diaaalsalheen.doaa.viewmodel;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.doaa.response.CategoriesItem;
import grand.diaaalsalheen.doaa.response.DoaaResponse;
import grand.diaaalsalheen.doaa.response.SubCategoryResponse;
import grand.diaaalsalheen.doaa.view.AdaiaCategoriesAdapter;
import grand.diaaalsalheen.doaa.view.AdaiaSubCategoriesAdapter;
import grand.diaaalsalheen.home.viewmodel.ImagesViewModel;
import grand.diaaalsalheen.mongat.response.MongatDetailsItem;


public class DoaaViewModel extends ImagesViewModel {

    private AdaiaCategoriesAdapter adaiaCategoriesAdapter;

    private AdaiaSubCategoriesAdapter adaiaSubCategoriesAdapter;

    public DoaaViewModel() {
        getAdiaCategories();
    }


    @BindingAdapter({"app:adapter"})
    public static void getAdaiaBinding(RecyclerView recyclerView, AdaiaCategoriesAdapter adaiaAdapter) {
        recyclerView.setAdapter(adaiaAdapter);
    }


    @BindingAdapter({"app:adapter"})
    public static void getAdaiaBinding(RecyclerView recyclerView, AdaiaSubCategoriesAdapter adaiaAdapter) {
        recyclerView.setAdapter(adaiaAdapter);
    }




    @Bindable
    public AdaiaCategoriesAdapter getAdaiaCategoriesAdapter() {
        return this.adaiaCategoriesAdapter == null ? this.adaiaCategoriesAdapter = new AdaiaCategoriesAdapter() : this.adaiaCategoriesAdapter;
    }

    @Bindable
    public AdaiaSubCategoriesAdapter getAdaiaSubCategoriesAdapter() {
        return this.adaiaSubCategoriesAdapter == null ? this.adaiaSubCategoriesAdapter = new AdaiaSubCategoriesAdapter() : this.adaiaSubCategoriesAdapter;
    }

    private void getAdiaCategories() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                DoaaResponse doaaResponse = (DoaaResponse) response;

              /* CategoriesItem categoriesItem =   new CategoriesItem();
                categoriesItem.setId(-1);
                categoriesItem.setName(ResourcesManager.getString(R.string.takebat));
                categoriesItem.setSelected(false);
                doaaResponse.getData().getCategories().add(0,categoriesItem);*/

                doaaResponse.getData().getCategories().get(0).setSelected(true);
                getAdaiaCategoriesAdapter().updateData(doaaResponse.getData().getCategories());
                getAdaiaSubCategoriesAdapter().updateData(doaaResponse.getData().getSubcategories());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.DOAA_CATEGORIES, null, DoaaResponse.class);
    }


    public void getAdiaSubCategories(int id) {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                SubCategoryResponse subCategoryResponse = (SubCategoryResponse) response;
                getAdaiaSubCategoriesAdapter().updateData(subCategoryResponse.getData());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.DOAA_SUB_CATEGORIES+id, null, SubCategoryResponse.class);
    }



}
