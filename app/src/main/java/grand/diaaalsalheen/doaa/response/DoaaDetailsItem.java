package grand.diaaalsalheen.doaa.response;


import com.google.gson.annotations.SerializedName;


public class DoaaDetailsItem {

	@SerializedName("voice")
	private String voice;

	@SerializedName("doaa_sub_category_id")
	private int doaaSubCategoryId;

	@SerializedName("id")
	private int id;

	@SerializedName("text")
	private String text;

	public void setVoice(String voice){
		this.voice = voice;
	}

	public String getVoice(){
		return voice;
	}

	public void setDoaaSubCategoryId(int doaaSubCategoryId){
		this.doaaSubCategoryId = doaaSubCategoryId;
	}

	public int getDoaaSubCategoryId(){
		return doaaSubCategoryId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"voice = '" + voice + '\'' + 
			",doaa_sub_category_id = '" + doaaSubCategoryId + '\'' + 
			",id = '" + id + '\'' + 
			",text = '" + text + '\'' + 
			"}";
		}
}