package grand.diaaalsalheen.doaa.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CategoriesItem{

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

    @Expose
    private boolean isSelected;


	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}


	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}


    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }



	@Override
 	public String toString(){
		return 
			"CategoriesItem{" + 
			"name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}