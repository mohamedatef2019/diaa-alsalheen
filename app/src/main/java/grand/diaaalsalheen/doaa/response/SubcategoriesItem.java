package grand.diaaalsalheen.doaa.response;

 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;


public class SubcategoriesItem implements Serializable {

	@SerializedName("name")
	private String name;

	@SerializedName("doaa_category_id")
	private int doaaCategoryId;

	@SerializedName("id")
	private int id;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDoaaCategoryId(int doaaCategoryId){
		this.doaaCategoryId = doaaCategoryId;
	}

	public int getDoaaCategoryId(){
		return doaaCategoryId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"SubcategoriesItem{" + 
			"name = '" + name + '\'' + 
			",doaa_category_id = '" + doaaCategoryId + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}