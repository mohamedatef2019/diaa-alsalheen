package grand.diaaalsalheen.doaa.response;

import java.util.List;
 import com.google.gson.annotations.SerializedName;


public class DoaaItem {

	@SerializedName("categories")
	private List<CategoriesItem> categories;

	@SerializedName("subcategories")
	private List<SubcategoriesItem> subcategories;

	public void setCategories(List<CategoriesItem> categories){
		this.categories = categories;
	}

	public List<CategoriesItem> getCategories(){
		return categories;
	}

	public void setSubcategories(List<SubcategoriesItem> subcategories){
		this.subcategories = subcategories;
	}

	public List<SubcategoriesItem> getSubcategories(){
		return subcategories;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"categories = '" + categories + '\'' + 
			",subcategories = '" + subcategories + '\'' + 
			"}";
		}
}