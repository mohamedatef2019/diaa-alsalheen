package grand.diaaalsalheen.sendmessage.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.SweetAlertDialogss;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentSendCodeBinding;
import grand.diaaalsalheen.databinding.FragmentSendMessageBinding;
import grand.diaaalsalheen.sendmessage.viewmodel.SendMessageViewModel;

public class SendMessageFragment extends BaseFragment {

    private View rootView;
    private SendMessageViewModel sendMessageViewModel;
    private FragmentSendMessageBinding fragmentSendMessageBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSendMessageBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_send_message, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentSendMessageBinding.getRoot();
        sendMessageViewModel = new SendMessageViewModel();
        fragmentSendMessageBinding.setSendMessageViewModel(sendMessageViewModel);
    }


    private void liveDataListeners() {
        sendMessageViewModel.getClicksMutableLiveData().observe(this, result -> {

            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }if (result == Codes.DATA_UPDATED  ) {
                SweetAlertDialogss.messageSent(getActivity());
            }

        });
    }


}