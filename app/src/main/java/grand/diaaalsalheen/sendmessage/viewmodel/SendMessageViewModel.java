package grand.diaaalsalheen.sendmessage.viewmodel;

import android.view.View;
import com.android.volley.Request;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.model.DefaultResponse;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.sendmessage.request.SendMessageRequest;


public class SendMessageViewModel extends BaseViewModel {
    SendMessageRequest sendMessageRequest;


    public SendMessageViewModel() {

        sendMessageRequest = new SendMessageRequest();
    }


    public void sendMessageClick() {
        sendMessage();
    }


    public SendMessageRequest getSendMessageRequest() {
        return sendMessageRequest;
    }

    private void sendMessage() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                getClicksMutableLiveData().setValue(Codes.DATA_UPDATED);
                DefaultResponse sendCodeResponse = (DefaultResponse) response;
                sendMessageRequest.setEmail(null);
                sendMessageRequest.setName(null);
                sendMessageRequest.setMessage(null);
                notifyChange();
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.CONTACT_US, getSendMessageRequest(), DefaultResponse.class);
    }


}
