package grand.diaaalsalheen.works.viewmodel;

import android.view.View;

import com.android.volley.Request;

import grand.diaaalsalheen.FavouriteRequest;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.favourite.viewmodel.FavouriteViewModel;
import grand.diaaalsalheen.works.response.WorksItem;
import grand.diaaalsalheen.works.response.workdetails.WorkDetailsItem;
import grand.diaaalsalheen.works.response.workdetails.WorkDetailsResponse;


public class WorksDetailsViewModel extends BaseViewModel {

    WorkDetailsItem workDetailsItem;


    public WorksDetailsViewModel(WorksItem worksItem) {
        getDoaaDetails(worksItem);
    }

    private void getDoaaDetails(WorksItem worksItem) {
        accessLoadingBar(View.VISIBLE);


        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                WorkDetailsResponse workDetailsResponse = (WorkDetailsResponse) response;
                workDetailsItem = workDetailsResponse.getData();
                accessLoadingBar(View.GONE);
                notifyChange();
                getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.AMAL_DETAILS + worksItem.getId(), null, WorkDetailsResponse.class);
    }

    public WorkDetailsItem getWorkDetailsItem() {
        return workDetailsItem;
    }

    public void onPlayAudioClick(){
        getClicksMutableLiveData().setValue(Codes.PLAY_AUDIO);
    }

    public void addToFavouriteClick() {
        FavouriteViewModel.addToFavourite(new FavouriteRequest(getWorkDetailsItem().getId(), "App\\Models\\Work"));
    }

    public void onZoomInFontClick() {
        getClicksMutableLiveData().setValue(Codes.ZOOM_IN);
    }

    public void onZoomOutFontClick() {
        getClicksMutableLiveData().setValue(Codes.ZOOM_OUT);
    }



    public void onShareClick() {
        getClicksMutableLiveData().setValue(Codes.SHARE);
    }
}
