package grand.diaaalsalheen.works.viewmodel;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.home.viewmodel.ImagesViewModel;
import grand.diaaalsalheen.works.response.WorksResponse;
import grand.diaaalsalheen.works.view.WorksCategoriesAdapter;


public class WorksViewModel extends ImagesViewModel {

    private WorksCategoriesAdapter worksCategoriesAdapter;


    public WorksViewModel() {
        getCategories();
    }


    public WorksViewModel(int categoryID) {
        getSubCategories(categoryID);
    }



    @BindingAdapter({"app:adapter"})
    public static void getCategoriesBinding(RecyclerView recyclerView, WorksCategoriesAdapter amalCategoriesAdapter) {
        recyclerView.setAdapter(amalCategoriesAdapter);
    }


    @Bindable
    public WorksCategoriesAdapter getWorksCategoriesAdapter() {
        return this.worksCategoriesAdapter == null ? this.worksCategoriesAdapter = new WorksCategoriesAdapter() : this.worksCategoriesAdapter;
    }


    private void getCategories() {
         accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                WorksResponse worksResponse = (WorksResponse) response;
                getWorksCategoriesAdapter().updateData(worksResponse.getData());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.AMAL_CATEGORIES,null,   WorksResponse.class);
    }

    private void getSubCategories(int categoryID) {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                WorksResponse worksResponse = (WorksResponse) response;
                getWorksCategoriesAdapter().updateData(worksResponse.getData());
                accessLoadingBar(View.GONE);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.AMAL_SUB_CATEGORIES+categoryID,null,   WorksResponse.class);
    }
}
