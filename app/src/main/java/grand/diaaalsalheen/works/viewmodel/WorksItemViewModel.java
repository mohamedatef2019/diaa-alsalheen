
package grand.diaaalsalheen.works.viewmodel;

import androidx.databinding.Bindable;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;

import grand.diaaalsalheen.works.response.WorksItem;


public class WorksItemViewModel extends BaseItemViewModel {
    private WorksItem worksItem;
    private int position;

    public WorksItemViewModel(WorksItem worksItem,int position) {
        this.worksItem = worksItem;
        this.position=position;
    }


    @Bindable
    public WorksItem getWorksItem() {
        return worksItem;
    }

    public void onWorkItemClicked(){
        getItemsOperationsLiveListener().setValue(Codes.MONGAT_DETAILS);
    }

    public boolean isLight() {
        return position%2==0;
    }



}
