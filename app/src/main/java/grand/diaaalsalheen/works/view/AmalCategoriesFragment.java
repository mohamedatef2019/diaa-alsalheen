package grand.diaaalsalheen.works.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.utils.MovementManager;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentMonagasBinding;
import grand.diaaalsalheen.databinding.FragmentWorksBinding;
import grand.diaaalsalheen.works.viewmodel.WorksViewModel;


public class AmalCategoriesFragment extends BaseFragment {
    private View rootView;
    private WorksViewModel worksViewModel;
    private FragmentWorksBinding fragmentWorksBinding;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentWorksBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_works, container, false);
        assert getArguments() != null;

        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();

    }

    private void binding() {
        rootView = fragmentWorksBinding.getRoot();
        worksViewModel = new WorksViewModel();
        fragmentWorksBinding.setWorksViewModel(worksViewModel);
    }


    private void liveDataListeners() {
        worksViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }

        });

        worksViewModel.getWorksCategoriesAdapter().getItemsOperationsLiveListener().observe(this, worksItem -> {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Params.BUNDLE_WORK_ITEM, worksItem);
            MovementManager.startActivity(getActivity(), Codes.AMAL_SUB_CATEGORIES, bundle);
        });
    }


}