package grand.diaaalsalheen.works.view;


import android.content.Intent;
import android.os.Bundle;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.huhx0015.hxaudio.audio.HXMusic;

import org.sufficientlysecure.htmltextview.HtmlFormatter;
import org.sufficientlysecure.htmltextview.HtmlFormatterBuilder;

import grand.diaaalsalheen.BuildConfig;
import grand.diaaalsalheen.MediaPlayerUtils;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.Params;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentDoaaDetailsBinding;
import grand.diaaalsalheen.databinding.FragmentWorkDetailsBinding;
import grand.diaaalsalheen.doaa.response.SubcategoriesItem;
import grand.diaaalsalheen.doaa.viewmodel.DoaaDetailsViewModel;
import grand.diaaalsalheen.works.response.WorksItem;
import grand.diaaalsalheen.works.viewmodel.WorksDetailsViewModel;


public class WorksDetailsFragment extends BaseFragment {
    private View rootView;
    private WorksDetailsViewModel worksDetailsViewModel;
    private FragmentWorkDetailsBinding fragmentWorkDetailsBinding;
    private WorksItem worksItem;
    int textSize=0;
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentWorkDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_work_details, container, false);
        assert getArguments() != null;

        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();

    }

    private void binding() {
        rootView = fragmentWorkDetailsBinding.getRoot();
        assert getArguments() != null;
        worksItem = (WorksItem) getArguments().getSerializable(Params.BUNDLE_WORK_ITEM);
        worksDetailsViewModel = new WorksDetailsViewModel(worksItem);
        fragmentWorkDetailsBinding.setWorkDetailsViewModel(worksDetailsViewModel);
    }

    @Override
    public void onDestroyView() {
        HXMusic.stop();
        super.onDestroyView();
    }

    private void liveDataListeners() {
        worksDetailsViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            } else if (result == Codes.PLAY_AUDIO) {
                MediaPlayerUtils.playAudio(worksDetailsViewModel.getWorkDetailsItem().getVoice(),worksItem.getName(),getActivity(),fragmentWorkDetailsBinding.ivPlay);
            }else if (result == Codes.ZOOM_IN) {
                textSize+=5;
                fragmentWorkDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            } else if (result == Codes.ZOOM_OUT) {
                textSize-=5;
                fragmentWorkDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            }
            else if (result == Codes.DATA_RECIVED) {
                Spanned formattedHtml = HtmlFormatter.formatHtml(new HtmlFormatterBuilder().setHtml(worksDetailsViewModel.getWorkDetailsItem().getText()));
                String str=worksDetailsViewModel.getWorkDetailsItem().getText();
                fragmentWorkDetailsBinding.tvHtmlViewer.loadDataWithBaseURL(null, str, "text/html", "utf-8", null);
                textSize =  270;
                fragmentWorkDetailsBinding.tvHtmlViewer.setInitialScale(textSize);
            }else if(result == Codes.SHARE){
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, WebServices.SHARE_LINK+"1/"+ worksDetailsViewModel.getWorkDetailsItem().getId() + "/ar \n\n" + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);

                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }


        });


    }


}