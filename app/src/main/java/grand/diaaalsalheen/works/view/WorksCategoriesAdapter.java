
package grand.diaaalsalheen.works.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.databinding.ItemWorkBinding;
import grand.diaaalsalheen.works.response.WorksItem;
import grand.diaaalsalheen.works.viewmodel.WorksItemViewModel;


public class WorksCategoriesAdapter extends RecyclerView.Adapter<WorksCategoriesAdapter.CompaniesViewHolder> {

    private List<WorksItem> monagatCategoryItems;
    private MutableLiveData<WorksItem> itemsOperationsLiveListener;


    public WorksCategoriesAdapter() {
        this.monagatCategoryItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_work,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {
        WorksItem monagatCategoryItem = monagatCategoryItems.get(position);
        WorksItemViewModel companyItemViewModel = new WorksItemViewModel(monagatCategoryItem,position);
        companyItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
            itemsOperationsLiveListener.setValue(monagatCategoryItem);
        });
        holder.setViewModel(companyItemViewModel);
    }



    public MutableLiveData<WorksItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.monagatCategoryItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<WorksItem> data) {

            this.monagatCategoryItems.clear();

        assert data != null;
        this.monagatCategoryItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemWorkBinding itemWorkBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemWorkBinding == null) {
                itemWorkBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemWorkBinding != null) {
                itemWorkBinding.unbind();
            }
        }

        void setViewModel(WorksItemViewModel worksItemViewModel) {
            if (itemWorkBinding != null) {
                itemWorkBinding.setWorksItemViewModel(worksItemViewModel);
            }
        }


    }



}
