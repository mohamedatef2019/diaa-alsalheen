package grand.diaaalsalheen.works.response.workdetails;

import com.google.gson.annotations.SerializedName;


public class WorkDetailsItem {

	@SerializedName("voice")
	private String voice;

	@SerializedName("aamal_category_id")
	private int aamalCategoryId;

	@SerializedName("id")
	private int id;

	@SerializedName("text")
	private String text;

	public void setVoice(String voice){
		this.voice = voice;
	}

	public String getVoice(){
		return voice;
	}

	public void setAamalCategoryId(int aamalCategoryId){
		this.aamalCategoryId = aamalCategoryId;
	}

	public int getAamalCategoryId(){
		return aamalCategoryId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"voice = '" + voice + '\'' + 
			",aamal_category_id = '" + aamalCategoryId + '\'' + 
			",id = '" + id + '\'' + 
			",text = '" + text + '\'' + 
			"}";
		}
}