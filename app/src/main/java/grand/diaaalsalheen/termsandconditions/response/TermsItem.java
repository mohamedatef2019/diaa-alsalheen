package grand.diaaalsalheen.termsandconditions.response;

 import com.google.gson.annotations.SerializedName;


public class TermsItem {

	@SerializedName("term_title")
	private String termTitle;

	@SerializedName("id")
	private int id;

	@SerializedName("term_desc")
	private String termDesc;

	public void setTermTitle(String termTitle){
		this.termTitle = termTitle;
	}

	public String getTermTitle(){
		return termTitle;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTermDesc(String termDesc){
		this.termDesc = termDesc;
	}

	public String getTermDesc(){
		return termDesc;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"term_title = '" + termTitle + '\'' + 
			",id = '" + id + '\'' + 
			",term_desc = '" + termDesc + '\'' + 
			"}";
		}
}