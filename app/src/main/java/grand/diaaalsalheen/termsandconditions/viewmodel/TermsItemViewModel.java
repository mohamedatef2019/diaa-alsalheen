
package grand.diaaalsalheen.termsandconditions.viewmodel;

import androidx.databinding.Bindable;
import grand.diaaalsalheen.base.viewmodel.BaseItemViewModel;
import grand.diaaalsalheen.termsandconditions.response.TermsItem;



public class TermsItemViewModel extends BaseItemViewModel {
    private TermsItem termsItem;


    public TermsItemViewModel(TermsItem termsItem) {
        this.termsItem = termsItem;
    }


    @Bindable
    public TermsItem getTermsItem() {
        return termsItem;
    }




}
