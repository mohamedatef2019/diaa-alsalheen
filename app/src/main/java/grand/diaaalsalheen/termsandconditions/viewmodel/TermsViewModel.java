package grand.diaaalsalheen.termsandconditions.viewmodel;


import android.util.Log;
import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.tasks.view.TasksAdapter;
import grand.diaaalsalheen.tasks.view.TasksProgressAdapter;
import grand.diaaalsalheen.termsandconditions.response.TermsItem;
import grand.diaaalsalheen.termsandconditions.response.TermsResponse;
import grand.diaaalsalheen.termsandconditions.view.TermsAdapter;


public class TermsViewModel extends BaseViewModel {

    private TermsAdapter termsAdapter;


    public TermsViewModel() {

        getTerms();
    }




    private void getTerms() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                TermsResponse termsResponse = (TermsResponse) response;
                getTermsAdapter().updateData(termsResponse.getData());
                accessLoadingBar(View.GONE);
                Log.e("err",termsResponse.toString());
                notifyChange();
            }
        }).requestJsonObject(Request.Method.GET, WebServices.TERMS_AND_CONDITIONS, new Object(), TermsResponse.class);
    }



    @BindingAdapter({"app:adapter"})
    public static void getTermsBinding(RecyclerView recyclerView, TermsAdapter termsAdapter) {
            recyclerView.setAdapter(termsAdapter);

    }


    @Bindable
    public TermsAdapter getTermsAdapter() {
        return this.termsAdapter = this.termsAdapter==null?new TermsAdapter():this.termsAdapter ;
    }




}
