package grand.diaaalsalheen.termsandconditions.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentTermsBinding;
import grand.diaaalsalheen.termsandconditions.viewmodel.TermsViewModel;


public class TermsFragment extends BaseFragment {

    private View rootView;
    private TermsViewModel termsViewModel;
    private FragmentTermsBinding fragmentTermsBinding;



    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentTermsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_terms, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentTermsBinding.getRoot();
        termsViewModel = new TermsViewModel();
        fragmentTermsBinding.setTermsViewModel(termsViewModel);
    }


    private void liveDataListeners() {
        termsViewModel.getClicksMutableLiveData().observe(this, result -> {
            if (result == View.VISIBLE || result == View.GONE) {
                accessLoadingBar(result);
            }
        });

    }



    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }




}