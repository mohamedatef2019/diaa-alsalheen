
package grand.diaaalsalheen.termsandconditions.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.diaaalsalheen.R;
import grand.diaaalsalheen.databinding.ItemTermBinding;
import grand.diaaalsalheen.termsandconditions.response.TermsItem;
import grand.diaaalsalheen.termsandconditions.viewmodel.TermsItemViewModel;

public class TermsAdapter extends RecyclerView.Adapter<TermsAdapter.CompaniesViewHolder> {

    private List<TermsItem> termsItems;
    private MutableLiveData<TermsItem> itemsOperationsLiveListener;


    public TermsAdapter() {
        this.termsItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public CompaniesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_term,
                new FrameLayout(parent.getContext()), false);
        return new CompaniesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompaniesViewHolder holder, final int position) {
        TermsItem termsItem = termsItems.get(position);
        TermsItemViewModel termsItemViewModel = new TermsItemViewModel(termsItem);
        termsItemViewModel.getItemsOperationsLiveListener().observeForever(aVoid -> {
            itemsOperationsLiveListener.setValue(termsItem);
        });
        holder.setViewModel(termsItemViewModel);
    }


    public MutableLiveData<TermsItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }


    @Override
    public int getItemCount() {
        return this.termsItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CompaniesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<TermsItem> data) {

        this.termsItems.clear();

        assert data != null;
        this.termsItems.addAll(data);

        notifyDataSetChanged();
    }

    static class CompaniesViewHolder extends RecyclerView.ViewHolder {
        ItemTermBinding itemTermBinding;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemTermBinding == null) {
                itemTermBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemTermBinding != null) {
                itemTermBinding.unbind();
            }
        }

        void setViewModel(TermsItemViewModel taskItemViewModel) {
            if (itemTermBinding != null) {
                itemTermBinding.setItemTermViewModel(taskItemViewModel);
            }
        }


    }


}
