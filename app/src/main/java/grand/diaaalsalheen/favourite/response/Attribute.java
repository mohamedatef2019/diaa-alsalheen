package grand.diaaalsalheen.favourite.response;

 import com.google.gson.annotations.Expose;
 import com.google.gson.annotations.SerializedName;

 public class Attribute{

	@SerializedName("voice")
	private String voice;

	@SerializedName("aamal_category_id")
	private int aamalCategoryId;

	@SerializedName("id")
	private int id;

	@SerializedName("text")
	private String text;

	@Expose
     private String title;

	public void setVoice(String voice){
		this.voice = voice;
	}

	public String getVoice(){
		return voice;
	}

	public void setAamalCategoryId(int aamalCategoryId){
		this.aamalCategoryId = aamalCategoryId;
	}

	public int getAamalCategoryId(){
		return aamalCategoryId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

     public void setTitle(String title) {
         this.title = title;
     }

     public String getTitle() {
         return title;
     }

     @Override
 	public String toString(){
		return 
			"Attribute{" + 
			"voice = '" + voice + '\'' + 
			",aamal_category_id = '" + aamalCategoryId + '\'' + 
			",id = '" + id + '\'' + 
			",text = '" + text + '\'' + 
			"}";
		}
}