package grand.diaaalsalheen.favourite.response;


import com.google.gson.annotations.SerializedName;

public class FavouriteItem{

	@SerializedName("user_id")
	private int userId;

	@SerializedName("attribute_id")
	private int attributeId;

	@SerializedName("attribute_type")
	private String attributeType;

	@SerializedName("id")
	private int id;

	@SerializedName("attribute")
	private Attribute attribute;

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setAttributeId(int attributeId){
		this.attributeId = attributeId;
	}

	public int getAttributeId(){
		return attributeId;
	}

	public void setAttributeType(String attributeType){
		this.attributeType = attributeType;
	}

	public String getAttributeType(){
		return attributeType;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setAttribute(Attribute attribute){
		this.attribute = attribute;
	}

	public Attribute getAttribute(){
		return attribute;
	}

	@Override
 	public String toString(){
		return 
			"FavouriteItem{" + 
			"user_id = '" + userId + '\'' + 
			",attribute_id = '" + attributeId + '\'' + 
			",attribute_type = '" + attributeType + '\'' + 
			",id = '" + id + '\'' + 
			",attribute = '" + attribute + '\'' + 
			"}";
		}
}