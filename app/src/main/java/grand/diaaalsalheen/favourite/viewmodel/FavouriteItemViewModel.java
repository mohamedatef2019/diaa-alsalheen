
package grand.diaaalsalheen.favourite.viewmodel;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.lifecycle.MutableLiveData;



import java.util.ArrayList;

import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.favourite.response.FavouriteItem;



public class FavouriteItemViewModel extends BaseObservable {
    private FavouriteItem itemsItem;
    private MutableLiveData<Integer> itemsOperationsLiveListener;

    public FavouriteItemViewModel(FavouriteItem itemsItem) {
        this.itemsItem = itemsItem;
        this.itemsOperationsLiveListener = new MutableLiveData<>();
    }


    public MutableLiveData<Integer> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener;
    }


    @Bindable
    public FavouriteItem getFavouriteItem() {
        return itemsItem;
    }

    public void setFavouriteItem(FavouriteItem itemsItem) {
        this.itemsItem = itemsItem;
    }






    public void addToFavouriteClick(){

        getItemsOperationsLiveListener().setValue(Codes.DELETE_TO_FAVOURITE);
    }



}
