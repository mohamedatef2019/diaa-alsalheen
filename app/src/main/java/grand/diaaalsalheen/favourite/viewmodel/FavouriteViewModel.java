package grand.diaaalsalheen.favourite.viewmodel;

import android.util.Pair;
import android.view.View;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;
import com.google.gson.Gson;
import java.util.List;
import grand.diaaalsalheen.FavouriteRequest;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.constantsutils.WebServices;
import grand.diaaalsalheen.base.model.DefaultResponse;
import grand.diaaalsalheen.base.viewmodel.BaseViewModel;
import grand.diaaalsalheen.base.volleyutils.ConnectionHelper;
import grand.diaaalsalheen.base.volleyutils.ConnectionListener;
import grand.diaaalsalheen.base.volleyutils.MyApplication;
import grand.diaaalsalheen.favourite.response.Attribute;
import grand.diaaalsalheen.favourite.response.FavouritesResponse;
import grand.diaaalsalheen.favourite.view.FavouritesAdapter;
import grand.diaaalsalheen.quran.response.quran.AyahsItem;
import grand.diaaalsalheen.quran.response.quran.QuranResponse;
import grand.diaaalsalheen.quran.response.quran.SurahsItem;

import static grand.diaaalsalheen.quran.view.SewarFragment.getJsonFromAssets;


public class FavouriteViewModel extends BaseViewModel {

    private FavouritesAdapter itemsAdapter;


    public FavouriteViewModel() {

        getItems();
    }


    @BindingAdapter({"app:adapter"})
    public static void getItemsBinding(RecyclerView recyclerView, FavouritesAdapter itemsAdapter) {
        recyclerView.setAdapter(itemsAdapter);

    }

    public FavouritesAdapter getItemsAdapter() {
        return this.itemsAdapter == null ? this.itemsAdapter = new FavouritesAdapter() : this.itemsAdapter;
    }

    private void getItems() {

        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                FavouritesResponse itemsResponse = (FavouritesResponse) response;

                if (itemsResponse.getData() != null) {
                    for (int i = 0; i < itemsResponse.getData().size(); i++) {
                        if (itemsResponse.getData().get(i).getAttribute() == null) {
                            itemsResponse.getData().get(i).setAttribute(new Attribute());
                            Pair<String ,String> aya=getAyah(itemsResponse.getData().get(i).getId());
                            if(aya!=null) {
                                itemsResponse.getData().get(i).getAttribute().setTitle(aya.second);
                                itemsResponse.getData().get(i).getAttribute().setText(aya.first);
                            }
                        }
                    }
                }

                getItemsAdapter().updateData(itemsResponse.getData());
                getClicksMutableLiveData().setValue(Codes.DATA_RECIVED);
                accessLoadingBar(View.GONE);

            }

        }).requestJsonObject(Request.Method.GET, WebServices.FAVOURITE, new Object(), FavouritesResponse.class);
    }


    private Pair<String ,String> getAyah(int ayatNumber) {
        QuranResponse quranResponse;
        String json = getJsonFromAssets(MyApplication.getInstance().getApplicationContext());
        quranResponse = new Gson().fromJson(json, QuranResponse.class);
        List<SurahsItem> surahs = quranResponse.getData().getSurahs();
        for (int i = 0; i < surahs.size(); i++) {
            List<AyahsItem> ayahs = surahs.get(i).getAyahs();
            for (int j = 0; j < ayahs.size(); j++) {
                if (ayahs.get(j).getNumber() == ayatNumber) {
                    return new Pair<>(ayahs.get(j).getText(),surahs.get(i).getName());
                }
            }
        }

        return null;
    }


    public static void addToFavourite(FavouriteRequest favouriteRequest) {

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {


            }
        }).requestJsonObject(Request.Method.POST, WebServices.ADD_TO_FAVOURITE, favouriteRequest, DefaultResponse.class);


    }


}
