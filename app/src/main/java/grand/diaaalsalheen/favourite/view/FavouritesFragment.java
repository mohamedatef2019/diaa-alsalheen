package grand.diaaalsalheen.favourite.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.views.fragments.BaseFragment;
import grand.diaaalsalheen.databinding.FragmentFavouritesBinding;
import grand.diaaalsalheen.favourite.viewmodel.FavouriteViewModel;



public class FavouritesFragment extends BaseFragment {

    private View rootView;
    private FavouriteViewModel itemsViewModel;
    private FragmentFavouritesBinding fragmentFavouritesBinding;



    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentFavouritesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_favourites, container, false);
        init();
        return rootView;
    }


    private void init() {
        binding();
        liveDataListeners();
    }

    private void binding() {
        rootView = fragmentFavouritesBinding.getRoot();
        assert getArguments() != null;
        itemsViewModel = new FavouriteViewModel();
        fragmentFavouritesBinding.setFavouriteViewModel(itemsViewModel);
    }


    private void liveDataListeners() {
        itemsViewModel.getClicksMutableLiveData().observe(this, result -> {

         if(result == Codes.DATA_RECIVED){
                setAdapterListener();
            }else if(result ==  View.VISIBLE || result ==  View.GONE){
                accessLoadingBar(result);
            }
        });
    }


    private void setAdapterListener(){


        itemsViewModel.getItemsAdapter().getItemsOperationsLiveListener().observeForever(itemsItem -> {


        });
    }






}