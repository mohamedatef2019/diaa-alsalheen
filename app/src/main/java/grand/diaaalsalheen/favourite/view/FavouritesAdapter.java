
package grand.diaaalsalheen.favourite.view;

import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.sufficientlysecure.htmltextview.HtmlFormatter;
import org.sufficientlysecure.htmltextview.HtmlFormatterBuilder;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import grand.diaaalsalheen.FavouriteRequest;
import grand.diaaalsalheen.R;
import grand.diaaalsalheen.SweetAlertDialogss;
import grand.diaaalsalheen.base.constantsutils.Codes;
import grand.diaaalsalheen.base.utils.ResourcesManager;
import grand.diaaalsalheen.base.utils.UserPreferenceHelper;
import grand.diaaalsalheen.databinding.ItemFavouriteBinding;
import grand.diaaalsalheen.favourite.response.FavouriteItem;

import grand.diaaalsalheen.favourite.viewmodel.FavouriteItemViewModel;
import grand.diaaalsalheen.favourite.viewmodel.FavouriteViewModel;


public class FavouritesAdapter extends RecyclerView.Adapter<FavouritesAdapter.ItemsViewHolder> {

    private List<FavouriteItem> itemsItems;
    private MutableLiveData<FavouriteItem> itemsOperationsLiveListener;

    public FavouritesAdapter() {
        this.itemsItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public ItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favourite,
                new FrameLayout(parent.getContext()), false);
        return new ItemsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsViewHolder holder, final int position) {
        FavouriteItem itemsItem = itemsItems.get(position);
        FavouriteItemViewModel categoryItemViewModel = new FavouriteItemViewModel(itemsItem);




        categoryItemViewModel.getItemsOperationsLiveListener().observeForever(integer -> {


            if (integer == Codes.ADD_TO_FAVOURITE) {

                if (UserPreferenceHelper.isLogined()) {
                    SweetAlertDialogss.showFavDialog(holder.itemView.getContext());
                } else {
                    SweetAlertDialogss.loginToContinue(holder.itemView.getContext());
                }


            } else if (integer == Codes.DELETE_TO_FAVOURITE) {
                try {
                    if (UserPreferenceHelper.isLogined()) {

                        new SweetAlertDialog(holder.itemView.getContext(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(ResourcesManager.getString(R.string.favourite))
                                .setContentText(ResourcesManager.getString(R.string.msg_sure_delete))
                                .setConfirmButton(ResourcesManager.getString(R.string.action_submit), sweetAlertDialog -> {

                                    FavouriteViewModel.addToFavourite(new FavouriteRequest(itemsItem.getAttributeId(), itemsItem.getAttributeType()));
                                    itemsOperationsLiveListener.setValue(itemsItem);
                                    itemsItems.remove(position);
                                    notifyItemRemoved(position);
                                    notifyDataSetChanged();

                                    sweetAlertDialog.cancel();
                                    sweetAlertDialog.dismiss();
                                })
                                .show();



                    } else {

                        SweetAlertDialogss.loginToContinue(holder.itemView.getContext());



                    }
                } catch (Exception e) {
                    e.getStackTrace();
                }
            }
        });

        Spanned formattedHtml = HtmlFormatter.formatHtml(new HtmlFormatterBuilder().setHtml(itemsItem.getAttribute().getText()));
        holder.itemFavouriteBinding.tvFavouriteDetails.setText(formattedHtml);


        holder.setViewModel(categoryItemViewModel);
    }


    public List<FavouriteItem> getItemsItems() {
        return itemsItems;
    }

    public MutableLiveData<FavouriteItem> getItemsOperationsLiveListener() {
        return itemsOperationsLiveListener = (itemsOperationsLiveListener == null) ? new MutableLiveData<>() : itemsOperationsLiveListener;
    }

    @Override
    public int getItemCount() {
        return this.itemsItems.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ItemsViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ItemsViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<FavouriteItem> data) {
        this.itemsItems.clear();
        assert data != null;
        this.itemsItems.addAll(data);
        notifyDataSetChanged();
    }

     static public class ItemsViewHolder extends RecyclerView.ViewHolder {
        ItemFavouriteBinding itemFavouriteBinding;

        ItemsViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemFavouriteBinding == null) {
                itemFavouriteBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemFavouriteBinding != null) {
                itemFavouriteBinding.unbind();
            }
        }

        void setViewModel(FavouriteItemViewModel itemItemViewModel) {
            if (itemFavouriteBinding != null) {
                itemFavouriteBinding.setItemFavouriteViewModel(itemItemViewModel);
            }
        }


    }
}


